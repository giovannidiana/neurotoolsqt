#ifndef MLABTOOLS_H
#define MLABTOOLS_H

#endif // MLABTOOLS_H

#include<vector>
#include<armadillo>

using namespace std;

namespace MLabTools {

double Entropy(arma::rowvec &);
double Entropy(arma::vec &);
double Entropy(double *,int);
double MaxEntropy(arma::mat &);
double MaxEntropy(arma::mat &);
std::string fixedLengthString(int value, int digits);

}
