#include "settingsform.h"
#include "ui_settingsform.h"
#include "settings.h"

SettingsForm::SettingsForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsForm)
{
    ui->setupUi(this);
}

SettingsForm::SettingsForm(settings *set,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsForm)
{
    ui->setupUi(this);

    CX=set;
    ui->CHECKSHAPE_COMPACTNESS->setText(QString::number(CX->CHECKSHAPE_COMPACTNESS));
    ui->CHECKSHAPE_MAXRATIO->setText(QString::number(CX->CHECKSHAPE_MAXRATIO));
    ui->CHECKSHAPE_MINSIZE->setText(QString::number(CX->CHECKSHAPE_MINSIZE));
    ui->DENSCL_FREQ->setText(QString::number(CX->DENSCL_FREQ));
    ui->DENSCL_TH_DIST->setText(QString::number(CX->DENSCL_TH_DIST));
    ui->DENSCL_TH_SLOPE->setText(QString::number(CX->DENSCL_TH_SLOPE));
    ui->DENSCL_TSHIFT->setText(QString::number(CX->DENSCL_TSHIFT));
    ui->MAXMIN_BINSIZE->setText(QString::number(CX->MAXMIN_BINSIZE));
    ui->MAXSIZE->setText(QString::number(CX->MAXSIZE));
    ui->MIP->setCurrentIndex(CX->MIP);
    ui->NNUSED->setText(QString::number(CX->NNUSED));
    ui->PATTERN_NUM->setText(QString::number(CX->PATTERN_NUM));
    ui->SNR->setText(QString::number(CX->SNR));
    ui->TRAF_BUFFSIZE->setText(QString::number(CX->TRAF_BUFFSIZE));
    ui->TRAF_WS->setText(QString::number(CX->TRAF_WS));
    ui->WSFLOOD_BLUR->setText(QString::number(CX->WSFLOOD_BLUR));
    ui->WSFLOOD_CELLRAD->setText(QString::number(CX->WSFLOOD_CELLRAD));
    ui->WSFLOOD_CORRWEIGHT->setText(QString::number(CX->WSFLOOD_CORRWEIGHT));
    ui->WSFLOOD_MAXMIXDIST->setText(QString::number(CX->WSFLOOD_MAXMIXDIST));
    ui->WSFLOOD_MINFLU->setText(QString::number(CX->WSFLOOD_MINFLU));
    ui->WSFLOOD_RAD->setText(QString::number(CX->WSFLOOD_RAD));
    ui->WSFLOOD_TOL->setText(QString::number(CX->WSFLOOD_TOL));
    ui->BCLPARAMS_VARX->setText(QString::number(CX->BCLPARAMS_VARX));
    ui->BCLPARAMS_VARB->setText(QString::number(CX->BCLPARAMS_VARB));
    ui->BCLPARAMS_VARS->setText(QString::number(CX->BCLPARAMS_VARS));
    ui->BCLPARAMS_PROB->setText(QString::number(CX->BCLPARAMS_PROB));
    ui->BCLPARAMS_LAMBDA->setText(QString::number(CX->BCLPARAMS_LAMBDA));
    ui->BCLPARAMS_B0DEV->setText(QString::number(CX->BCLPARAMS_B0DEV));

}

SettingsForm::~SettingsForm()
{
    delete ui;
}

void SettingsForm::on_OKButton_clicked()
{
    CX->CHECKSHAPE_COMPACTNESS=ui->CHECKSHAPE_COMPACTNESS->text().toDouble();
    CX->CHECKSHAPE_MAXRATIO=ui->CHECKSHAPE_MAXRATIO->text().toDouble();
    CX->CHECKSHAPE_MINSIZE=ui->CHECKSHAPE_MINSIZE->text().toDouble();
    CX->DENSCL_FREQ=ui->DENSCL_FREQ->text().toDouble();
    CX->DENSCL_TH_DIST=ui->DENSCL_TH_DIST->text().toDouble();
    CX->DENSCL_TH_SLOPE=ui->DENSCL_TH_SLOPE->text().toDouble();
    CX->DENSCL_TSHIFT=ui->DENSCL_TSHIFT->text().toDouble();
    CX->MAXMIN_BINSIZE=ui->MAXMIN_BINSIZE->text().toInt();
    CX->MAXSIZE=ui->MAXSIZE->text().toInt();
    CX->MIP=ui->MIP->currentIndex();
    CX->NNUSED=ui->NNUSED->text().toInt();
    CX->PATTERN_NUM=ui->PATTERN_NUM->text().toInt();
    CX->SNR=ui->SNR->text().toDouble();
    CX->TRAF_BUFFSIZE=ui->TRAF_BUFFSIZE->text().toInt();
    CX->TRAF_WS=ui->TRAF_WS->text().toInt();
    CX->WSFLOOD_BLUR=ui->WSFLOOD_BLUR->text().toInt();
    CX->WSFLOOD_CELLRAD=ui->WSFLOOD_CELLRAD->text().toInt();
    CX->WSFLOOD_CORRWEIGHT=ui->WSFLOOD_CORRWEIGHT->text().toDouble();
    CX->WSFLOOD_MAXMIXDIST=ui->WSFLOOD_MAXMIXDIST->text().toDouble();
    CX->WSFLOOD_MINFLU=ui->WSFLOOD_MINFLU->text().toDouble();
    CX->WSFLOOD_RAD=ui->WSFLOOD_RAD->text().toInt();
    CX->WSFLOOD_TOL=ui->WSFLOOD_TOL->text().toDouble();   
    CX->BCLPARAMS_VARX=ui->BCLPARAMS_VARX->text().toDouble();
    CX->BCLPARAMS_VARB=ui->BCLPARAMS_VARB->text().toDouble();
    CX->BCLPARAMS_VARS=ui->BCLPARAMS_VARS->text().toDouble();
    CX->BCLPARAMS_LAMBDA=ui->BCLPARAMS_LAMBDA->text().toDouble();
    CX->BCLPARAMS_PROB=ui->BCLPARAMS_PROB->text().toDouble();
    CX->BCLPARAMS_B0DEV=ui->BCLPARAMS_B0DEV->text().toDouble();

    close();

}

void SettingsForm::on_ResetButton_clicked()
{

}

