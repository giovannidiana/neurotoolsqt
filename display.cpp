#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include "handlers.h"
#include <QString>
#include "display.h"
#include "mlabtools.h"

using namespace std;
using namespace cv;

int DisplayNifti(nifti_image* nim)
{

    size_t i,k;

    // Load nii image

    const size_t nx=nim->nx;
    const size_t ny=nim->ny;
    const size_t nz=nim->nz;

    unsigned short* data = static_cast<unsigned short*>(nim->data);

    vector<unsigned short*> tmp(nz);

    // Construct the 3D image as a vector of 2D images.
    vector<Mat> image3D(nz);	// image to show.

    for(k=0;k<nz;k++){
        tmp[k] = new unsigned short[nx*ny];
        for(i=0;i<nx*ny;i++){ tmp[k][i]=data[nx*ny*k+i];}
        image3D[k]= Mat((int)ny,(int)nx,CV_16U,(void*)tmp[k]);
    }

    // Create the window to display the 3D image and the WST
    namedWindow( "Display window", WINDOW_NORMAL | WINDOW_KEEPRATIO ); // Create a window for display.
    resizeWindow( "Display window", nx,ny);
//    moveWindow("Display window",0,0);

    int brightness=7;
    int minval=0;
    bool run=false;

    // Set up of the GUI.
    int level=0;
    createTrackbar( "Time frame", "Display window", &level, nz-1,  NULL);
    createTrackbar( "Brightness", "Display window", &brightness, 2000,  NULL);
    createTrackbar( "Min", "Display window", &minval, 1000,  NULL);
    imshow("Display window",brightness*(image3D[level]-minval*Mat::ones((int)ny,(int)nx, CV_16U)));
    Point2i param(0,0);

    char c;
    while(c != 'q')	 {
         setMouseCallback("Display window",on_mouse_disp,&param);
         if(level+1<nz && run==1) level+=1;
         ostringstream disp;
         disp<<"x="<<param.y<<", y="<<param.x<<", value="<<image3D[level].at<ushort>(param.y,param.x)<<' '<<level;
         displayStatusBar("Display window", disp.str(), 0 );
         imshow("Display window",brightness*(image3D[level]-minval*Mat::ones((int)ny,(int)nx, CV_16U)));
         c=waitKey(50);
         if(c == ' ') run=!run;
    }

    destroyAllWindows();

    for(k=0;k<nz;k++) {
        delete [] tmp[k];
        tmp[k]=NULL;
    }

    return 0;

}

void DisplayTiff(QString InputFile,QString Prefix)
{
    int index=1;

    string filename=InputFile.toStdString()+"/"+Prefix.toStdString()+"_Cycle00001_Ch2_"+MLabTools::fixedLengthString(index,6)+".ome.tif";
    cout<<filename<<endl;
    vector<Mat> srcRec;

    Mat src=imread(filename);
    while(!src.empty()) {
        srcRec.push_back(src.clone());
        filename=InputFile.toStdString()+"/"+Prefix.toStdString()+"_Cycle00001_Ch2_"+MLabTools::fixedLengthString(index,6)+".ome.tif";
        src=imread(filename);
        cout<<filename<<endl;
        index++;
    }

    namedWindow("Tiff Display",WINDOW_NORMAL|WINDOW_KEEPRATIO);
    int b=0;
    double bd=1;
    cv::createTrackbar( "brightness", "Tiff Display", &b, 200);
    cv::createTrackbar( "time frame", "Tiff Display", &index, srcRec.size());

    char c=' ';
    index=0;
    while(c!='q'){
        bd=0.2+(100-0.2)/200.*b;
        imshow("Tiff Display",bd*srcRec[index]);
        c=waitKey(10);
    }

    destroyAllWindows();

}

