/********************************************************************************
** Form generated from reading UI file 'analysisform.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ANALYSISFORM_H
#define UI_ANALYSISFORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSlider>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AnalysisForm
{
public:
    QLabel *SegmentedImageWindow;
    QWidget *PlotDevice;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QSlider *horizontalSlider;
    QSlider *horizontalSlider_2;
    QSlider *horizontalSlider_3;
    QSlider *horizontalSlider_4;
    QSlider *horizontalSlider_5;

    void setupUi(QWidget *AnalysisForm)
    {
        if (AnalysisForm->objectName().isEmpty())
            AnalysisForm->setObjectName(QString::fromUtf8("AnalysisForm"));
        AnalysisForm->resize(863, 628);
        SegmentedImageWindow = new QLabel(AnalysisForm);
        SegmentedImageWindow->setObjectName(QString::fromUtf8("SegmentedImageWindow"));
        SegmentedImageWindow->setGeometry(QRect(13, 16, 341, 301));
        SegmentedImageWindow->setStyleSheet(QString::fromUtf8("background-color: rgb(248, 161, 73);"));
        PlotDevice = new QWidget(AnalysisForm);
        PlotDevice->setObjectName(QString::fromUtf8("PlotDevice"));
        PlotDevice->setGeometry(QRect(379, 19, 471, 301));
        PlotDevice->setStyleSheet(QString::fromUtf8("background-color: rgb(149, 254, 255);"));
        widget = new QWidget(AnalysisForm);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(640, 360, 131, 191));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSlider = new QSlider(widget);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(horizontalSlider);

        horizontalSlider_2 = new QSlider(widget);
        horizontalSlider_2->setObjectName(QString::fromUtf8("horizontalSlider_2"));
        horizontalSlider_2->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(horizontalSlider_2);

        horizontalSlider_3 = new QSlider(widget);
        horizontalSlider_3->setObjectName(QString::fromUtf8("horizontalSlider_3"));
        horizontalSlider_3->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(horizontalSlider_3);

        horizontalSlider_4 = new QSlider(widget);
        horizontalSlider_4->setObjectName(QString::fromUtf8("horizontalSlider_4"));
        horizontalSlider_4->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(horizontalSlider_4);

        horizontalSlider_5 = new QSlider(widget);
        horizontalSlider_5->setObjectName(QString::fromUtf8("horizontalSlider_5"));
        horizontalSlider_5->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(horizontalSlider_5);


        retranslateUi(AnalysisForm);

        QMetaObject::connectSlotsByName(AnalysisForm);
    } // setupUi

    void retranslateUi(QWidget *AnalysisForm)
    {
        AnalysisForm->setWindowTitle(QApplication::translate("AnalysisForm", "Form", 0, QApplication::UnicodeUTF8));
        SegmentedImageWindow->setText(QString());
#ifndef QT_NO_WHATSTHIS
        horizontalSlider->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_ACCESSIBILITY
        horizontalSlider->setAccessibleName(QString());
#endif // QT_NO_ACCESSIBILITY
    } // retranslateUi

};

namespace Ui {
    class AnalysisForm: public Ui_AnalysisForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ANALYSISFORM_H
