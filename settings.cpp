#include<iostream>
#include<fstream>
#include<cstring>
#include<cstdio>
#include "settings.h"
#include<QString>

using namespace std;

settings::settings() :
    DIST_FUN(settings::EUCLIDEAN_DIST)
{
    WSFLOOD_MINFLU=500;
    WSFLOOD_TOL=1;
    WSFLOOD_RAD=11;
    WSFLOOD_BLUR=50;
    WSFLOOD_CELLRAD=6;
    TRAF_BUFFSIZE=20;
    TRAF_WS=5;
    MAXMIN_BINSIZE=5;
    PATTERN_NUM=0;
    CHECKSHAPE_MAXRATIO=100;
    CHECKSHAPE_MINSIZE=0;
    CHECKSHAPE_COMPACTNESS=0;
    MAXSIZE=300;
    SNR=0;
    WSFLOOD_CORRWEIGHT=0.1;
    WSFLOOD_MAXMIXDIST=10;
    MIP=0;
    DISPLAY_SCALE=1;
    DENSCL_TSHIFT=90;
    DENSCL_FREQ=20;
    DENSCL_TH_SLOPE=1;
    DENSCL_TH_DIST=-1;
    KNNVAR=10;
    NNUSED=4;
    BCLPARAMS_VARX=1e-2;
    BCLPARAMS_VARS=1;
    BCLPARAMS_VARB=1e-4;
    BCLPARAMS_PROB=1e-2;
    BCLPARAMS_LAMBDA=0.6;
    BCLPARAMS_B0DEV=0.6;
}

int settings::load(QString config_filename){
    ifstream config_file(config_filename.toStdString().c_str());
    string tmp;

    if(config_file.is_open()){
        while(config_file>>tmp){
            if(strcmp(tmp.c_str(),"WSFLOOD_MINFLU")==0) config_file>>WSFLOOD_MINFLU;
            if(strcmp(tmp.c_str(),"WSFLOOD_TOL")==0) config_file>>WSFLOOD_TOL;
            if(strcmp(tmp.c_str(),"WSFLOOD_RAD")==0) config_file>>WSFLOOD_RAD;
            if(strcmp(tmp.c_str(),"WSFLOOD_BLUR")==0) config_file>>WSFLOOD_BLUR;
            if(strcmp(tmp.c_str(),"WSFLOOD_CELLRAD")==0) config_file>>WSFLOOD_CELLRAD;
            if(strcmp(tmp.c_str(),"WSFLOOD_CORRWEIGHT")==0) config_file>>WSFLOOD_CORRWEIGHT;
            if(strcmp(tmp.c_str(),"WSFLOOD_MAXMIXDIST")==0) config_file>>WSFLOOD_MAXMIXDIST;
            if(strcmp(tmp.c_str(),"TRAF_BUFFSIZE")==0) config_file>>TRAF_BUFFSIZE;
            if(strcmp(tmp.c_str(),"TRAF_WS")==0) config_file>>TRAF_WS;
            if(strcmp(tmp.c_str(),"MAXMIN_BINSIZE")==0) config_file>>MAXMIN_BINSIZE;
            if(strcmp(tmp.c_str(),"PATTERN_NUM")==0) config_file>>PATTERN_NUM;
            if(strcmp(tmp.c_str(),"CHECKSHAPE_MAXRATIO")==0) config_file>>CHECKSHAPE_MAXRATIO;
            if(strcmp(tmp.c_str(),"CHECKSHAPE_MINSIZE")==0) config_file>>CHECKSHAPE_MINSIZE;
            if(strcmp(tmp.c_str(),"CHECKSHAPE_COMPACTNESS")==0) config_file>>CHECKSHAPE_COMPACTNESS;
            if(strcmp(tmp.c_str(),"MAXSIZE")==0) config_file>>MAXSIZE;
            if(strcmp(tmp.c_str(),"SNR")==0) config_file>>SNR;
            if(strcmp(tmp.c_str(),"MIP")==0) config_file>>MIP;
            if(strcmp(tmp.c_str(),"DENSCL_TSHIFT")==0) config_file>>DENSCL_TSHIFT;
            if(strcmp(tmp.c_str(),"DENSCL_FREQ")==0) config_file>>DENSCL_FREQ;
            if(strcmp(tmp.c_str(),"DENSCL_TH_SLOPE")==0) config_file>>DENSCL_TH_SLOPE;
            if(strcmp(tmp.c_str(),"DENSCL_TH_DIST")==0) config_file>>DENSCL_TH_DIST;
            if(strcmp(tmp.c_str(),"DISPLAY_SCALE")==0) config_file>>DISPLAY_SCALE;
            if(strcmp(tmp.c_str(),"KNNVAR")==0) config_file>>KNNVAR;
            if(strcmp(tmp.c_str(),"NNUSED")==0) config_file>>NNUSED;
            if(strcmp(tmp.c_str(),"BCLPARAMS_VARX")==0) config_file>>BCLPARAMS_VARX;
            if(strcmp(tmp.c_str(),"BCLPARAMS_VARB")==0) config_file>>BCLPARAMS_VARB;
            if(strcmp(tmp.c_str(),"BCLPARAMS_VARS")==0) config_file>>BCLPARAMS_VARS;
            if(strcmp(tmp.c_str(),"BCLPARAMS_LAMBDA")==0) config_file>>BCLPARAMS_LAMBDA;
            if(strcmp(tmp.c_str(),"BCLPARAMS_PROB")==0) config_file>>BCLPARAMS_PROB;
            if(strcmp(tmp.c_str(),"BCLPARAMS_B0DEV")==0) config_file>>BCLPARAMS_B0DEV;
        }
        config_file.close();
        return 0;
    } else {
        config_file.close();
        return 1;
    }

}

void settings::print(){
    cout<<"WSFLOOD_MINFLU = "<<WSFLOOD_MINFLU<<endl;
    cout<<"WSFLOOD_TOL = "<<WSFLOOD_TOL<<endl;
    cout<<"WSFLOOD_RAD = "<<WSFLOOD_RAD<<endl;
    cout<<"WSFLOOD_BLUR = "<<WSFLOOD_BLUR<<endl;
    cout<<"WSFLOOD_CELLRAD = "<<WSFLOOD_CELLRAD<<endl;
    cout<<"WSFLOOD_CORRWEIGHT = "<<WSFLOOD_CORRWEIGHT<<endl;
    cout<<"WSFLOOD_MAXMIXDIST = "<<WSFLOOD_MAXMIXDIST<<endl;
    cout<<"TRAF_BUFFSIZE = "<<TRAF_BUFFSIZE<<endl;
    cout<<"TRAF_WS = "<<TRAF_WS<<endl;
    cout<<"MAXMIN_BINSIZE = "<<MAXMIN_BINSIZE<<endl;
    cout<<"PATTERN_NUM = "<<PATTERN_NUM<<endl;
    cout<<"CHECKSHAPE_MAXRATIO = "<<CHECKSHAPE_MAXRATIO<<endl;
    cout<<"CHECKSHAPE_MINSIZE = "<<CHECKSHAPE_MINSIZE<<endl;
    cout<<"CHECKSHAPE_COMPACTNESS = "<<CHECKSHAPE_COMPACTNESS<<endl;
    cout<<"MAXSIZE = "<<MAXSIZE<<endl;
    cout<<"SNR = "<<SNR<<endl;
    cout<<"MIP = "<<MIP<<endl;
}
