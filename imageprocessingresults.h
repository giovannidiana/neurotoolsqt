#ifndef IMAGEPROCESSINGRESULTS_H
#define IMAGEPROCESSINGRESULTS_H

#include <QDialog>
#include "displayws.h"
#include "timesequencedisplay.h"
#include "bcldisplay.h"
#include "bclgroupdisplay.h"

namespace Ui {
class imageprocessingresults;
}

class imageprocessingresults : public QDialog
{
    Q_OBJECT

public:
    explicit imageprocessingresults(ImageProcessing &, QImage *im, QWidget *parent = 0);
    ~imageprocessingresults();

    void ShowSegmentedImage(QImage &image);

    QVector<TimeSequenceDisplay*> ts;
    QVector<BCLDisplay*> bcl_ts;

private slots:

    void on_FilterButton_clicked();

    void on_SNR_valueChanged(int value);

    void on_CellRadius_valueChanged(int value);

    void on_Size_valueChanged(int value);

    void on_Ratio_valueChanged(int value);

    void on_Compactness_valueChanged(int value);

    void on_Update_clicked();

    void on_toolButton_clicked();

    void on_SaveButton_clicked();

    void on_pushButton_clicked();

public slots:
    void mouse_current_pos(QPoint&);
    void mouse_current_cell(QPoint &pos);
    void mouse_click_on_cell(QPoint &pos);
    void mouse_double_click_on_cell(QPoint &pos);

private:
    ImageProcessing *IP;
    Ui::imageprocessingresults *ui;
    QImage *SegmentedImage;
    BCLparams bclpar;
};

#endif // IMAGEPROCESSINGRESULTS_H
