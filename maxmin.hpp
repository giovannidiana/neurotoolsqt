#ifndef MAXMIN_HPP
#define MAXMIN_HPP

#include<iostream>
#include<vector>

using namespace std;

template<typename T>
T maxmin(const T* trace, int size, int binsize, bool verbose=0){

    const T* ptr=trace;
    const T* end=trace+size;

    T maxf=0;
    T minf;
    int counter=0;
    int gcounter=0;

    while(ptr!=end){


        if(verbose) cout<<"start new bin"<<endl;
        counter=0;
        minf=*ptr;
        if(verbose) cout<<gcounter<<' '<<0<<' '<<*ptr<<endl;
        gcounter++;
        counter++;

        while(counter<binsize){
            ++ptr;
            if(ptr==end) break;
            minf=min(minf,*ptr);
                if(verbose) cout<<gcounter<<' '<<counter<<' '<<*ptr<<endl;
            counter++;
            gcounter++;
        }

        if(counter==binsize) {
                if(verbose) cout<<"min="<<minf<<endl;
            maxf=max(maxf,minf);
            ++ptr;
        } else break;
    }

    return maxf;
}




#endif // MAXMIN_HPP

