#ifndef SETTINGSFORM_H
#define SETTINGSFORM_H

#include <QDialog>
#include "settings.h"

namespace Ui {
class SettingsForm;
}

class SettingsForm : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsForm(QWidget *parent = 0);
    explicit SettingsForm(settings *set,QWidget *parent = 0);
    ~SettingsForm();

private slots:
    void on_OKButton_clicked();

    void on_ResetButton_clicked();

private:
    Ui::SettingsForm *ui;
    settings *CX;
};

#endif // SETTINGSFORM_H
