#include "mcolors.h"

mcolors::mcolors()
{
    vec.resize(18);
    vec[0]=qRgb(110,249,20);
    vec[1]=qRgb(241,6,227);
    vec[2]=qRgb(6,12,241);
    vec[3]=qRgb(31,28,37);
    vec[4]=qRgb(23,100,98);
    vec[5]=qRgb(243,250,84);
    vec[6]=qRgb(84,240,250);
    vec[7]=qRgb(250,109,84);
    vec[8]=qRgb(218,50,218);
    vec[9]=qRgb(252,242,249);
    vec[10]=qRgb(250,84,166);
    vec[11]=qRgb(84,250,196);
    vec[12]=qRgb(96,38,70);
    vec[13]=qRgb(169,99,80);
    vec[14]=qRgb(209,114,12);
    vec[15]=qRgb(228,234,193);
    vec[16]=qRgb(222,238,130);
    vec[17]=qRgb(24,118,149);

}

