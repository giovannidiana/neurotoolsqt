/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QProgressBar>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen_file;
    QAction *actionClose;
    QAction *actionAbout_this_software;
    QAction *actionSelect_config_file;
    QAction *actionSet_parameters;
    QAction *actionTest_window;
    QAction *actionNeural_assembler;
    QAction *actionFunctional_clustering;
    QAction *actionOpen_Nifti_image;
    QAction *actionImage_processing;
    QAction *actionOpen_Nifti_in_memory;
    QAction *actionOpen_Tif_list;
    QWidget *centralWidget;
    QProgressBar *progressBar;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuInformations;
    QMenu *menuParameters;
    QMenu *menuTools;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(688, 120);
        actionOpen_file = new QAction(MainWindow);
        actionOpen_file->setObjectName(QString::fromUtf8("actionOpen_file"));
        actionClose = new QAction(MainWindow);
        actionClose->setObjectName(QString::fromUtf8("actionClose"));
        actionAbout_this_software = new QAction(MainWindow);
        actionAbout_this_software->setObjectName(QString::fromUtf8("actionAbout_this_software"));
        actionSelect_config_file = new QAction(MainWindow);
        actionSelect_config_file->setObjectName(QString::fromUtf8("actionSelect_config_file"));
        actionSet_parameters = new QAction(MainWindow);
        actionSet_parameters->setObjectName(QString::fromUtf8("actionSet_parameters"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/toobar_icons/resources/Settings_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSet_parameters->setIcon(icon);
        actionTest_window = new QAction(MainWindow);
        actionTest_window->setObjectName(QString::fromUtf8("actionTest_window"));
        actionNeural_assembler = new QAction(MainWindow);
        actionNeural_assembler->setObjectName(QString::fromUtf8("actionNeural_assembler"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/toobar_icons/resources/clustering.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionNeural_assembler->setIcon(icon1);
        actionFunctional_clustering = new QAction(MainWindow);
        actionFunctional_clustering->setObjectName(QString::fromUtf8("actionFunctional_clustering"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/toobar_icons/resources/FC_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionFunctional_clustering->setIcon(icon2);
        actionOpen_Nifti_image = new QAction(MainWindow);
        actionOpen_Nifti_image->setObjectName(QString::fromUtf8("actionOpen_Nifti_image"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/toobar_icons/resources/microscope_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen_Nifti_image->setIcon(icon3);
        actionImage_processing = new QAction(MainWindow);
        actionImage_processing->setObjectName(QString::fromUtf8("actionImage_processing"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/toobar_icons/resources/segmentation_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionImage_processing->setIcon(icon4);
        actionOpen_Nifti_in_memory = new QAction(MainWindow);
        actionOpen_Nifti_in_memory->setObjectName(QString::fromUtf8("actionOpen_Nifti_in_memory"));
        actionOpen_Tif_list = new QAction(MainWindow);
        actionOpen_Tif_list->setObjectName(QString::fromUtf8("actionOpen_Tif_list"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(4, 0, 201, 31));
        progressBar->setValue(0);
        progressBar->setTextVisible(false);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 688, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuInformations = new QMenu(menuBar);
        menuInformations->setObjectName(QString::fromUtf8("menuInformations"));
        menuParameters = new QMenu(menuBar);
        menuParameters->setObjectName(QString::fromUtf8("menuParameters"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuParameters->menuAction());
        menuBar->addAction(menuInformations->menuAction());
        menuFile->addAction(actionOpen_Nifti_image);
        menuFile->addAction(actionOpen_Tif_list);
        menuFile->addAction(actionOpen_Nifti_in_memory);
        menuFile->addAction(actionClose);
        menuFile->addSeparator();
        menuFile->addAction(actionTest_window);
        menuInformations->addAction(actionAbout_this_software);
        menuParameters->addAction(actionSelect_config_file);
        menuParameters->addAction(actionSet_parameters);
        menuTools->addAction(actionImage_processing);
        menuTools->addAction(actionFunctional_clustering);
        menuTools->addAction(actionNeural_assembler);
        mainToolBar->addAction(actionOpen_Nifti_image);
        mainToolBar->addAction(actionImage_processing);
        mainToolBar->addAction(actionFunctional_clustering);
        mainToolBar->addAction(actionNeural_assembler);
        mainToolBar->addAction(actionSet_parameters);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MLab Toolbox", 0, QApplication::UnicodeUTF8));
        actionOpen_file->setText(QApplication::translate("MainWindow", "&Process nifti", 0, QApplication::UnicodeUTF8));
        actionClose->setText(QApplication::translate("MainWindow", "&Close", 0, QApplication::UnicodeUTF8));
        actionAbout_this_software->setText(QApplication::translate("MainWindow", "&Version", 0, QApplication::UnicodeUTF8));
        actionSelect_config_file->setText(QApplication::translate("MainWindow", "&Set config file", 0, QApplication::UnicodeUTF8));
        actionSet_parameters->setText(QApplication::translate("MainWindow", "Set &parameters", 0, QApplication::UnicodeUTF8));
        actionTest_window->setText(QApplication::translate("MainWindow", "&test window", 0, QApplication::UnicodeUTF8));
        actionNeural_assembler->setText(QApplication::translate("MainWindow", "&Neural assembler", 0, QApplication::UnicodeUTF8));
        actionFunctional_clustering->setText(QApplication::translate("MainWindow", "&Functional clustering", 0, QApplication::UnicodeUTF8));
        actionOpen_Nifti_image->setText(QApplication::translate("MainWindow", "&Open Nifti image", 0, QApplication::UnicodeUTF8));
        actionImage_processing->setText(QApplication::translate("MainWindow", "&Image processing", 0, QApplication::UnicodeUTF8));
        actionOpen_Nifti_in_memory->setText(QApplication::translate("MainWindow", "Open Nifti in memory", 0, QApplication::UnicodeUTF8));
        actionOpen_Tif_list->setText(QApplication::translate("MainWindow", "Open Tif list", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "Fi&le", 0, QApplication::UnicodeUTF8));
        menuInformations->setTitle(QApplication::translate("MainWindow", "Help", 0, QApplication::UnicodeUTF8));
        menuParameters->setTitle(QApplication::translate("MainWindow", "Setti&ngs", 0, QApplication::UnicodeUTF8));
        menuTools->setTitle(QApplication::translate("MainWindow", "Tools", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
