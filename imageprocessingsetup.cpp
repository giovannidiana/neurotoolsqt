#include "imageprocessingsetup.h"
#include "ui_imageprocessingsetup.h"
#include "imageprocessingresults.h"
#include "assemblyform.h"
#include <QFileDialog>
#include <opencv2/opencv.hpp>
#include <fstream>
#include <armadillo>

imageprocessingsetup::imageprocessingsetup(settings &set,MainWindow *parent) :
    QDialog(parent),
    ui(new Ui::imageprocessingsetup)
{
    ui->setupUi(this);
    CX=&set;
    main=parent;
//    ResultsFolder="/home/diana/workspace/ToolBox_ML/ToolBox_QT/test";
//    InputFileName="/home/diana/workspace/data/TomSh/161004_PVN_barrage_6dpf_F1_3/im_slice_1/rim_slice_1.nii";
//    Prefix="test";
    ui->PrefixLineEdit->setText("test");
    ui->comboBox->setCurrentIndex(0);
}

imageprocessingsetup::~imageprocessingsetup()
{
    delete ui;
}

void imageprocessingsetup::on_pushButton_clicked()
{

    if(ui->comboBox->currentIndex()<=1){
        InputFileName=QFileDialog::getOpenFileName(
                    this,
                    tr("Select Nifti file"),
                    "/home/diana/workspace/data/TomSh/",
                    "Nifti file (*.nii)"
                    );
    } else if(ui->comboBox->currentIndex()==2){
        InputFileName=QFileDialog::getExistingDirectory(
                0,
                ("Select Folder"),
                (".")
                );
    }

    ui->InputDisplayLabel->setText(InputFileName);
}

void imageprocessingsetup::on_pushButton_2_clicked()
{
    ResultsFolder=QFileDialog::getExistingDirectory(
                0,
                ("Select Folder"),
                (".")
                );

    ui->ResultsFolderDisplayLabel->setText(ResultsFolder);
}

void imageprocessingsetup::on_Clean_clicked()
{
    this->hide();
    Prefix=ui->PrefixLineEdit->text();
    ImageProcessing IP(*CX,main);
    IP.Prefix=Prefix;
    IP.ResultsFolder=ResultsFolder;
    IP.InputFile=InputFileName;

    main->showProgressBar(true);
    IP.TCLsimple();
    main->showProgressBar(false);
}


// Segmentation routine
void imageprocessingsetup::on_Segment_clicked()
{
    this->hide();

    // read the Prefix from the screen
    Prefix=ui->PrefixLineEdit->text();

//    CX->load("/home/diana/workspace/ToolBox_ML/ToolBox_QT/CONFIG_2ph");
//    Prefix="test";
//    InputFileName="/home/diana/workspace/data/TomSa/180220_wt_h2b_gc6s_7dpf_fish2_sa.nii";
//    ResultsFolder="/home/diana/workspace/ToolBox_ML/ToolBox_QT/tomsa";

    // Initialize an instance of image processing
    ImageProcessing IP(*CX,main);

    // Assign folder names and prefix to IP
    IP.Prefix=Prefix;
    IP.ResultsFolder=ResultsFolder;
    IP.InputFile=InputFileName;
    IP.ExternalData=ui->checkData->checkState();

    // Start the processing
    main->showProgressBar(true);
    if(ui->comboBox->currentIndex()==0) /*Full Analysis*/ {
        // segmentation
        IP.dataType=0;
        IP.seg_ws();
        // compute cell properties and generate all_cell_resp
        // NOTE: the size of all_cell_resp is nsegments.
        IP.GetCellProperties();
        cv::imwrite((ResultsFolder+'/'+Prefix+"_wst.tiff").toStdString(), IP.wst_nocut);            
    } else if(ui->comboBox->currentIndex()==1){
        IP.LoadPreviousResults();
        IP.GetCellProperties();
    } else if(ui->comboBox->currentIndex()==2){
        IP.dataType=1;
        IP.seg_ws();
        IP.GetCellProperties();
    }

    main->showProgressBar(false);

    QImage SegmentedImage(IP.width,IP.height,QImage::Format_RGB32);

    imageprocessingresults IPR(IP,&SegmentedImage);

    QVector<QRgb> colors(IP.nsegments+1);

    colors[0]=qRgb(0,0,0);
    SegmentedImage.fill(colors[0]);
    for(unsigned int i=1;i<=IP.nsegments;++i) {
        colors[i]=qRgb((int)(rand()*1./RAND_MAX*255),
                       (int)(rand()*1./RAND_MAX*255),
                       (int)(rand()*1./RAND_MAX*255));
    }

    for(unsigned int i=0;i<IP.width;++i){
        for(unsigned int j=0;j<IP.height;++j){
            if(IP.wst_nocut.at<unsigned short>(j,i)>0)
                SegmentedImage.setPixel(i,j,colors[IP.wst_nocut.at<unsigned short>(j,i)]);
        }
    }

    IPR.ShowSegmentedImage(SegmentedImage);
    IPR.exec();

}

void imageprocessingsetup::on_Assembler_clicked()
{
    this->hide();

    //Prefix=ui->PrefixLineEdit->text();

    CX->load("/home/diana/workspace/ToolBox_ML/ToolBox_QT/CONFIG_2ph");
//    Prefix="test";
//    InputFileName="/home/diana/workspace/data/TomSa/180220_wt_h2b_gc6s_7dpf_fish2_sa.nii";
//    ResultsFolder="/home/diana/workspace/ToolBox_ML/ToolBox_QT/tomsa";

    ImageProcessing IP(*CX,main);
    IP.Prefix=Prefix;
    IP.ResultsFolder=ResultsFolder;
    IP.InputFile=InputFileName;
    IP.ExternalData=ui->checkData->checkState();

    main->showProgressBar(true);
    IP.cosi();
    main->showProgressBar(false);

    AssemblyForm AF(IP,main);
    AF.plotMap();
    AF.exec();
}

void imageprocessingsetup::on_checkData_clicked(bool checked)
{
    ui->comboBox->setEnabled(!checked);
    ui->pushButton->setEnabled(!checked);
}
