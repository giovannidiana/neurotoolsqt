#include "mlabtools.h"

double MLabTools::Entropy(arma::rowvec &w){
    double nfac=0;
    double en=0;
    int i;

    for(i=0; i<w.n_elem;++i) nfac+=w(i);
    for(i=0; i<w.n_elem;++i) if( w(i)>0 ) en+= - w(i)/nfac * log(w(i)/nfac);

    return en;

}

double MLabTools::Entropy(arma::vec &w){
    double nfac=0;
    double en=0;
    int i;

    for(i=0; i<w.n_elem;++i) nfac+=w(i);
    for(i=0; i<w.n_elem;++i) if( w(i)>0 ) en+= - w(i)/nfac * log(w(i)/nfac);

    return en;
}

double MLabTools::Entropy(double *w,int n){
    double nfac=0;
    double en=0;
    int i;

    for(i=0; i<n;++i) nfac+=w[i];
    for(i=0; i<n;++i) if( w[i]>0 ) en+= - w[i]/nfac * log(w[i]/nfac);

    return en;

}

double MLabTools::MaxEntropy(arma::mat &){
    double nfac=0;
    double en=0;
    int i;



    return en;

}

std::string MLabTools::fixedLengthString(int value, int digits) {
    unsigned int uvalue = value;
    if (value < 0) {
        uvalue = -uvalue;
    }
    std::string result;
    while (digits-- > 0) {
        result += ('0' + uvalue % 10);
        uvalue /= 10;
    }
    if (value < 0) {
        result += '-';
    }
    std::reverse(result.begin(), result.end());
    return result;
}
