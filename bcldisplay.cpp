#include "bcldisplay.h"
#include "ui_bcldisplay.h"
#include "bcl.h"

BCLDisplay::BCLDisplay(std::vector<double> &datay,double f,BCLparams &par, QWidget *parent, int x0, int y0) :
    QDialog(parent),
    ui(new Ui::BCLDisplay)
{
    ui->setupUi(this);
    move(x0,y0);
    ui->display->addGraph();
    ui->display->addGraph();
//    ui->display->addGraph();
    y=&datay[0];
    n=datay.size();
    bclres = new bcl_type(n,f);
    bclres->setData(y);
    bclpar=&par;
    ui->slider->setValue((int)((log10(bclpar->varX)+5)*ui->slider->maximum()/6.));
    ui->value->setText(QString::number(bclpar->varX));
}

BCLDisplay::~BCLDisplay()
{
    delete ui;
    delete bclres;
}

void BCLDisplay::plot()
{

    //int good=bclres->bclEM(*bclpar,0,2);
    int good=bclres->bcl(*bclpar,1);

    QVector<double> xx(n),calcium(n),baseline(n);
    QVector<double> activity;
    for(unsigned int i=0;i<n;++i) {
        xx[i]=i;
        calcium[i]=bclres->cal[i];
        baseline[i]=bclres->B[i];
        if(bclres->sks[i]==1){
            activity.push_back(i);
        }

    }

    ui->display->xAxis->setRange(0, n);
    ui->display->yAxis->setRange(*std::min_element(calcium.begin(),calcium.end()),
                              *std::max_element(calcium.begin(),calcium.end()));

    ui->display->graph(0)->setData(xx,calcium);

    QVector<double> yactivity(activity.size(),ui->display->yAxis->range().minRange);
    ui->display->graph(1)->setPen(QPen(Qt::red));
    ui->display->graph(1)->setData(activity,yactivity);
    ui->display->graph(1)->setLineStyle(QCPGraph::lsNone);
    ui->display->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 5));
    ui->display->replot();
}

void BCLDisplay::setTitle(QString title)
{
    setWindowTitle(title);
}

void BCLDisplay::on_slider_sliderMoved(int value)
{
    int ind=ui->variable->currentIndex();
    double expval=pow(10.,-5+(double)value/ui->slider->maximum()*(1+5));
    double probval=(double)value/ui->slider->maximum();
    double lambdaval=(double)value/ui->slider->maximum()*2.;
    switch (ind) {

    case 0: // varX
        bclpar->varX=expval;
        ui->value->setText(QString::number(bclpar->varX));
        break;

    case 1: //var S
        bclpar->varS=expval;
        ui->value->setText(QString::number(bclpar->varS));
        break;

    case 2:
        bclpar->varB=expval;
         ui->value->setText(QString::number(bclpar->varB));
        break;

    case 3:
        bclpar->prob=probval;
         ui->value->setText(QString::number(bclpar->prob));
        break;

    case 4:
        bclpar->lambda=lambdaval;
        ui->value->setText(QString::number(bclpar->lambda));
        break;

    case 5:
        bclpar->b0dev=expval;
        ui->value->setText(QString::number(bclpar->b0dev));
        break;

    }

    plot();

}

void BCLDisplay::on_variable_currentIndexChanged(int index)
{

    switch(index){
    case 0: //varX
        ui->slider->setValue((int)((log10(bclpar->varX)+5)*ui->slider->maximum()/6.));
        ui->value->setText(QString::number(bclpar->varX));
        break;
    case 1: //varS
        ui->slider->setValue((int)((log10(bclpar->varS)+5)*ui->slider->maximum()/6.));
        ui->value->setText(QString::number(bclpar->varS));
        break;
    case 2: //varB
        ui->slider->setValue((int)((log10(bclpar->varB)+5)*ui->slider->maximum()/6.));
        ui->value->setText(QString::number(bclpar->varB));
        break;
    case 3: //prob
        ui->slider->setValue((int)(bclpar->prob*ui->slider->maximum()));
        ui->value->setText(QString::number(bclpar->prob));
        break;
    case 4: //lambda
        ui->slider->setValue((int)(bclpar->lambda*ui->slider->maximum()/2.));
        ui->value->setText(QString::number(bclpar->lambda));
        break;
    case 5: //b0dev
        ui->slider->setValue((int)((log10(bclpar->b0dev)+5)*ui->slider->maximum()/6));
        ui->value->setText(QString::number(bclpar->b0dev));
        break;
    }
}
