/********************************************************************************
** Form generated from reading UI file 'assemblyform.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ASSEMBLYFORM_H
#define UI_ASSEMBLYFORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QScrollBar>
#include <QtGui/QSlider>
#include <QtGui/QSpinBox>
#include <QtGui/QSplitter>
#include <QtGui/QTabWidget>
#include <QtGui/QToolBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_AssemblyForm
{
public:
    QGridLayout *gridLayout_2;
    QTabWidget *tabWidget;
    QWidget *tab_1;
    QGridLayout *gridLayout_3;
    QLabel *neuroimage;
    QWidget *tab_2;
    QGridLayout *gridLayout_4;
    QLabel *label_5;
    QSlider *brightness;
    QLabel *wstlabel;
    QToolBox *toolBox;
    QCustomPlot *entropyPlot;
    QCustomPlot *page_2;
    QWidget *page;
    QGridLayout *gridLayout_5;
    QLabel *label_6;
    QSplitter *splitter_5;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QCustomPlot *Activity;
    QScrollBar *scrollBar;
    QLabel *Raster;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout;
    QSpinBox *spinAssemblies;
    QPushButton *updateEntropy;
    QSplitter *splitter_3;
    QSplitter *splitter_4;
    QSplitter *splitter_2;
    QSlider *threshold;
    QLabel *label_3;
    QSplitter *splitter;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout;
    QSlider *maxval;
    QSlider *minval;
    QLabel *label_4;
    QLabel *label;
    QPushButton *SVD;
    QSpinBox *Expand;
    QLabel *label_2;

    void setupUi(QDialog *AssemblyForm)
    {
        if (AssemblyForm->objectName().isEmpty())
            AssemblyForm->setObjectName(QString::fromUtf8("AssemblyForm"));
        AssemblyForm->resize(825, 693);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(AssemblyForm->sizePolicy().hasHeightForWidth());
        AssemblyForm->setSizePolicy(sizePolicy);
        gridLayout_2 = new QGridLayout(AssemblyForm);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        tabWidget = new QTabWidget(AssemblyForm);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        tab_1 = new QWidget();
        tab_1->setObjectName(QString::fromUtf8("tab_1"));
        gridLayout_3 = new QGridLayout(tab_1);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        neuroimage = new QLabel(tab_1);
        neuroimage->setObjectName(QString::fromUtf8("neuroimage"));
        QSizePolicy sizePolicy1(QSizePolicy::Ignored, QSizePolicy::Ignored);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(neuroimage->sizePolicy().hasHeightForWidth());
        neuroimage->setSizePolicy(sizePolicy1);
        neuroimage->setMinimumSize(QSize(0, 0));
        neuroimage->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        neuroimage->setFrameShape(QFrame::Box);
        neuroimage->setFrameShadow(QFrame::Sunken);
        neuroimage->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout_3->addWidget(neuroimage, 0, 0, 1, 1);

        tabWidget->addTab(tab_1, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayout_4 = new QGridLayout(tab_2);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy2);
        label_5->setMaximumSize(QSize(70, 20));

        gridLayout_4->addWidget(label_5, 1, 0, 1, 1);

        brightness = new QSlider(tab_2);
        brightness->setObjectName(QString::fromUtf8("brightness"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(brightness->sizePolicy().hasHeightForWidth());
        brightness->setSizePolicy(sizePolicy3);
        brightness->setOrientation(Qt::Horizontal);

        gridLayout_4->addWidget(brightness, 1, 1, 1, 1);

        wstlabel = new QLabel(tab_2);
        wstlabel->setObjectName(QString::fromUtf8("wstlabel"));
        sizePolicy1.setHeightForWidth(wstlabel->sizePolicy().hasHeightForWidth());
        wstlabel->setSizePolicy(sizePolicy1);
        wstlabel->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));

        gridLayout_4->addWidget(wstlabel, 0, 0, 1, 2);

        tabWidget->addTab(tab_2, QString());

        gridLayout_2->addWidget(tabWidget, 0, 0, 1, 1);

        toolBox = new QToolBox(AssemblyForm);
        toolBox->setObjectName(QString::fromUtf8("toolBox"));
        sizePolicy2.setHeightForWidth(toolBox->sizePolicy().hasHeightForWidth());
        toolBox->setSizePolicy(sizePolicy2);
        entropyPlot = new QCustomPlot();
        entropyPlot->setObjectName(QString::fromUtf8("entropyPlot"));
        entropyPlot->setGeometry(QRect(0, 0, 400, 242));
        toolBox->addItem(entropyPlot, QString::fromUtf8("Entropy"));
        page_2 = new QCustomPlot();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        page_2->setGeometry(QRect(0, 0, 400, 242));
        toolBox->addItem(page_2, QString::fromUtf8("Page 2"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        gridLayout_5 = new QGridLayout(page);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_6 = new QLabel(page);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_5->addWidget(label_6, 1, 0, 1, 1);

        toolBox->addItem(page, QString::fromUtf8("Page"));

        gridLayout_2->addWidget(toolBox, 0, 1, 1, 1);

        splitter_5 = new QSplitter(AssemblyForm);
        splitter_5->setObjectName(QString::fromUtf8("splitter_5"));
        sizePolicy.setHeightForWidth(splitter_5->sizePolicy().hasHeightForWidth());
        splitter_5->setSizePolicy(sizePolicy);
        splitter_5->setOrientation(Qt::Horizontal);
        splitter_5->setChildrenCollapsible(true);
        layoutWidget = new QWidget(splitter_5);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        Activity = new QCustomPlot(layoutWidget);
        Activity->setObjectName(QString::fromUtf8("Activity"));
        sizePolicy2.setHeightForWidth(Activity->sizePolicy().hasHeightForWidth());
        Activity->setSizePolicy(sizePolicy2);
        Activity->setMinimumSize(QSize(600, 100));
        Activity->setMaximumSize(QSize(16777215, 100));

        verticalLayout->addWidget(Activity);

        scrollBar = new QScrollBar(layoutWidget);
        scrollBar->setObjectName(QString::fromUtf8("scrollBar"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(scrollBar->sizePolicy().hasHeightForWidth());
        scrollBar->setSizePolicy(sizePolicy4);
        scrollBar->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(scrollBar);

        Raster = new QLabel(layoutWidget);
        Raster->setObjectName(QString::fromUtf8("Raster"));
        sizePolicy2.setHeightForWidth(Raster->sizePolicy().hasHeightForWidth());
        Raster->setSizePolicy(sizePolicy2);
        Raster->setMinimumSize(QSize(0, 100));
        Raster->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        Raster->setFrameShape(QFrame::NoFrame);
        Raster->setFrameShadow(QFrame::Plain);
        Raster->setScaledContents(false);

        verticalLayout->addWidget(Raster);

        splitter_5->addWidget(layoutWidget);
        layoutWidget1 = new QWidget(splitter_5);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        gridLayout = new QGridLayout(layoutWidget1);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        spinAssemblies = new QSpinBox(layoutWidget1);
        spinAssemblies->setObjectName(QString::fromUtf8("spinAssemblies"));

        gridLayout->addWidget(spinAssemblies, 2, 0, 1, 1);

        updateEntropy = new QPushButton(layoutWidget1);
        updateEntropy->setObjectName(QString::fromUtf8("updateEntropy"));

        gridLayout->addWidget(updateEntropy, 3, 1, 1, 1);

        splitter_3 = new QSplitter(layoutWidget1);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        splitter_3->setOrientation(Qt::Horizontal);
        splitter_4 = new QSplitter(splitter_3);
        splitter_4->setObjectName(QString::fromUtf8("splitter_4"));
        QSizePolicy sizePolicy5(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(splitter_4->sizePolicy().hasHeightForWidth());
        splitter_4->setSizePolicy(sizePolicy5);
        splitter_4->setOrientation(Qt::Horizontal);
        splitter_2 = new QSplitter(splitter_4);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Vertical);
        threshold = new QSlider(splitter_2);
        threshold->setObjectName(QString::fromUtf8("threshold"));
        QSizePolicy sizePolicy6(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(threshold->sizePolicy().hasHeightForWidth());
        threshold->setSizePolicy(sizePolicy6);
        threshold->setOrientation(Qt::Vertical);
        splitter_2->addWidget(threshold);
        label_3 = new QLabel(splitter_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QSizePolicy sizePolicy7(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy7);
        splitter_2->addWidget(label_3);
        splitter_4->addWidget(splitter_2);
        splitter = new QSplitter(splitter_4);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        layoutWidget2 = new QWidget(splitter);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        horizontalLayout = new QHBoxLayout(layoutWidget2);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        maxval = new QSlider(layoutWidget2);
        maxval->setObjectName(QString::fromUtf8("maxval"));
        sizePolicy6.setHeightForWidth(maxval->sizePolicy().hasHeightForWidth());
        maxval->setSizePolicy(sizePolicy6);
        maxval->setOrientation(Qt::Vertical);

        horizontalLayout->addWidget(maxval);

        minval = new QSlider(layoutWidget2);
        minval->setObjectName(QString::fromUtf8("minval"));
        sizePolicy6.setHeightForWidth(minval->sizePolicy().hasHeightForWidth());
        minval->setSizePolicy(sizePolicy6);
        minval->setOrientation(Qt::Vertical);

        horizontalLayout->addWidget(minval);

        splitter->addWidget(layoutWidget2);
        label_4 = new QLabel(splitter);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QSizePolicy sizePolicy8(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy8.setHorizontalStretch(0);
        sizePolicy8.setVerticalStretch(0);
        sizePolicy8.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy8);
        label_4->setAlignment(Qt::AlignCenter);
        splitter->addWidget(label_4);
        splitter_4->addWidget(splitter);
        splitter_3->addWidget(splitter_4);

        gridLayout->addWidget(splitter_3, 0, 0, 1, 2);

        label = new QLabel(layoutWidget1);
        label->setObjectName(QString::fromUtf8("label"));
        sizePolicy2.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy2);

        gridLayout->addWidget(label, 1, 1, 1, 1);

        SVD = new QPushButton(layoutWidget1);
        SVD->setObjectName(QString::fromUtf8("SVD"));

        gridLayout->addWidget(SVD, 3, 0, 1, 1);

        Expand = new QSpinBox(layoutWidget1);
        Expand->setObjectName(QString::fromUtf8("Expand"));
        QSizePolicy sizePolicy9(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy9.setHorizontalStretch(0);
        sizePolicy9.setVerticalStretch(0);
        sizePolicy9.setHeightForWidth(Expand->sizePolicy().hasHeightForWidth());
        Expand->setSizePolicy(sizePolicy9);
        Expand->setMinimumSize(QSize(0, 0));

        gridLayout->addWidget(Expand, 1, 0, 1, 1);

        label_2 = new QLabel(layoutWidget1);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 2, 1, 1, 1);

        splitter_5->addWidget(layoutWidget1);

        gridLayout_2->addWidget(splitter_5, 3, 0, 1, 2);

        QWidget::setTabOrder(tabWidget, Expand);
        QWidget::setTabOrder(Expand, spinAssemblies);
        QWidget::setTabOrder(spinAssemblies, SVD);
        QWidget::setTabOrder(SVD, maxval);
        QWidget::setTabOrder(maxval, minval);
        QWidget::setTabOrder(minval, threshold);

        retranslateUi(AssemblyForm);

        tabWidget->setCurrentIndex(0);
        toolBox->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(AssemblyForm);
    } // setupUi

    void retranslateUi(QDialog *AssemblyForm)
    {
        AssemblyForm->setWindowTitle(QApplication::translate("AssemblyForm", "Neural assembler", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        tabWidget->setToolTip(QApplication::translate("AssemblyForm", "<html><head/><body><p><br/></p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        neuroimage->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_1), QApplication::translate("AssemblyForm", "Display", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("AssemblyForm", "Brightness", 0, QApplication::UnicodeUTF8));
        wstlabel->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("AssemblyForm", "Projection", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(entropyPlot), QApplication::translate("AssemblyForm", "Entropy", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(page_2), QApplication::translate("AssemblyForm", "Page 2", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("AssemblyForm", "TextLabel", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(page), QApplication::translate("AssemblyForm", "Page", 0, QApplication::UnicodeUTF8));
        Raster->setText(QString());
        updateEntropy->setText(QApplication::translate("AssemblyForm", "Update Entropy", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("AssemblyForm", "Threshold", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("AssemblyForm", "Range controls", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("AssemblyForm", "Bin size", 0, QApplication::UnicodeUTF8));
        SVD->setText(QApplication::translate("AssemblyForm", "SVD", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("AssemblyForm", "Assemblies", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AssemblyForm: public Ui_AssemblyForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ASSEMBLYFORM_H
