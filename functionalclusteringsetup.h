#ifndef FUNCTIONALCLUSTERINGSETUP_H
#define FUNCTIONALCLUSTERINGSETUP_H

#include <QDialog>
#include <QString>
#include "settings.h"
#include <vector>
#include "dialog.h"
#include "mainwindow.h"

namespace Ui {
class FunctionalClusteringSetup;
}

class FunctionalClusteringSetup : public QDialog
{
    Q_OBJECT

public:
    explicit FunctionalClusteringSetup(QWidget *parent = 0);
    explicit FunctionalClusteringSetup(MainWindow *main,QWidget *parent = 0);

    ~FunctionalClusteringSetup();

     settings *CX;
     Dialog *SelectEpochDialog;
     MainWindow* mw;
     void updateProgressBar(int,QString);

private slots:
    void on_ChooseEpochButton_clicked();

    void on_ChooseInputButton_clicked();

    void on_AnalyzeButton_clicked();

    void on_ChooseResultsFolderButton_clicked();

    void on_checkData_clicked(bool checked);

private:
    Ui::FunctionalClusteringSetup *ui;
    int Mode;
    QString InputFileName;
    QString EpochFileName;
    QString ResultsFolder;
    QString Prefix;
    void RetrieveEpochList(std::vector<QString> &);
    int GetNEP();
};

#endif // FUNCTIONALCLUSTERINGSETUP_H
