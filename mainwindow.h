#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "settings.h"
#include <nifti1_io.h>
#include <opencv2/opencv.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void updateProgressBar(int,QString);
    void showProgressBar(bool);
    nifti_image* nim;
    std::vector<cv::Mat> tiffdata;

private slots:

    void on_actionClose_triggered();

    void on_actionAbout_this_software_triggered();

    void on_actionSet_parameters_triggered();

    void on_actionSelect_config_file_triggered();

    void on_actionOpen_Nifti_image_triggered();

    nifti_image* loadData();

    void on_actionFunctional_clustering_triggered();

    void on_actionNeural_assembler_triggered();

    void on_actionImage_processing_triggered();


    void on_actionOpen_Nifti_in_memory_triggered();


    void on_actionOpen_Tif_list_triggered();

private:
    Ui::MainWindow *ui;
    settings CX;
};

#endif // MAINWINDOW_H
