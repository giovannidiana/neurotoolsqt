#ifndef ASSEMBLYFORM_H
#define ASSEMBLYFORM_H

#include <QDialog>
#include "displayws.h"
#include <armadillo>
#include "mainwindow.h"

namespace Ui {
class AssemblyForm;
}

class AssemblyForm : public QDialog
{
    Q_OBJECT

public:
    explicit AssemblyForm(ImageProcessing &,MainWindow *parent = 0);
    ~AssemblyForm();

    void setup(MainWindow *parent);
    void MakeBinary(int,double); // this makes activity
    void plotTotalActivity(double);
    void plotMap();
    void PlotImage(int frame);
    void PlotWST();

private slots:
    void on_maxval_valueChanged(int value);

    void on_minval_valueChanged(int value);

    void on_scrollBar_valueChanged(int value);

    void on_brightness_valueChanged(int value);

    void on_Expand_valueChanged(int arg1);

    void on_threshold_valueChanged(int value);

    void on_SVD_clicked();

    void on_updateEntropy_clicked();

private:
    Ui::AssemblyForm *ui;
    ImageProcessing *ip;

    arma::Mat<int> binary_display;
    arma::Mat<int> binary_from_bcl;
    MainWindow *main;
    arma::Row<int> activity_tot;
    arma::mat activity;
    std::vector<int> cellID_orig;
    QPixmap *pix;
    QImage *qim;

    QPixmap *pix_neurons;
    QImage *image_neurons;

    QPixmap *pix_wst;
    QImage *image_wst;

    double brightness;
    int ncells;
    int binsize;

    int BCLMODE;
    int BCLEMSTEPS;

    double threshold;
    unsigned int n_assemblies;
    bool svd_on;
    std::vector<unsigned int> cell_labels;
};

#endif // ASSEMBLYFORM_H
