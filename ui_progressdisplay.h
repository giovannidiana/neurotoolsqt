/********************************************************************************
** Form generated from reading UI file 'progressdisplay.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROGRESSDISPLAY_H
#define UI_PROGRESSDISPLAY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QProgressBar>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ProgressDisplay
{
public:
    QVBoxLayout *verticalLayout;
    QProgressBar *progressBar;
    QLabel *AnalysisStatus;

    void setupUi(QDialog *ProgressDisplay)
    {
        if (ProgressDisplay->objectName().isEmpty())
            ProgressDisplay->setObjectName(QString::fromUtf8("ProgressDisplay"));
        ProgressDisplay->resize(400, 109);
        verticalLayout = new QVBoxLayout(ProgressDisplay);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        progressBar = new QProgressBar(ProgressDisplay);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);

        verticalLayout->addWidget(progressBar);

        AnalysisStatus = new QLabel(ProgressDisplay);
        AnalysisStatus->setObjectName(QString::fromUtf8("AnalysisStatus"));
        AnalysisStatus->setStyleSheet(QString::fromUtf8("background-color: rgb(238, 238, 238);"));

        verticalLayout->addWidget(AnalysisStatus);


        retranslateUi(ProgressDisplay);

        QMetaObject::connectSlotsByName(ProgressDisplay);
    } // setupUi

    void retranslateUi(QDialog *ProgressDisplay)
    {
        ProgressDisplay->setWindowTitle(QApplication::translate("ProgressDisplay", "Dialog", 0, QApplication::UnicodeUTF8));
        AnalysisStatus->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ProgressDisplay: public Ui_ProgressDisplay {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROGRESSDISPLAY_H
