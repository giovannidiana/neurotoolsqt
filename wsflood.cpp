#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include<boost/tuple/tuple.hpp>
#include<boost/array.hpp>
#include<boost/tuple/tuple_io.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include "settings.h"
#include"displayws.h"
#include <QApplication>


using namespace std;
using namespace cv;

double ImageProcessing::Corr(unsigned short* data, size_t a, size_t b,size_t nz){
    double m1=0,m2=0;
    double c=0,s1,s2;
    for(int i=0;i<nz;++i){
        m1+=data[a*nz+i];
        m2+=data[b*nz+i];
    }
    m1/=nz; m2/=nz;
    for(int i=0;i<nz;++i){
        c+=(data[a*nz+i]-m1)*(data[b*nz+i]-m2);
        s1+=pow(data[a*nz+i]-m1,2);
        s2+=pow(data[b*nz+i]-m2,2);
    }

    if(m1>0 && m2>0){
        c=c/sqrt(s1*s2);
    } else c=1e-6;

    return c;
}


double ImageProcessing::matchMoments(Mat& m1,Mat& m2){
    double hu1[7],hu2[7];
    Moments M1=moments(m1);
    Moments M2=moments(m2);
    HuMoments(M1,hu1);
    HuMoments(M2,hu2);
    double dist=0;

    for(unsigned int i=0;i<7;i++){
        dist=max(fabs(hu1[i]-hu2[i]),dist);
    }

    return(dist);
}

bool ImageProcessing::checkShape(Mat& m1,double maxratio,double minsize,double compactness){
    // Compactness must some number lower than 1 but not too low otherwise we are
    // keeping higly non ellipsoidal cells,
    Moments M1=moments(m1);
    double ei[2];
    double area;
    int nonzero=countNonZero(m1);
    ei[0]=(M1.mu02+M1.mu20-sqrt(pow(M1.mu02-M1.mu20,2)+4*pow(M1.mu11,2)))/2;
    ei[1]=(M1.mu02+M1.mu20+sqrt(pow(M1.mu02-M1.mu20,2)+4*pow(M1.mu11,2)))/2;
    area=CV_PI*sqrt(ei[0]*ei[1]);

    if(min(sqrt(ei[0]),sqrt(ei[1]))>minsize &&
       sqrt(ei[1]/ei[0])<maxratio &&
       nonzero/area>compactness){

        return(true);

    } else {

        return(false);

    }
}

void ImageProcessing::GetShape(Mat& m1,double* shape){
    // Compactness must some number lower than 1 but not too low otherwise we are
    // keeping higly non ellipsoidal cells,
    Moments M1=moments(m1);
    double ei[2];
    double area;
    int nonzero=countNonZero(m1);
    ei[0]=(M1.mu02+M1.mu20-sqrt(pow(M1.mu02-M1.mu20,2)+4*pow(M1.mu11,2)))/2;
    ei[1]=(M1.mu02+M1.mu20+sqrt(pow(M1.mu02-M1.mu20,2)+4*pow(M1.mu11,2)))/2;
    area=CV_PI*sqrt(ei[0]*ei[1]);

    shape[0]=(min(sqrt(ei[0]),sqrt(ei[1])));
    shape[1]=(sqrt(ei[1]/ei[0]));
    shape[2]=(nonzero/area);
}

boost::array<double,2> ImageProcessing::getEi(Mat& m1){
    Moments M1=moments(m1);
    boost::array<double,2> pt;
    pt[0]=(M1.mu02+M1.mu20-sqrt(pow(M1.mu02-M1.mu20,2)+4*pow(M1.mu11,2)))/2;
    pt[1]=(M1.mu02+M1.mu20+sqrt(pow(M1.mu02-M1.mu20,2)+4*pow(M1.mu11,2)))/2;

    pt[0]=sqrt(pt[0]);
    pt[1]=sqrt(pt[1]);

    return(pt);
}

double ImageProcessing::matchPattern(Mat& m1,Mat& m2){
    double diff=0;
    for(int i=0;i<m1.cols*m1.rows;++i){
        diff+=pow(m1.at<float>(i)-m2.at<float>(i),2);
    }
    return(sqrt(diff/m1.cols/m1.rows));
}

void ImageProcessing::copyPattern(const Mat& src, Mat &pattern,boost::array<int,2> center,int RAD){
    int i,j;
    for(i=0;i<2*RAD+1;++i){
        for(j=0;j<2*RAD+1;++j){
            if(src.type()==CV_16U){
                pattern.at<ushort>(i,j)=0;
            } else {
                pattern.at<float>(i,j)=0;
            }
        }
    }

    for(i=-RAD;i<=RAD;i++){
        for(j=-RAD;j<=RAD;j++){
            if(center[0]+i>=0 && center[0]+i<src.rows &&
               center[1]+j>=0 && center[1]+j<src.cols){
                if(src.type()==CV_16U){
                    pattern.at<ushort>(i+RAD,j+RAD)=src.at<ushort>(center[0]+i,center[1]+j);
                } else {
                    pattern.at<float>(i+RAD,j+RAD)=src.at<float>(center[0]+i,center[1]+j);
                }
            }
        }
    }
}


void ImageProcessing::wsflood(const Mat &src, Mat &wst, int& ncells,int& ncells_eff, unsigned short* tdata, int nz){

    // The idea is to start from the maximum intensity voxels and
    // propagate the flooding.
    unsigned int i,j,k;
    int r,c,rloc,cloc;
    int ncol=src.cols;
    int nrow=src.rows;
    int f;
    double tol=CX->WSFLOOD_TOL;
    int rad=CX->WSFLOOD_RAD;
    int bsize=CX->WSFLOOD_BLUR;
    int npoints,counter;
    vector<double> tmp_dist;

    Mat kernel = (Mat_<float>(3,3) << 1,  1, 1,
                                      1, -8, 1,
                                      1,  1, 1);
    ushort tmp,ass_tmp,ass_nei;
    Mat mat_tmp(2*rad+1,2*rad+1,CV_16UC1,Scalar(0));
    float* data;
    ushort* out_data;
    vector< boost::array<int,2> > ass_tuple;
    boost::array<int,2> tmp_tup;
    int ass_tuple_tot=0;
    int assign=0;
    bool sw;
    Mat image,image_float;
    Mat imgLaplacian,mask;
    Mat tmp_wst,tmp_image;
    MatIterator_<float> it,end;
    MatIterator_<ushort> it_wst;

    // Convert the source image into float
    src.convertTo(image_float,CV_32F);
    GaussianBlur(src,image,Size(bsize,bsize),0,0);
    GaussianBlur(image_float,image_float_b,Size(bsize,bsize),0,0);

    // select peaks, which have negative laplacian.
    filter2D(image_float_b,imgLaplacian,CV_32F,kernel);
    threshold(imgLaplacian, mask,0,1,THRESH_BINARY_INV);

    // I sharpen the blurred source image
    //image_float_b=image_float_b-imgLaplacian;

    // Convert
    mask.convertTo(mask,CV_8U);

    double maxflu=image.at<float>(0);

    for(it=image_float_b.begin<float>();it!=image_float_b.end<float>();++it) {
        if(*it>maxflu) maxflu=*it;
    }

    // We scan from max to the minimum intensity.
    f=maxflu+tol;
    while(f>CX->WSFLOOD_MINFLU){

        if(progress!=NULL){
            if((int)(maxflu-f)%((int)(maxflu-CX->WSFLOOD_MINFLU)/20)==0){
                progress->updateProgressBar((int)(100.*(maxflu-f)/(maxflu-CX->WSFLOOD_MINFLU)),"scan image");
                QApplication::processEvents(QEventLoop::AllEvents,100);
            }
        }
        f+=-tol;

        //cout<<"Scan fluorescence "<<setw(5)<<f
        //    <<" assignment "<<setw(5)<<assign<<"     \r"<<flush;

        data=(float*)image_float_b.data;
        out_data=(ushort*)wst.data;

        for(i=0;i<ncol*nrow;i++){
            tmp=*data;
            if(/*tmp<=f &&*/ tmp>(f-tol) && mask.at<uchar>(i)==1 && *out_data==0){
                r=i/ncol;
                c=i%ncol;

                tmp_tup[0]=r; tmp_tup[1]=c;
                copyPattern(wst,mat_tmp,tmp_tup,rad);

                ass_tuple.resize(1);
                ass_tuple[0][0]=0;
                ass_tuple[0][1]=0;
                for(it_wst=mat_tmp.begin<ushort>();it_wst!=mat_tmp.end<ushort>();++it_wst){
                    sw=0;
                      if(true){
                        ass_nei=*it_wst;
                        if(ass_nei!=0) {
                            if(ass_tuple[0][0]==0){
                                ass_tuple[0][0]=ass_nei;
                                ass_tuple[0][1]=1;

                            } else {
                                for(k=0;k<ass_tuple.size();++k){
                                    if(ass_nei==ass_tuple[k][0]) {
                                        ass_tuple[k][1]++;
                                        sw=1;
                                        break;
                                    }
                                }

                                if(sw==0){
                                    tmp_tup[0]=ass_nei;
                                    tmp_tup[1]=1;
                                    ass_tuple.push_back(tmp_tup);
                                }
                            }
                        }
                    }
                }

                if(ass_tuple[0][0]==0){
                    assign++;
                    tmp_tup[0]=r; tmp_tup[1]=c;
                    peaks.push_back(tmp_tup);
                    *out_data=assign;
                } else {
                    sort(ass_tuple.begin(),ass_tuple.end(),SortArray());
                    ass_tuple_tot=0;
                    tmp_dist.resize(0);
                    for(k=0;k<ass_tuple.size();++k){
                        ass_tuple_tot+=ass_tuple[k][1];
                        tmp_dist.push_back(sqrt(pow(r-peaks[ass_tuple[k][0]-1][0],2)+
                                                pow(c-peaks[ass_tuple[k][0]-1][1],2)+
                                                CX->WSFLOOD_CORRWEIGHT/Corr(tdata,i,ncol*peaks[ass_tuple[k][0]-1][0]+peaks[ass_tuple[k][0]-1][1],nz)));
                    }
                    if(*min_element(tmp_dist.begin(),tmp_dist.end())<CX->WSFLOOD_MAXMIXDIST){
                        *out_data=ass_tuple[ distance(tmp_dist.begin(),min_element(tmp_dist.begin(),tmp_dist.end()))][0];
                    } else {
                        assign++;
                        tmp_tup[0]=r;tmp_tup[1]=c;
                        peaks.push_back(tmp_tup);
                        *out_data=assign;
                    }
                }


            } // close if(tmp==f)

            // increase pointers
            data++;
            out_data++;
        } // close for on stack
    } // close flooding
    //cout<<endl;

    npoints=assign;

    // These two values will be passed outside
    ncells=npoints;
    ncells_eff=npoints;

    wst.copyTo(wst_nocut);
    nsegments=npoints;
    select.resize(nsegments,1);

    int pattern_size=(CX->WSFLOOD_CELLRAD+2)*2+1;
    tmp_image=Mat::zeros(pattern_size,pattern_size,CV_32F);
    tmp_wst=Mat::zeros(pattern_size,pattern_size,CV_16U);
    Mat test=Mat::zeros(pattern_size,pattern_size,CV_32F);

    vector<double> vec_match(npoints);
    vector<double> size_match(npoints);
    vector<bool> check_vec(npoints);
    vector<ushort> reassign(npoints);

    for(i=1;i<=npoints;i++){
        if(progress!=NULL){
            if((i%(int)(npoints/20.))==0){
                progress->updateProgressBar((int)(100.*i/npoints),"processing cells");
                QApplication::processEvents(QEventLoop::AllEvents,100);
            }
        }
        counter=0;
        copyPattern(wst,tmp_wst,peaks[i-1],CX->WSFLOOD_CELLRAD+2);
        copyPattern(image_float_b,tmp_image,peaks[i-1],CX->WSFLOOD_CELLRAD+2);

        for(j=0;j<tmp_wst.rows*tmp_wst.cols;++j){
            if(tmp_wst.at<ushort>(j)==i){
                test.at<float>(j)=tmp_image.at<float>(j);
                counter++;
            } else test.at<float>(j)=0;
        }

        // checkShape usage: (matrix, maxratio, minsize, compactness)
        check_vec[i-1]=checkShape(test,CX->CHECKSHAPE_MAXRATIO,CX->CHECKSHAPE_MINSIZE,CX->CHECKSHAPE_COMPACTNESS);
        size_match[i-1]=counter;
    }

    counter=0;

    for(it_wst=wst.begin<ushort>(); it_wst!=wst.end<ushort>();++it_wst){
        counter++;
        if(progress!=NULL){
            if((counter % (int)(ncol*nrow/20.))==0) {
                progress->updateProgressBar((int)(counter*100./(ncol*nrow)),"Cleaning");
                QApplication::processEvents(QEventLoop::AllEvents,200);
            }
        }
        i=*it_wst;
        if(i>0){
            if(!check_vec[i-1] || size_match[i-1]>CX->MAXSIZE ){
                select[i-1]=0;
                *it_wst=0;
            }
        }
    }

    for(i=1;i<=npoints;++i){
        if(!check_vec[i-1] || size_match[i-1]>CX->MAXSIZE ) {
            ncells_eff+=-1;
        }
    }

    wst.copyTo(wst_selec);

}

