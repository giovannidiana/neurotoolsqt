/********************************************************************************
** Form generated from reading UI file 'settingsform.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGSFORM_H
#define UI_SETTINGSFORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_SettingsForm
{
public:
    QGroupBox *groupBox;
    QLabel *label;
    QLineEdit *WSFLOOD_MINFLU;
    QLabel *label_2;
    QLineEdit *WSFLOOD_TOL;
    QLabel *label_3;
    QLineEdit *WSFLOOD_RAD;
    QLabel *label_5;
    QLineEdit *WSFLOOD_BLUR;
    QLabel *label_6;
    QLineEdit *WSFLOOD_CELLRAD;
    QLabel *label_16;
    QLineEdit *WSFLOOD_CORRWEIGHT;
    QLabel *label_17;
    QLineEdit *WSFLOOD_MAXMIXDIST;
    QLabel *label_18;
    QComboBox *MIP;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_5;
    QLabel *label_23;
    QLabel *label_22;
    QLineEdit *NNUSED;
    QLineEdit *DENSCL_TH_SLOPE;
    QLineEdit *DENSCL_FREQ;
    QLabel *label_19;
    QLabel *label_20;
    QLineEdit *DENSCL_TH_DIST;
    QLineEdit *DENSCL_TSHIFT;
    QLabel *label_21;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QLabel *label_7;
    QLineEdit *TRAF_BUFFSIZE;
    QLabel *label_8;
    QLineEdit *TRAF_WS;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_4;
    QLabel *label_9;
    QLineEdit *MAXMIN_BINSIZE;
    QLabel *label_10;
    QLineEdit *PATTERN_NUM;
    QLabel *label_15;
    QLineEdit *SNR;
    QLabel *label_12;
    QLineEdit *CHECKSHAPE_MINSIZE;
    QLabel *label_13;
    QLineEdit *CHECKSHAPE_COMPACTNESS;
    QLabel *label_11;
    QLineEdit *CHECKSHAPE_MAXRATIO;
    QLabel *label_14;
    QLineEdit *MAXSIZE;
    QPushButton *OKButton;
    QPushButton *ResetButton;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_3;
    QLabel *label_24;
    QLineEdit *BCLPARAMS_VARX;
    QLabel *label_25;
    QLineEdit *BCLPARAMS_VARB;
    QLabel *label_26;
    QLineEdit *BCLPARAMS_VARS;
    QLabel *label_27;
    QLineEdit *BCLPARAMS_LAMBDA;
    QLabel *label_28;
    QLineEdit *BCLPARAMS_B0DEV;
    QLabel *label_29;
    QLineEdit *BCLPARAMS_PROB;

    void setupUi(QDialog *SettingsForm)
    {
        if (SettingsForm->objectName().isEmpty())
            SettingsForm->setObjectName(QString::fromUtf8("SettingsForm"));
        SettingsForm->resize(593, 681);
        groupBox = new QGroupBox(SettingsForm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 276, 296));
        groupBox->setAutoFillBackground(true);
        groupBox->setStyleSheet(QString::fromUtf8(""));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(18, 26, 130, 17));
        WSFLOOD_MINFLU = new QLineEdit(groupBox);
        WSFLOOD_MINFLU->setObjectName(QString::fromUtf8("WSFLOOD_MINFLU"));
        WSFLOOD_MINFLU->setGeometry(QRect(154, 26, 112, 27));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(18, 59, 55, 17));
        WSFLOOD_TOL = new QLineEdit(groupBox);
        WSFLOOD_TOL->setObjectName(QString::fromUtf8("WSFLOOD_TOL"));
        WSFLOOD_TOL->setGeometry(QRect(154, 59, 112, 27));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(18, 92, 38, 17));
        WSFLOOD_RAD = new QLineEdit(groupBox);
        WSFLOOD_RAD->setObjectName(QString::fromUtf8("WSFLOOD_RAD"));
        WSFLOOD_RAD->setGeometry(QRect(154, 92, 112, 27));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(18, 125, 60, 17));
        WSFLOOD_BLUR = new QLineEdit(groupBox);
        WSFLOOD_BLUR->setObjectName(QString::fromUtf8("WSFLOOD_BLUR"));
        WSFLOOD_BLUR->setGeometry(QRect(154, 125, 112, 27));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(18, 158, 59, 17));
        WSFLOOD_CELLRAD = new QLineEdit(groupBox);
        WSFLOOD_CELLRAD->setObjectName(QString::fromUtf8("WSFLOOD_CELLRAD"));
        WSFLOOD_CELLRAD->setGeometry(QRect(154, 158, 112, 27));
        label_16 = new QLabel(groupBox);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setGeometry(QRect(18, 191, 106, 17));
        WSFLOOD_CORRWEIGHT = new QLineEdit(groupBox);
        WSFLOOD_CORRWEIGHT->setObjectName(QString::fromUtf8("WSFLOOD_CORRWEIGHT"));
        WSFLOOD_CORRWEIGHT->setGeometry(QRect(154, 191, 112, 27));
        label_17 = new QLabel(groupBox);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setGeometry(QRect(18, 224, 75, 17));
        WSFLOOD_MAXMIXDIST = new QLineEdit(groupBox);
        WSFLOOD_MAXMIXDIST->setObjectName(QString::fromUtf8("WSFLOOD_MAXMIXDIST"));
        WSFLOOD_MAXMIXDIST->setGeometry(QRect(154, 224, 112, 27));
        label_18 = new QLabel(groupBox);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setGeometry(QRect(18, 257, 104, 17));
        MIP = new QComboBox(groupBox);
        MIP->setObjectName(QString::fromUtf8("MIP"));
        MIP->setGeometry(QRect(154, 257, 62, 27));
        groupBox_3 = new QGroupBox(SettingsForm);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(310, 290, 250, 195));
        gridLayout_5 = new QGridLayout(groupBox_3);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_23 = new QLabel(groupBox_3);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        gridLayout_5->addWidget(label_23, 4, 0, 1, 1);

        label_22 = new QLabel(groupBox_3);
        label_22->setObjectName(QString::fromUtf8("label_22"));

        gridLayout_5->addWidget(label_22, 3, 0, 1, 1);

        NNUSED = new QLineEdit(groupBox_3);
        NNUSED->setObjectName(QString::fromUtf8("NNUSED"));

        gridLayout_5->addWidget(NNUSED, 4, 1, 1, 1);

        DENSCL_TH_SLOPE = new QLineEdit(groupBox_3);
        DENSCL_TH_SLOPE->setObjectName(QString::fromUtf8("DENSCL_TH_SLOPE"));

        gridLayout_5->addWidget(DENSCL_TH_SLOPE, 2, 1, 1, 1);

        DENSCL_FREQ = new QLineEdit(groupBox_3);
        DENSCL_FREQ->setObjectName(QString::fromUtf8("DENSCL_FREQ"));

        gridLayout_5->addWidget(DENSCL_FREQ, 1, 1, 1, 1);

        label_19 = new QLabel(groupBox_3);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        gridLayout_5->addWidget(label_19, 0, 0, 1, 1);

        label_20 = new QLabel(groupBox_3);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        gridLayout_5->addWidget(label_20, 1, 0, 1, 1);

        DENSCL_TH_DIST = new QLineEdit(groupBox_3);
        DENSCL_TH_DIST->setObjectName(QString::fromUtf8("DENSCL_TH_DIST"));

        gridLayout_5->addWidget(DENSCL_TH_DIST, 3, 1, 1, 1);

        DENSCL_TSHIFT = new QLineEdit(groupBox_3);
        DENSCL_TSHIFT->setObjectName(QString::fromUtf8("DENSCL_TSHIFT"));

        gridLayout_5->addWidget(DENSCL_TSHIFT, 0, 1, 1, 1);

        label_21 = new QLabel(groupBox_3);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        gridLayout_5->addWidget(label_21, 2, 0, 1, 1);

        groupBox_2 = new QGroupBox(SettingsForm);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(20, 310, 258, 96));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_7 = new QLabel(groupBox_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout_2->addWidget(label_7, 0, 0, 1, 1);

        TRAF_BUFFSIZE = new QLineEdit(groupBox_2);
        TRAF_BUFFSIZE->setObjectName(QString::fromUtf8("TRAF_BUFFSIZE"));

        gridLayout_2->addWidget(TRAF_BUFFSIZE, 0, 1, 1, 1);

        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_2->addWidget(label_8, 1, 0, 1, 1);

        TRAF_WS = new QLineEdit(groupBox_2);
        TRAF_WS->setObjectName(QString::fromUtf8("TRAF_WS"));

        gridLayout_2->addWidget(TRAF_WS, 1, 1, 1, 1);

        groupBox_4 = new QGroupBox(SettingsForm);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(320, 10, 232, 261));
        gridLayout_4 = new QGridLayout(groupBox_4);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        label_9 = new QLabel(groupBox_4);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_4->addWidget(label_9, 0, 0, 1, 1);

        MAXMIN_BINSIZE = new QLineEdit(groupBox_4);
        MAXMIN_BINSIZE->setObjectName(QString::fromUtf8("MAXMIN_BINSIZE"));

        gridLayout_4->addWidget(MAXMIN_BINSIZE, 0, 1, 1, 1);

        label_10 = new QLabel(groupBox_4);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_4->addWidget(label_10, 1, 0, 1, 1);

        PATTERN_NUM = new QLineEdit(groupBox_4);
        PATTERN_NUM->setObjectName(QString::fromUtf8("PATTERN_NUM"));

        gridLayout_4->addWidget(PATTERN_NUM, 1, 1, 1, 1);

        label_15 = new QLabel(groupBox_4);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_4->addWidget(label_15, 2, 0, 1, 1);

        SNR = new QLineEdit(groupBox_4);
        SNR->setObjectName(QString::fromUtf8("SNR"));

        gridLayout_4->addWidget(SNR, 2, 1, 1, 1);

        label_12 = new QLabel(groupBox_4);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout_4->addWidget(label_12, 3, 0, 1, 1);

        CHECKSHAPE_MINSIZE = new QLineEdit(groupBox_4);
        CHECKSHAPE_MINSIZE->setObjectName(QString::fromUtf8("CHECKSHAPE_MINSIZE"));

        gridLayout_4->addWidget(CHECKSHAPE_MINSIZE, 3, 1, 1, 1);

        label_13 = new QLabel(groupBox_4);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_4->addWidget(label_13, 4, 0, 1, 1);

        CHECKSHAPE_COMPACTNESS = new QLineEdit(groupBox_4);
        CHECKSHAPE_COMPACTNESS->setObjectName(QString::fromUtf8("CHECKSHAPE_COMPACTNESS"));

        gridLayout_4->addWidget(CHECKSHAPE_COMPACTNESS, 4, 1, 1, 1);

        label_11 = new QLabel(groupBox_4);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout_4->addWidget(label_11, 5, 0, 1, 1);

        CHECKSHAPE_MAXRATIO = new QLineEdit(groupBox_4);
        CHECKSHAPE_MAXRATIO->setObjectName(QString::fromUtf8("CHECKSHAPE_MAXRATIO"));

        gridLayout_4->addWidget(CHECKSHAPE_MAXRATIO, 5, 1, 1, 1);

        label_14 = new QLabel(groupBox_4);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_4->addWidget(label_14, 6, 0, 1, 1);

        MAXSIZE = new QLineEdit(groupBox_4);
        MAXSIZE->setObjectName(QString::fromUtf8("MAXSIZE"));

        gridLayout_4->addWidget(MAXSIZE, 6, 1, 1, 1);

        OKButton = new QPushButton(SettingsForm);
        OKButton->setObjectName(QString::fromUtf8("OKButton"));
        OKButton->setGeometry(QRect(450, 620, 85, 27));
        ResetButton = new QPushButton(SettingsForm);
        ResetButton->setObjectName(QString::fromUtf8("ResetButton"));
        ResetButton->setGeometry(QRect(360, 620, 85, 27));
        groupBox_5 = new QGroupBox(SettingsForm);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(90, 430, 187, 228));
        gridLayout_3 = new QGridLayout(groupBox_5);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_24 = new QLabel(groupBox_5);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        gridLayout_3->addWidget(label_24, 0, 0, 1, 1);

        BCLPARAMS_VARX = new QLineEdit(groupBox_5);
        BCLPARAMS_VARX->setObjectName(QString::fromUtf8("BCLPARAMS_VARX"));

        gridLayout_3->addWidget(BCLPARAMS_VARX, 0, 1, 1, 1);

        label_25 = new QLabel(groupBox_5);
        label_25->setObjectName(QString::fromUtf8("label_25"));

        gridLayout_3->addWidget(label_25, 1, 0, 1, 1);

        BCLPARAMS_VARB = new QLineEdit(groupBox_5);
        BCLPARAMS_VARB->setObjectName(QString::fromUtf8("BCLPARAMS_VARB"));

        gridLayout_3->addWidget(BCLPARAMS_VARB, 1, 1, 1, 1);

        label_26 = new QLabel(groupBox_5);
        label_26->setObjectName(QString::fromUtf8("label_26"));

        gridLayout_3->addWidget(label_26, 2, 0, 1, 1);

        BCLPARAMS_VARS = new QLineEdit(groupBox_5);
        BCLPARAMS_VARS->setObjectName(QString::fromUtf8("BCLPARAMS_VARS"));

        gridLayout_3->addWidget(BCLPARAMS_VARS, 2, 1, 1, 1);

        label_27 = new QLabel(groupBox_5);
        label_27->setObjectName(QString::fromUtf8("label_27"));

        gridLayout_3->addWidget(label_27, 3, 0, 1, 1);

        BCLPARAMS_LAMBDA = new QLineEdit(groupBox_5);
        BCLPARAMS_LAMBDA->setObjectName(QString::fromUtf8("BCLPARAMS_LAMBDA"));

        gridLayout_3->addWidget(BCLPARAMS_LAMBDA, 3, 1, 1, 1);

        label_28 = new QLabel(groupBox_5);
        label_28->setObjectName(QString::fromUtf8("label_28"));

        gridLayout_3->addWidget(label_28, 4, 0, 1, 1);

        BCLPARAMS_B0DEV = new QLineEdit(groupBox_5);
        BCLPARAMS_B0DEV->setObjectName(QString::fromUtf8("BCLPARAMS_B0DEV"));

        gridLayout_3->addWidget(BCLPARAMS_B0DEV, 4, 1, 1, 1);

        label_29 = new QLabel(groupBox_5);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        gridLayout_3->addWidget(label_29, 5, 0, 1, 1);

        BCLPARAMS_PROB = new QLineEdit(groupBox_5);
        BCLPARAMS_PROB->setObjectName(QString::fromUtf8("BCLPARAMS_PROB"));

        gridLayout_3->addWidget(BCLPARAMS_PROB, 5, 1, 1, 1);


        retranslateUi(SettingsForm);

        QMetaObject::connectSlotsByName(SettingsForm);
    } // setupUi

    void retranslateUi(QDialog *SettingsForm)
    {
        SettingsForm->setWindowTitle(QApplication::translate("SettingsForm", "Parameters", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("SettingsForm", "Segmentation ", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SettingsForm", "Minimum fluorescence", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("SettingsForm", "Tolerance", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("SettingsForm", "Radius", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("SettingsForm", "Blur range", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("SettingsForm", "Cell radius", 0, QApplication::UnicodeUTF8));
        label_16->setText(QApplication::translate("SettingsForm", "Correlation weight", 0, QApplication::UnicodeUTF8));
        label_17->setText(QApplication::translate("SettingsForm", "Max distance", 0, QApplication::UnicodeUTF8));
        label_18->setText(QApplication::translate("SettingsForm", "2D projection type", 0, QApplication::UnicodeUTF8));
        MIP->clear();
        MIP->insertItems(0, QStringList()
         << QApplication::translate("SettingsForm", "MEAN", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SettingsForm", "MIP", 0, QApplication::UnicodeUTF8)
        );
        groupBox_3->setTitle(QApplication::translate("SettingsForm", "Clustering", 0, QApplication::UnicodeUTF8));
        label_23->setText(QApplication::translate("SettingsForm", "kNN", 0, QApplication::UnicodeUTF8));
        label_22->setText(QApplication::translate("SettingsForm", "Distance threshold", 0, QApplication::UnicodeUTF8));
        label_19->setText(QApplication::translate("SettingsForm", "Time shift", 0, QApplication::UnicodeUTF8));
        label_20->setText(QApplication::translate("SettingsForm", "Frequency", 0, QApplication::UnicodeUTF8));
        label_21->setText(QApplication::translate("SettingsForm", "Slope threshold", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("SettingsForm", "Detrending", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("SettingsForm", "Buffer size", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("SettingsForm", "Moving window size", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("SettingsForm", "Selection", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("SettingsForm", "MaxMin binsize", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("SettingsForm", "Patterns", 0, QApplication::UnicodeUTF8));
        label_15->setText(QApplication::translate("SettingsForm", "SNR", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("SettingsForm", "Minimum size", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("SettingsForm", "Compactness", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("SettingsForm", "Maximum ratio", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("SettingsForm", "Maximum size", 0, QApplication::UnicodeUTF8));
        OKButton->setText(QApplication::translate("SettingsForm", "OK", 0, QApplication::UnicodeUTF8));
        ResetButton->setText(QApplication::translate("SettingsForm", "Reset", 0, QApplication::UnicodeUTF8));
        groupBox_5->setTitle(QApplication::translate("SettingsForm", "Bayesian signal extraction", 0, QApplication::UnicodeUTF8));
        label_24->setText(QApplication::translate("SettingsForm", "varX", 0, QApplication::UnicodeUTF8));
        label_25->setText(QApplication::translate("SettingsForm", "varB", 0, QApplication::UnicodeUTF8));
        label_26->setText(QApplication::translate("SettingsForm", "varS", 0, QApplication::UnicodeUTF8));
        label_27->setText(QApplication::translate("SettingsForm", "lambda", 0, QApplication::UnicodeUTF8));
        label_28->setText(QApplication::translate("SettingsForm", "B0dev", 0, QApplication::UnicodeUTF8));
        label_29->setText(QApplication::translate("SettingsForm", "prob", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SettingsForm: public Ui_SettingsForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSFORM_H
