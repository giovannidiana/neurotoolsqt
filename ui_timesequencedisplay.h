/********************************************************************************
** Form generated from reading UI file 'timesequencedisplay.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TIMESEQUENCEDISPLAY_H
#define UI_TIMESEQUENCEDISPLAY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QVBoxLayout>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_TimeSequenceDisplay
{
public:
    QVBoxLayout *verticalLayout;
    QCustomPlot *traj;

    void setupUi(QDialog *TimeSequenceDisplay)
    {
        if (TimeSequenceDisplay->objectName().isEmpty())
            TimeSequenceDisplay->setObjectName(QString::fromUtf8("TimeSequenceDisplay"));
        TimeSequenceDisplay->resize(1000, 183);
        verticalLayout = new QVBoxLayout(TimeSequenceDisplay);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        traj = new QCustomPlot(TimeSequenceDisplay);
        traj->setObjectName(QString::fromUtf8("traj"));

        verticalLayout->addWidget(traj);


        retranslateUi(TimeSequenceDisplay);

        QMetaObject::connectSlotsByName(TimeSequenceDisplay);
    } // setupUi

    void retranslateUi(QDialog *TimeSequenceDisplay)
    {
        TimeSequenceDisplay->setWindowTitle(QApplication::translate("TimeSequenceDisplay", "Dialog", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TimeSequenceDisplay: public Ui_TimeSequenceDisplay {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TIMESEQUENCEDISPLAY_H
