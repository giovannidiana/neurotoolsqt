#include "bcl.h"

#include<iostream>
#include"bcl.h"
#include<armadillo>

using namespace std;
using namespace arma;

BCLparams::BCLparams(){
    varX=0.002;
    varS=4e-4;
    varB=6e-4;
    prob=0.04;
    lambda=0.6;
    b0dev=0.6;
}

BCLparams::BCLparams(const BCLparams &par){
    varX=par.varX;
    varS=par.varS;
    varB=par.varB;
    prob=par.prob;
    lambda=par.lambda;
    b0dev=par.b0dev;
}

BCLparams::BCLparams(settings *CX){
    varX=CX->BCLPARAMS_VARX;
    varS=CX->BCLPARAMS_VARS;
    varB=CX->BCLPARAMS_VARB;
    prob=CX->BCLPARAMS_PROB;
    lambda=CX->BCLPARAMS_LAMBDA;
    b0dev=CX->BCLPARAMS_B0DEV;
}

void BCLparams::print(){
    std::cout<<"varX = "<<varX<<std::endl;
    std::cout<<"varS = "<<varS<<std::endl;
    std::cout<<"varB = "<<varB<<std::endl;
    std::cout<<"prob = "<<prob<<std::endl;
    std::cout<<"lambda = "<<lambda<<std::endl;
    std::cout<<"b0dev = "<<b0dev<<std::endl<<std::endl;
}

void BCLparams::varX_from_data(const std::vector<double> &x)
{
    unsigned int k;
    unsigned int counter=0;
    double var=0;

    for(k=1;k<x.size();++k){
       if(x[k]-x[k-1]<0){
           counter++;
           var+=pow(x[k]-x[k-1],2);
       }
    }

    varX=(var/counter);
}

bcl_type::bcl_type(size_t s,double f) :
    orig(NULL), IS_EMPTY(true)
{
    size=s;
    cal.resize(s);
    sks.resize(s);
    B.resize(s);
    orig_norm.resize(s);
    min=0;
    max=0;
    freq=f;

    for(size_t i=0; i<s;i++){
        cal[i]=0; sks[i]=0; B[i]=0;
    }

}

void bcl_type::setData(double* data){

    double normalization=0;

    if(data!=NULL) {
        IS_EMPTY=false;
        orig=data;
        for(size_t i=0; i<size;i++)
            normalization+=orig[i]/size;

        for(size_t i=0; i<size;i++) orig_norm[i]=orig[i]/normalization;

        min=*min_element(orig_norm.begin(),orig_norm.end());
        max=*max_element(orig_norm.begin(),orig_norm.end());
    }
}

int bcl_type::bcl(const BCLparams &params,int Mode){

    if(orig==NULL) return 0;

    int k;
    double* y=&orig_norm[0];


    //B[0]=(max*params.b0dev+(1-params.b0dev)*min);
    B[0]=0;
    for(k=0;k<200;k++) B[0]+=orig_norm[k]/200.;

    cal[0]=0;

    double cnew;
    double Bnew;
    double cspike;
    double Bspike;
    double logp0,logp1;
    double pi=datum::pi;

    for(int t=1;t<size;++t){
        cnew=cal[t-1]*exp(-params.lambda/freq); // dt=1/freq
        Bnew=(params.varX*B[t-1]+params.varB*(y[t]-cnew))/(params.varX+params.varB);
        logp0=log(1-params.prob)-log(2*pi)-0.5*log(params.varX)-0.5*log(params.varB)-pow(Bnew-B[t-1],2)/(2*params.varB)-pow(y[t]-Bnew-cnew,2)/(2*params.varX);

        if(Mode==0){ //GAUS
            cspike=(params.varS*(y[t]-B[t-1])+cnew*(params.varX+params.varB))/(params.varX+params.varS+params.varB);
            Bspike=B[t-1]+params.varB/params.varS*(cspike-cnew);
            logp1=log(params.prob)-0.5*log(pow(2*pi,3))-0.5*log(params.varX)-0.5*log(params.varB)-0.5*log(params.varS)-pow(Bspike-B[t-1],2)/(2*params.varB)*(1+params.varX/params.varB+params.varS/params.varB);
        } else if (Mode==1){ //EXP
            cspike=y[t]-B[t-1]-params.varS*(params.varB+params.varX);
            Bspike=B[t-1]+params.varS*params.varB;
            logp1=log(params.prob)-log(2*pi)-0.5*log(params.varX)-0.5*log(params.varB)+log(params.varS)-pow(Bspike-B[t-1],2)/(2*params.varB)+pow(Bspike+cspike-y[t],2)/(2*params.varX) - params.varS*(cspike-cnew) ;
        }


        if(logp1<logp0 || cspike<cnew){
            cal[t]=cnew;
            B[t]=Bnew;
            sks[t]=0;

        } else {
            cal[t]=cspike;
            B[t]=Bspike;
            sks[t]=1;

        }
    }

    return 1;
}

int bcl_type::bclEM(const BCLparams &par,int Mode,int nsteps){

    BCLparams params(par);

    int i,k,spikesum;
    double meanS,varS;
    double meanB,varB;
    double meanX,varX;

    for(i=0;i<nsteps;++i){

        bcl(params,Mode);

        spikesum=0;
        for(k=0;k<size;++k) spikesum+=sks[k];

        //meanX=0;for(k=0;k<size;++k) meanX+=(orig[k]-cal[k]-B[k])/(size);
        varX=0;for(k=0;k<size;++k) varX+=pow(orig_norm[k]-cal[k]-B[k],2)/(size);
        params.varX=varX;


        //meanB=0;for(k=1;k<size;++k) meanB+=(B[k]-B[k-1])/(size-1);
        varB=0;for(k=1;k<size;++k) varB+=pow(B[k]-B[k-1],2)/(size-1);
        params.varB=varB;

        if(spikesum>0){            
            if(Mode==0){
                meanS=0;
                varS=0; for(k=1;k<size;++k) varS+=pow((cal[k]-cal[k-1]*exp(-params.lambda/freq))*sks[k],2)/spikesum;
                params.varS=varS;
            } else if(Mode==1) {
                meanS=0; for(k=1;k<size;++k) meanS+=(cal[k]-cal[k-1]*exp(-params.lambda/freq))*sks[k]/spikesum;
                params.varS=1.0/meanS;
            }
            params.prob=(double)spikesum/size;
        } else {
            params.varS=0;
            params.prob=1.0/size;
        }

    }

    return 0;

}


          /*
bcl.ml<-function(y,varB0,varC0, varX0,pp0=0.4,lam0=.001){
    #q90=quantile(y,0.5)
    #varX0=var(y[y<q90])
    layout(matrix(1:2,ncol=1))
    pp=pp0
    varC=varC0
    varB=varB0
    varX=varX0
    lam=lam0
    par(ask=T)
    cat("Probe_decay"," ","noise"," ","Baseline_variance"," ","Calcium_variance"," ","spike_frequency"," ","log-likelihood","\n")
    for(i in 1:30){
        trace=bcl(y,pp,lam,varB,varC,varX);
        cat(lam,' ',varX,' ',varB,' ',varC,' ',pp,' ',trace[[4]],"\n")
        plot(y,type='l')
        lines(trace[[2]]+trace[[1]],col="red")
        lines(trace[[2]],col="blue")
        plot(trace[[1]],type='l')
        points(x=(1:length(y))[trace[[3]]==1],y=rep(0,length(y))[trace[[3]]==1],col="red",cex=.2,pch=19);

        #pp=sum(trace[[3]])/(length(y))
        varB_s1=mean(((trace[[2]][2:length(y)]-trace[[2]][1:(length(y)-1)])[trace[[3]][2:length(y)]==1])^2)
        varB_s0=mean(((trace[[2]][2:length(y)]-trace[[2]][1:(length(y)-1)])[trace[[3]][2:length(y)]==0])^2)
        varB_tot=mean(((trace[[2]][2:length(y)]-trace[[2]][1:(length(y)-1)]))^2)

        #varX=varX*exp(0.01*i)
        #varC=varC*exp(0.01*i)
        #varB=0.5*(varB_tot+sqrt(varB_tot^2+4*(2*(1-pp)*varX*varB_s0+2*pp*(varX+varC)*varB_s1)))
        if(sum(trace[[3]])>0) varC=mean(((trace[[1]][2:length(y)]-trace[[1]][1:(length(y)-1)]*exp(-lam))[trace[[3]][2:length(y)]==1])^2)
    }
}

*/
