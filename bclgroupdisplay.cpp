#include "bclgroupdisplay.h"
#include "ui_bclgroupdisplay.h"
#include "bcl.h"

BCLGroupDisplay::BCLGroupDisplay(std::vector<std::vector<double> > &datay,std::vector<int> selection, double f,BCLparams &par, QWidget *parent, int x0, int y0) :
    QDialog(parent),
    ui(new Ui::BCLGroupDisplay)
{
    ui->setupUi(this);

    cplist.resize(10);
    cplist[0]=ui->plot_1;
    cplist[1]=ui->plot_2;
    cplist[2]=ui->plot_3;
    cplist[3]=ui->plot_4;
    cplist[4]=ui->plot_5;
    cplist[5]=ui->plot_6;
    cplist[6]=ui->plot_7;
    cplist[7]=ui->plot_8;
    cplist[8]=ui->plot_9;
    cplist[9]=ui->plot_10;

    move(x0,y0);

    responsematrix=&datay;
    selectvec=&selection;
    n=datay[0].size();
    ncells=datay.size();
    y.resize(10);
    bclres.resize(10,bcl_type(n,f));

    bclpar=&par;
    ui->slider->setValue((int)((log10(bclpar->varX)+5)*ui->slider->maximum()/6.));
    ui->value->setText(QString::number(bclpar->varX));

    for(unsigned int k=0;k<10;++k){
        cplist[k]->addGraph();
        cplist[k]->addGraph();
        cplist[k]->addGraph();
    }

    // initialize the sampled cells to be desplayed
    std::vector<int> indexes(10);

    for(unsigned int k=0;k<10;++k){
        do {
            indexes[k]=qrand() % ncells;
        } while(selectvec->at(indexes[k])==0);

        y[k]=&(responsematrix->at(indexes[k])[0]);
        bclres[k].setData(y[k]);
    }

    plot();

}

BCLGroupDisplay::~BCLGroupDisplay()
{
    delete ui;
}

void BCLGroupDisplay::plot()
{

    unsigned int k;
    unsigned int k_data;


    for(k=0;k<10;++k){

        //int good=bclres[k].bclEM(*bclpar,0,2);
        BCLparams bclpar_copy(*bclpar);
        bclpar_copy.varX_from_data(bclres[k].orig_norm);
        //int good=bclres[k].bcl(*bclpar,1);
        int good=bclres[k].bcl(bclpar_copy,1);

        QVector<double> xx(n),calcium(n),baseline(n);
        QVector<double> activity;

        for(unsigned int i=0;i<n;++i) {
            xx[i]=i;
            calcium[i]=bclres[k].cal[i];
            baseline[i]=bclres[k].B[i];
            if(bclres[k].sks[i]==1){
                activity.push_back(i);
            }
        }

        cplist[k]->xAxis->setRange(0, n);
        cplist[k]->yAxis->setRange(*std::min_element(bclres[k].orig_norm.begin(),bclres[k].orig_norm.end()),
                                   *std::max_element(bclres[k].orig_norm.begin(),bclres[k].orig_norm.end()));

        cplist[k]->graph(0)->setData(xx,QVector<double>::fromStdVector(bclres[k].orig_norm));

        QVector<double> yactivity(activity.size(),cplist[k]->yAxis->range().lower+0.1);
        cplist[k]->graph(1)->setPen(QPen(Qt::red));
        cplist[k]->graph(1)->setData(activity,yactivity);
        cplist[k]->graph(1)->setLineStyle(QCPGraph::lsNone);
        cplist[k]->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 5));

        cplist[k]->graph(2)->setPen(QPen(QColor(255,105,0,255),3));
        cplist[k]->graph(2)->setData(xx,QVector<double>::fromStdVector(bclres[k].B));

        cplist[k]->replot();
    }
}

void BCLGroupDisplay::setTitle(QString title)
{
    setWindowTitle(title);
}

void BCLGroupDisplay::on_slider_sliderMoved(int value)
{
    int ind=ui->variable->currentIndex();
    double expval=pow(10.,-5+(double)value/ui->slider->maximum()*(1+5));
    double probval=(double)value/ui->slider->maximum();
    double lambdaval=(double)value/ui->slider->maximum()*2.;
    switch (ind) {

    case 0: // varX
        bclpar->varX=expval;
        ui->value->setText(QString::number(bclpar->varX));
        break;

    case 1: //var S
        bclpar->varS=expval;
        ui->value->setText(QString::number(bclpar->varS));
        break;

    case 2:
        bclpar->varB=expval;
         ui->value->setText(QString::number(bclpar->varB));
        break;

    case 3:
        bclpar->prob=probval;
         ui->value->setText(QString::number(bclpar->prob));
        break;

    case 4:
        bclpar->lambda=lambdaval;
        ui->value->setText(QString::number(bclpar->lambda));
        break;

    case 5:
        bclpar->b0dev=expval;
        ui->value->setText(QString::number(bclpar->b0dev));
        break;

    }

    plot();

}

void BCLGroupDisplay::on_variable_currentIndexChanged(int index)
{

    switch(index){
    case 0: //varX
        ui->slider->setValue((int)((log10(bclpar->varX)+5)*ui->slider->maximum()/6.));
        ui->value->setText(QString::number(bclpar->varX));
        break;
    case 1: //varS
        ui->slider->setValue((int)((log10(bclpar->varS)+5)*ui->slider->maximum()/6.));
        ui->value->setText(QString::number(bclpar->varS));
        break;
    case 2: //varB
        ui->slider->setValue((int)((log10(bclpar->varB)+5)*ui->slider->maximum()/6.));
        ui->value->setText(QString::number(bclpar->varB));
        break;
    case 3: //prob
        ui->slider->setValue((int)(bclpar->prob*ui->slider->maximum()));
        ui->value->setText(QString::number(bclpar->prob));
        break;
    case 4: //lambda
        ui->slider->setValue((int)(bclpar->lambda*ui->slider->maximum()/2.));
        ui->value->setText(QString::number(bclpar->lambda));
        break;
    case 5: //b0dev
        ui->slider->setValue((int)((log10(bclpar->b0dev)+5)*ui->slider->maximum()/6));
        ui->value->setText(QString::number(bclpar->b0dev));
        break;
    }
}

void BCLGroupDisplay::on_Resample_clicked()
{

    unsigned int k;

    std::vector<int> indexes(10);

    for(k=0;k<10;++k){
        do {
            indexes[k]=qrand() % ncells;
        } while(selectvec->at(indexes[k])==0);

        y[k]=&(responsematrix->at(indexes[k])[0]);
        bclres[k].setData(y[k]);
    }

    plot();
}
