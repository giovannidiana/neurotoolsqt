/********************************************************************************
** Form generated from reading UI file 'bclgroupdisplay.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BCLGROUPDISPLAY_H
#define UI_BCLGROUPDISPLAY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_BCLGroupDisplay
{
public:
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QCustomPlot *plot_9;
    QCustomPlot *plot_8;
    QCustomPlot *plot_3;
    QCustomPlot *plot_1;
    QCustomPlot *plot_7;
    QComboBox *variable;
    QCustomPlot *plot_10;
    QCustomPlot *plot_5;
    QSlider *slider;
    QCustomPlot *plot_4;
    QCustomPlot *plot_2;
    QLabel *value;
    QCustomPlot *plot_6;
    QPushButton *Resample;

    void setupUi(QDialog *BCLGroupDisplay)
    {
        if (BCLGroupDisplay->objectName().isEmpty())
            BCLGroupDisplay->setObjectName(QString::fromUtf8("BCLGroupDisplay"));
        BCLGroupDisplay->resize(494, 750);
        gridLayout_2 = new QGridLayout(BCLGroupDisplay);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        plot_9 = new QCustomPlot(BCLGroupDisplay);
        plot_9->setObjectName(QString::fromUtf8("plot_9"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(plot_9->sizePolicy().hasHeightForWidth());
        plot_9->setSizePolicy(sizePolicy);

        gridLayout->addWidget(plot_9, 8, 0, 1, 3);

        plot_8 = new QCustomPlot(BCLGroupDisplay);
        plot_8->setObjectName(QString::fromUtf8("plot_8"));
        sizePolicy.setHeightForWidth(plot_8->sizePolicy().hasHeightForWidth());
        plot_8->setSizePolicy(sizePolicy);

        gridLayout->addWidget(plot_8, 7, 0, 1, 3);

        plot_3 = new QCustomPlot(BCLGroupDisplay);
        plot_3->setObjectName(QString::fromUtf8("plot_3"));
        sizePolicy.setHeightForWidth(plot_3->sizePolicy().hasHeightForWidth());
        plot_3->setSizePolicy(sizePolicy);

        gridLayout->addWidget(plot_3, 2, 0, 1, 3);

        plot_1 = new QCustomPlot(BCLGroupDisplay);
        plot_1->setObjectName(QString::fromUtf8("plot_1"));
        sizePolicy.setHeightForWidth(plot_1->sizePolicy().hasHeightForWidth());
        plot_1->setSizePolicy(sizePolicy);

        gridLayout->addWidget(plot_1, 0, 0, 1, 3);

        plot_7 = new QCustomPlot(BCLGroupDisplay);
        plot_7->setObjectName(QString::fromUtf8("plot_7"));
        sizePolicy.setHeightForWidth(plot_7->sizePolicy().hasHeightForWidth());
        plot_7->setSizePolicy(sizePolicy);

        gridLayout->addWidget(plot_7, 6, 0, 1, 3);

        variable = new QComboBox(BCLGroupDisplay);
        variable->setObjectName(QString::fromUtf8("variable"));

        gridLayout->addWidget(variable, 10, 0, 1, 1);

        plot_10 = new QCustomPlot(BCLGroupDisplay);
        plot_10->setObjectName(QString::fromUtf8("plot_10"));
        sizePolicy.setHeightForWidth(plot_10->sizePolicy().hasHeightForWidth());
        plot_10->setSizePolicy(sizePolicy);

        gridLayout->addWidget(plot_10, 9, 0, 1, 3);

        plot_5 = new QCustomPlot(BCLGroupDisplay);
        plot_5->setObjectName(QString::fromUtf8("plot_5"));
        sizePolicy.setHeightForWidth(plot_5->sizePolicy().hasHeightForWidth());
        plot_5->setSizePolicy(sizePolicy);

        gridLayout->addWidget(plot_5, 4, 0, 1, 3);

        slider = new QSlider(BCLGroupDisplay);
        slider->setObjectName(QString::fromUtf8("slider"));
        slider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(slider, 10, 2, 1, 1);

        plot_4 = new QCustomPlot(BCLGroupDisplay);
        plot_4->setObjectName(QString::fromUtf8("plot_4"));
        sizePolicy.setHeightForWidth(plot_4->sizePolicy().hasHeightForWidth());
        plot_4->setSizePolicy(sizePolicy);

        gridLayout->addWidget(plot_4, 3, 0, 1, 3);

        plot_2 = new QCustomPlot(BCLGroupDisplay);
        plot_2->setObjectName(QString::fromUtf8("plot_2"));
        sizePolicy.setHeightForWidth(plot_2->sizePolicy().hasHeightForWidth());
        plot_2->setSizePolicy(sizePolicy);

        gridLayout->addWidget(plot_2, 1, 0, 1, 3);

        value = new QLabel(BCLGroupDisplay);
        value->setObjectName(QString::fromUtf8("value"));
        value->setMinimumSize(QSize(100, 0));

        gridLayout->addWidget(value, 10, 1, 1, 1);

        plot_6 = new QCustomPlot(BCLGroupDisplay);
        plot_6->setObjectName(QString::fromUtf8("plot_6"));
        sizePolicy.setHeightForWidth(plot_6->sizePolicy().hasHeightForWidth());
        plot_6->setSizePolicy(sizePolicy);

        gridLayout->addWidget(plot_6, 5, 0, 1, 3);

        Resample = new QPushButton(BCLGroupDisplay);
        Resample->setObjectName(QString::fromUtf8("Resample"));

        gridLayout->addWidget(Resample, 11, 0, 1, 3);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);


        retranslateUi(BCLGroupDisplay);

        QMetaObject::connectSlotsByName(BCLGroupDisplay);
    } // setupUi

    void retranslateUi(QDialog *BCLGroupDisplay)
    {
        BCLGroupDisplay->setWindowTitle(QApplication::translate("BCLGroupDisplay", "Dialog", 0, QApplication::UnicodeUTF8));
        variable->clear();
        variable->insertItems(0, QStringList()
         << QApplication::translate("BCLGroupDisplay", "var X", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("BCLGroupDisplay", "var S", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("BCLGroupDisplay", "var B", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("BCLGroupDisplay", "prob", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("BCLGroupDisplay", "lambda", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("BCLGroupDisplay", "b0dev", 0, QApplication::UnicodeUTF8)
        );
        value->setText(QString());
        Resample->setText(QApplication::translate("BCLGroupDisplay", "Resample", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class BCLGroupDisplay: public Ui_BCLGroupDisplay {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BCLGROUPDISPLAY_H
