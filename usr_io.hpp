#ifndef USR_IO_HPP
#define USR_IO_HPP

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <nifti1_io.h>

//typedef signed short MY_DATATYPE;

#define MIN_HEADER_SIZE 348
#define NII_HEADER_SIZE 352

using namespace std;
template<class TYPE>
void write_nii(TYPE* data, string nii_file,size_t xdim,size_t ydim,size_t zdim,int datatype){

    FILE* nii;
    nii=fopen(nii_file.c_str(),"w");

    nifti_1_header hdr;
    nifti1_extender pad={0,0,0,0};

    memset((void *)&hdr,0, sizeof(hdr));

    hdr.sizeof_hdr = MIN_HEADER_SIZE;
    hdr.dim[0] = 3;
    hdr.dim[1] = xdim;
    hdr.dim[2] = ydim;
    hdr.dim[3] = zdim;
    hdr.dim[4] = 1;
    switch ( datatype ){
        case 4:
            hdr.datatype = DT_SIGNED_SHORT;
            hdr.bitpix = 16;
            break;
        case 8:
            hdr.datatype = DT_SIGNED_INT;
            hdr.bitpix = 32;
            break;
        case 16:
            hdr.datatype = DT_FLOAT;
            hdr.bitpix = 32;
            break;
        case 512:
            hdr.datatype = DT_UINT16;
            hdr.bitpix = 16;
            break;
        case 64:
            hdr.datatype = DT_DOUBLE;
            hdr.bitpix = 64;
            break;
    }
    hdr.pixdim[1] = 1.0;
    hdr.pixdim[2] = 1.0;
    hdr.pixdim[3] = 1.0;
    hdr.pixdim[4] = 1.5;

    hdr.vox_offset = (float) NII_HEADER_SIZE;

    hdr.scl_slope = 100.0;
    hdr.xyzt_units = NIFTI_UNITS_MM | NIFTI_UNITS_SEC;
    strncpy(hdr.magic, "n+1\0", 4);

    fwrite(&hdr, MIN_HEADER_SIZE, 1, nii);

    fwrite(&pad, 4, 1, nii);

    fwrite(data, (size_t)(hdr.bitpix/8), xdim*ydim*zdim, nii);

    fclose(nii);
}


#endif // USR_IO_HPP

