/********************************************************************************
** Form generated from reading UI file 'bcldisplay.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BCLDISPLAY_H
#define UI_BCLDISPLAY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSlider>
#include <QtGui/QVBoxLayout>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_BCLDisplay
{
public:
    QVBoxLayout *verticalLayout;
    QCustomPlot *display;
    QHBoxLayout *horizontalLayout;
    QComboBox *variable;
    QLabel *value;
    QSlider *slider;

    void setupUi(QDialog *BCLDisplay)
    {
        if (BCLDisplay->objectName().isEmpty())
            BCLDisplay->setObjectName(QString::fromUtf8("BCLDisplay"));
        BCLDisplay->resize(1000, 453);
        verticalLayout = new QVBoxLayout(BCLDisplay);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        display = new QCustomPlot(BCLDisplay);
        display->setObjectName(QString::fromUtf8("display"));
        display->setMinimumSize(QSize(0, 400));

        verticalLayout->addWidget(display);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        variable = new QComboBox(BCLDisplay);
        variable->setObjectName(QString::fromUtf8("variable"));

        horizontalLayout->addWidget(variable);

        value = new QLabel(BCLDisplay);
        value->setObjectName(QString::fromUtf8("value"));
        value->setMinimumSize(QSize(100, 0));

        horizontalLayout->addWidget(value);

        slider = new QSlider(BCLDisplay);
        slider->setObjectName(QString::fromUtf8("slider"));
        slider->setMaximum(9999);
        slider->setOrientation(Qt::Horizontal);

        horizontalLayout->addWidget(slider);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(BCLDisplay);

        QMetaObject::connectSlotsByName(BCLDisplay);
    } // setupUi

    void retranslateUi(QDialog *BCLDisplay)
    {
        BCLDisplay->setWindowTitle(QApplication::translate("BCLDisplay", "Dialog", 0, QApplication::UnicodeUTF8));
        variable->clear();
        variable->insertItems(0, QStringList()
         << QApplication::translate("BCLDisplay", "var X", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("BCLDisplay", "var S", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("BCLDisplay", "var B", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("BCLDisplay", "prob", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("BCLDisplay", "lambda", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("BCLDisplay", "b0dev", 0, QApplication::UnicodeUTF8)
        );
        value->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class BCLDisplay: public Ui_BCLDisplay {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BCLDISPLAY_H
