#ifndef LABELMOUSE_H
#define LABELMOUSE_H

#include <QWidget>
#include <QObject>
#include <QLabel>
#include <QMouseEvent>
#include <QDebug>

class LabelMouse : public QLabel
{
    Q_OBJECT

public:
    int x,y;
    LabelMouse(QWidget *parent=0);
    ~LabelMouse();
protected:
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);

signals:
    void mouse_moved(QPoint&);
    void mouse_click_on_cell(QPoint&);
    void mouse_double_click_on_cell(QPoint&);
};

#endif // LABELMOUSE_H
