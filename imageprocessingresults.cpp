#include "imageprocessingresults.h"
#include "ui_imageprocessingresults.h"
#include "labelmouse.h"
#include "timesequencedisplay.h"
#include "bcl.h"
#include "assemblyform.h"

imageprocessingresults::imageprocessingresults(ImageProcessing &ip,QImage *im,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::imageprocessingresults)
{
    ui->setupUi(this);
    move(ip.progress->pos().x(),
         ip.progress->pos().y()+ip.progress->geometry().height());

    connect(ui->SegmentedImage, SIGNAL(mouse_moved(QPoint&)), this, SLOT(mouse_current_pos(QPoint&)));
    connect(ui->SegmentedImage, SIGNAL(mouse_moved(QPoint&)), this, SLOT(mouse_current_cell(QPoint&)));
    connect(ui->SegmentedImage, SIGNAL(mouse_click_on_cell(QPoint&)), this, SLOT(mouse_click_on_cell(QPoint&)));
    connect(ui->SegmentedImage, SIGNAL(mouse_double_click_on_cell(QPoint&)), this, SLOT(mouse_double_click_on_cell(QPoint&)));

    IP=&ip;
    SegmentedImage = im;
    bclpar=BCLparams(IP->CX);

    ui->SNR->setValue((int)(IP->CX->SNR/2*100));
    ui->SNRval->setText(QString::number(IP->CX->SNR));

    ui->CellRadius->setValue((int)(IP->CX->CHECKSHAPE_MINSIZE/400.*100));
    ui->CellRadval->setText(QString::number(IP->CX->CHECKSHAPE_MINSIZE));

    ui->Size->setValue((int)(IP->CX->MAXSIZE*100));
    ui->Sizeval->setText(QString::number(IP->CX->MAXSIZE));

    ui->Ratio->setValue((int)(IP->CX->CHECKSHAPE_MAXRATIO*100));
    ui->Ratioval->setText(QString::number(IP->CX->CHECKSHAPE_MAXRATIO));

    ui->Compactness->setValue((int)(IP->CX->CHECKSHAPE_COMPACTNESS*100));
    ui->Compactval->setText(QString::number(IP->CX->CHECKSHAPE_COMPACTNESS));

}

imageprocessingresults::~imageprocessingresults()
{
    delete ui;
}

void imageprocessingresults::on_FilterButton_clicked(){

    QVector<QRgb> colors(IP->nsegments+1);
    colors[0]=qRgb(0,0,0);
    SegmentedImage->fill(colors[0]);

    for(unsigned int i=1;i<=IP->nsegments;++i) {
        if(IP->size_match[i-1]<ui->Size->value()/100.*300 &&
           IP->radius[i-1]>ui->CellRadius->value()/100.*400 &&
           IP->compactness[i-1]>ui->Compactness->value()/100. &&
           IP->eccentricity[i-1]>ui->Ratio->value()/100. &&
           IP->snr_vec[i-1]>ui->SNR->value()/100.*2){
               colors[i]=qRgb((int)(rand()*1./RAND_MAX*255),
                       (int)(rand()*1./RAND_MAX*255),
                       (int)(rand()*1./RAND_MAX*255));
        } else {
            colors[i]=qRgb(0,0,0);
        }
    }

    for(unsigned int i=0;i<IP->width;++i){
        for(unsigned int j=0;j<IP->height;++j){
            if(IP->wst_nocut.at<unsigned short>(j,i)>0)
                SegmentedImage->setPixel(i,j,colors[IP->wst_nocut.at<unsigned short>(j,i)]);
        }
    }

    ShowSegmentedImage(*SegmentedImage);
    ui->SegmentedImage->repaint();
}

void imageprocessingresults::ShowSegmentedImage(QImage &image)
{
    QSize imageSize=ui->SegmentedImage->size();
    QImage ScaledImage = image.scaled(imageSize,Qt::KeepAspectRatio);
    QPixmap* seg = new QPixmap(QPixmap::fromImage(ScaledImage));
    ui->SegmentedImage->setPixmap(*seg);
    ui->SegmentedImage->setScaledContents(true);
}


void imageprocessingresults::on_SNR_valueChanged(int value)
{
    ui->SNRval->setText(QString::number(value*2.0/100.));
}

void imageprocessingresults::on_CellRadius_valueChanged(int value)
{
    ui->CellRadval->setText(QString::number(value*1.0/100*400.));
}

void imageprocessingresults::on_Size_valueChanged(int value)
{
    ui->Sizeval->setText(QString::number(value*1.0/100.*300));
}

void imageprocessingresults::on_Ratio_valueChanged(int value)
{
    ui->Ratioval->setText(QString::number(value*1.0/100.));
}

void imageprocessingresults::on_Compactness_valueChanged(int value)
{
    ui->Compactval->setText(QString::number(value*1.0/100.));
}

void imageprocessingresults::mouse_current_pos(QPoint &pos)
{
    ui->mouse_position->setText(QString::number(pos.x())+' '+QString::number(pos.y()));
}

void imageprocessingresults::mouse_current_cell(QPoint &pos)
{
    //inverted
    int cellid=IP->wst_nocut.at<unsigned short>((int)((double)IP->height/ui->SegmentedImage->geometry().height()*pos.y()),
                                                (int)((double)IP->width/ui->SegmentedImage->geometry().width()*pos.x())
                                     );

    ui->cellID->setText(QString::number(cellid));
}



void imageprocessingresults::mouse_click_on_cell(QPoint &pos)
{
    //inverted
    int cellid=IP->wst_nocut.at<unsigned short>((int)((double)IP->height/ui->SegmentedImage->geometry().height()*pos.y()),
                                                (int)((double)IP->width/ui->SegmentedImage->geometry().width()*pos.x())
                                     );

    std::vector<double> xaxis(IP->ntimes);
    for(unsigned int i=0;i<IP->ntimes;++i) xaxis[i]=i;


    if(cellid>0)
    {
        ts.push_back(new TimeSequenceDisplay(this, this->pos().x()-frameGeometry().width(),this->pos().y()));
        ts.back()->setTitle(QString::number(cellid));
        ts.back()->setData(xaxis,IP->all_cell_resp[cellid-1]);
        ts.back()->move(200,200);
        ts.back()->show();
    }

}

void imageprocessingresults::mouse_double_click_on_cell(QPoint &pos)
{
    // inverted
    int cellid=IP->wst_nocut.at<unsigned short>((int)((double)IP->height/ui->SegmentedImage->geometry().height()*pos.y()),
                                                (int)((double)IP->width/ui->SegmentedImage->geometry().width()*pos.x())

                                     );


    if(cellid>0)
    {
        bcl_ts.push_back(new BCLDisplay(IP->all_cell_resp[cellid-1],IP->CX->DENSCL_FREQ,bclpar,this,
                                        this->pos().x()-frameGeometry().width(),
                                        this->pos().y()+100));
        bcl_ts.back()->setTitle(QString::number(cellid));
        bcl_ts.back()->plot();
        bcl_ts.back()->move(200,200);
        bcl_ts.back()->show();

    }
}

void imageprocessingresults::on_Update_clicked()
{
    ui->toolButton->setEnabled(false);
    for(unsigned int i=1;i<=IP->nsegments;++i) {
        if(IP->size_match[i-1]<ui->Size->value()/100.*300 &&
           IP->radius[i-1]>ui->CellRadius->value()/100.*400 &&
           IP->compactness[i-1]>ui->Compactness->value()/100. &&
           IP->eccentricity[i-1]>ui->Ratio->value()/100. &&
           IP->snr_vec[i-1]>ui->SNR->value()/100.*2){

            IP->select[i-1]=1;

        } else {

            IP->select[i-1]=0;

        }
    }

    // Rebuild wst_selec
    IP->set_wst_selec();

    IP->CX->BCLPARAMS_VARX=bclpar.varX;
    IP->CX->BCLPARAMS_VARS=bclpar.varS;
    IP->CX->BCLPARAMS_VARB=bclpar.varB;
    IP->CX->BCLPARAMS_LAMBDA=bclpar.lambda;
    IP->CX->BCLPARAMS_PROB=bclpar.prob;
    IP->CX->BCLPARAMS_B0DEV=bclpar.b0dev;
    bclpar.print();
    IP->ExternalSelection=true;
    ui->toolButton->setEnabled(true);

}

void imageprocessingresults::on_toolButton_clicked()
{

    IP->CX->print();
    AssemblyForm AF(*IP,IP->progress);
    AF.exec();

}

void imageprocessingresults::on_SaveButton_clicked()
{
    unsigned int i,k, selection_size;
    unsigned int ncells,counter=0;
    int BCLMODE=1, BCLEMSTEPS=3;

    // Find selection size
    selection_size=0;
    bcl_type bclres(IP->ntimes,IP->CX->DENSCL_FREQ);
    for(i=0;i<IP->all_cell_resp.size();++i){
        if(IP->select[i]==1) selection_size++;
    }

    ncells=selection_size;
    arma::Mat<int> binary_from_bcl(ncells,IP->ntimes);

    for(i=0;i<IP->all_cell_resp.size();++i){
        if(IP->select[i]==1){
            bclres.setData(&IP->all_cell_resp[i][0]);
            bclres.bclEM(bclpar,BCLMODE,BCLEMSTEPS);
            for(k=0;k<IP->ntimes;k++) binary_from_bcl(counter,k) = bclres.sks[k];
            counter++;
        }
    }

    binary_from_bcl.save((IP->ResultsFolder+"/"+IP->Prefix+"_binary_activity.dat").toStdString(),arma::raw_ascii);

   IP->WriteCellCenters();

}

void imageprocessingresults::on_pushButton_clicked()
{
    BCLGroupDisplay bclgdisp(IP->all_cell_resp,IP->select,IP->CX->DENSCL_FREQ,bclpar,this,
                                            this->pos().x()-frameGeometry().width(),
                                            this->pos().y()+100);
    bclgdisp.exec();

}
