#include "timesequencedisplay.h"
#include "ui_timesequencedisplay.h"
#include <iostream>

TimeSequenceDisplay::TimeSequenceDisplay(QWidget *parent,int x0,int y0) :
    QDialog(parent),
    ui(new Ui::TimeSequenceDisplay)
{
    ui->setupUi(this);
    move(x0,y0);
    ui->traj->addGraph();

}

TimeSequenceDisplay::~TimeSequenceDisplay()
{
    delete ui;
}

void TimeSequenceDisplay::setData(std::vector<double> &x, std::vector<double> &y)
{
    QVector<double> xx(x.size()),yy(y.size());
    for(unsigned int i=0;i<x.size();++i) {
        xx[i]=x[i]; yy[i]=y[i];
    }
    ui->traj->xAxis->setRange(*std::min_element(x.begin(),x.end()), *std::max_element(x.begin(),x.end()));
    ui->traj->yAxis->setRange(*std::min_element(y.begin(),y.end()), *std::max_element(y.begin(),y.end()));
    ui->traj->graph(0)->setData(xx,yy);
    ui->traj->update();
}

void TimeSequenceDisplay::setTitle(QString title)
{
    setWindowTitle(title);
}


