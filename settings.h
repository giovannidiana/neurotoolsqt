#ifndef SETTINGS_H
#define SETTINGS_H

#include<QString>

class settings
{
public:
    settings();

    enum distfunc {EUCLIDEAN_DIST,COSINE_DIST,MAX_DIST, BRAVE_DIST, W_DIST, RANK_DIST};

    int load(QString);
    void print();
    double WSFLOOD_MINFLU;
    double WSFLOOD_TOL;
    int WSFLOOD_RAD;
    int WSFLOOD_BLUR;
    int WSFLOOD_CELLRAD;
    int TRAF_BUFFSIZE;
    int TRAF_WS;
    int MAXMIN_BINSIZE;
    int PATTERN_NUM;
    double CHECKSHAPE_MAXRATIO;
    double CHECKSHAPE_MINSIZE;
    double CHECKSHAPE_COMPACTNESS;
    int MAXSIZE;
    double SNR;
    double WSFLOOD_CORRWEIGHT;
    double WSFLOOD_MAXMIXDIST;
    int MIP;
    int DISPLAY_SCALE;
    double DENSCL_TSHIFT;
    double DENSCL_FREQ;
    double DENSCL_TH_SLOPE;
    double DENSCL_TH_DIST;
    int KNNVAR;
    int NNUSED;
    double BCLPARAMS_VARX;
    double BCLPARAMS_VARS;
    double BCLPARAMS_VARB;
    double BCLPARAMS_PROB;
    double BCLPARAMS_LAMBDA;
    double BCLPARAMS_B0DEV;

    const settings::distfunc DIST_FUN;

};

#endif // SETTINGS_H
