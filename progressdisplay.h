#ifndef PROGRESSDISPLAY_H
#define PROGRESSDISPLAY_H

#include <QDialog>

namespace Ui {
class ProgressDisplay;
}

class ProgressDisplay : public QDialog
{
    Q_OBJECT

public:
    explicit ProgressDisplay(QWidget *parent = 0);
    ~ProgressDisplay();

    void updateProgressBar(int p, QString s);

private:
    Ui::ProgressDisplay *ui;
};

#endif // PROGRESSDISPLAY_H
