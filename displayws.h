#ifndef DISPLAYWS_H
#define DISPLAYWS_H

#include <opencv2/opencv.hpp>
#include<QString>
#include<boost/array.hpp>
#include"settings.h"
#include <QString>
#include "progressdisplay.h"
#include "mainwindow.h"
#include "settings.h"

using namespace cv;

struct SortArray{
        inline bool operator() (const boost::array<int,2>& t1, const boost::array<int,2>& t2){

            return (t1[1] < t2[1]);
        }
};

class ImageProcessing {

public:
    settings* CX;
    QString InputFile, ResultsFolder, Prefix;
    ImageProcessing(settings &set,MainWindow *);
    ~ImageProcessing();
    int TCLsimple();
    int displayWS();
    int cosi();
    double Corr(unsigned short* data, size_t a, size_t b,size_t nz);
    double matchMoments(Mat& m1,Mat& m2);
    bool checkShape(Mat& m1,double maxratio,double minsize,double compactness);
    void GetShape(Mat &m1,double*);
    boost::array<double,2> getEi(Mat& m1);
    double matchPattern(Mat& m1,Mat& m2);

    void copyPattern(const Mat &src, Mat &pattern,boost::array<int,2> center,int RAD);

    void wsflood(const Mat &src, Mat &wst, int& ncells,int& ncells_eff, unsigned short* tdata, int nz);

    int seg_ws();

    void GetCellProperties();

    void WriteCellCenters();

    void GetQImage(std::vector<double> &thresholds, QImage *im);

    void LoadPreviousResults();

    void LoadData();

    void LoadDataFromTiff();

    void set_wst_selec();

    size_t width,height,ntimes;
    int nsegments;
    cv::Mat wst_nocut;
    cv::Mat wst_selec;
    std::vector<cv::Mat> srcRec;
    std::vector<double> size_match,radius,eccentricity,compactness,snr_vec;
    std::vector< std::vector<double> > all_cell_resp;
    unsigned short *data;
    int dataType;
    std::vector<int> select;

    bool ExternalSelection;
    bool ExternalData;
    MainWindow *progress;

private:

    std::vector<boost::array<int,2> > peaks;
    Mat image_float_b;
    std::vector<Point2i> centers;


};



#endif // DISPLAYWS_H

