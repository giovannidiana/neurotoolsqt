/********************************************************************************
** Form generated from reading UI file 'imageprocessingsetup.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMAGEPROCESSINGSETUP_H
#define UI_IMAGEPROCESSINGSETUP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QToolButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_imageprocessingsetup
{
public:
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLabel *InputDisplayLabel;
    QLabel *label_2;
    QComboBox *comboBox;
    QPushButton *pushButton;
    QLabel *label;
    QLineEdit *PrefixLineEdit;
    QPushButton *pushButton_2;
    QLabel *label_3;
    QLabel *ResultsFolderDisplayLabel;
    QCheckBox *checkData;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout_2;
    QLabel *label_4;
    QLabel *label_6;
    QToolButton *Segment;
    QLabel *label_5;
    QToolButton *Assembler;
    QToolButton *Clean;

    void setupUi(QDialog *imageprocessingsetup)
    {
        if (imageprocessingsetup->objectName().isEmpty())
            imageprocessingsetup->setObjectName(QString::fromUtf8("imageprocessingsetup"));
        imageprocessingsetup->resize(404, 304);
        layoutWidget = new QWidget(imageprocessingsetup);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 381, 174));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        InputDisplayLabel = new QLabel(layoutWidget);
        InputDisplayLabel->setObjectName(QString::fromUtf8("InputDisplayLabel"));

        gridLayout->addWidget(InputDisplayLabel, 0, 1, 1, 3);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMaximumSize(QSize(16777215, 20));

        gridLayout->addWidget(label_2, 4, 0, 1, 1);

        comboBox = new QComboBox(layoutWidget);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout->addWidget(comboBox, 3, 0, 1, 1);

        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setMaximumSize(QSize(60, 16777215));

        gridLayout->addWidget(pushButton, 3, 3, 1, 1);

        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMaximumSize(QSize(50, 20));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        PrefixLineEdit = new QLineEdit(layoutWidget);
        PrefixLineEdit->setObjectName(QString::fromUtf8("PrefixLineEdit"));

        gridLayout->addWidget(PrefixLineEdit, 6, 1, 1, 3);

        pushButton_2 = new QPushButton(layoutWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setMaximumSize(QSize(60, 16777215));

        gridLayout->addWidget(pushButton_2, 5, 3, 1, 1);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMaximumSize(QSize(40, 16777215));

        gridLayout->addWidget(label_3, 6, 0, 1, 1);

        ResultsFolderDisplayLabel = new QLabel(layoutWidget);
        ResultsFolderDisplayLabel->setObjectName(QString::fromUtf8("ResultsFolderDisplayLabel"));

        gridLayout->addWidget(ResultsFolderDisplayLabel, 4, 1, 1, 3);

        checkData = new QCheckBox(layoutWidget);
        checkData->setObjectName(QString::fromUtf8("checkData"));

        gridLayout->addWidget(checkData, 3, 1, 1, 1);

        layoutWidget1 = new QWidget(imageprocessingsetup);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(70, 190, 281, 112));
        gridLayout_2 = new QGridLayout(layoutWidget1);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_2->addWidget(label_4, 2, 0, 1, 1, Qt::AlignHCenter);

        label_6 = new QLabel(layoutWidget1);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_2->addWidget(label_6, 2, 2, 1, 1, Qt::AlignHCenter);

        Segment = new QToolButton(layoutWidget1);
        Segment->setObjectName(QString::fromUtf8("Segment"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/toobar_icons/resources/segmentation_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        Segment->setIcon(icon);
        Segment->setIconSize(QSize(50, 50));

        gridLayout_2->addWidget(Segment, 0, 1, 1, 1, Qt::AlignHCenter);

        label_5 = new QLabel(layoutWidget1);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_2->addWidget(label_5, 2, 1, 1, 1, Qt::AlignHCenter);

        Assembler = new QToolButton(layoutWidget1);
        Assembler->setObjectName(QString::fromUtf8("Assembler"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/toobar_icons/resources/clustering.png"), QSize(), QIcon::Normal, QIcon::Off);
        Assembler->setIcon(icon1);
        Assembler->setIconSize(QSize(50, 50));

        gridLayout_2->addWidget(Assembler, 0, 2, 1, 1);

        Clean = new QToolButton(layoutWidget1);
        Clean->setObjectName(QString::fromUtf8("Clean"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8("resources/cleaner_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        Clean->setIcon(icon2);
        Clean->setIconSize(QSize(50, 50));

        gridLayout_2->addWidget(Clean, 0, 0, 1, 1);


        retranslateUi(imageprocessingsetup);

        QMetaObject::connectSlotsByName(imageprocessingsetup);
    } // setupUi

    void retranslateUi(QDialog *imageprocessingsetup)
    {
        imageprocessingsetup->setWindowTitle(QApplication::translate("imageprocessingsetup", "Image processing setup", 0, QApplication::UnicodeUTF8));
        InputDisplayLabel->setText(QString());
        label_2->setText(QApplication::translate("imageprocessingsetup", "Results folder", 0, QApplication::UnicodeUTF8));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("imageprocessingsetup", "Nifti", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("imageprocessingsetup", "Seg", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("imageprocessingsetup", "Tiff folder", 0, QApplication::UnicodeUTF8)
        );
        pushButton->setText(QApplication::translate("imageprocessingsetup", "Choose", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("imageprocessingsetup", "Input", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("imageprocessingsetup", "Choose", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("imageprocessingsetup", "Prefix", 0, QApplication::UnicodeUTF8));
        ResultsFolderDisplayLabel->setText(QString());
        checkData->setText(QApplication::translate("imageprocessingsetup", "use memory", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("imageprocessingsetup", "Cleaning", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("imageprocessingsetup", "Assemblies", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("imageprocessingsetup", "Segmentation", 0, QApplication::UnicodeUTF8));
        Assembler->setText(QString());
        Clean->setText(QApplication::translate("imageprocessingsetup", "...", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class imageprocessingsetup: public Ui_imageprocessingsetup {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMAGEPROCESSINGSETUP_H
