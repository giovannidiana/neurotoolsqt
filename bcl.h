#ifndef BCL_H
#define BCL_H

#include<iostream>
#include<vector>
#include "settings.h"

using namespace std;

class BCLparams {
public:
    BCLparams();
    BCLparams(const BCLparams &);
    BCLparams(settings *);
    void print();
    double varX;
    double varB;
    double varS;
    double prob;
    double lambda;
    double b0dev;

    void varX_from_data(const std::vector<double> &x);
};

class bcl_type {
    private:
        int size;
        double freq;
        double* orig;
    public:
        bcl_type(size_t,double);
        vector<double> cal;
        vector<int> sks;
        vector<double> B;
        vector<double> orig_norm;
        double min,max;
        void setData(double* data);
        int bcl(const BCLparams&,int);
        int bclEM(const BCLparams&,int,int);

        bool IS_EMPTY;
};



#endif // BCL_H
