/********************************************************************************
** Form generated from reading UI file 'functionalclusteringresults.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FUNCTIONALCLUSTERINGRESULTS_H
#define UI_FUNCTIONALCLUSTERINGRESULTS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_FunctionalClusteringResults
{
public:
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QSlider *setSlope;
    QPushButton *classify;
    QLabel *label_3;
    QCustomPlot *DecisionPlot;
    QListWidget *listWidget;
    QLabel *label_4;
    QCustomPlot *ClusterCenter;
    QSlider *setDistance;
    QLabel *ClusterProfiles;
    QCustomPlot *SpatialMap;
    QLabel *label;
    QLabel *label_5;
    QLabel *label_2;
    QLabel *label_6;

    void setupUi(QDialog *FunctionalClusteringResults)
    {
        if (FunctionalClusteringResults->objectName().isEmpty())
            FunctionalClusteringResults->setObjectName(QString::fromUtf8("FunctionalClusteringResults"));
        FunctionalClusteringResults->resize(946, 645);
        layoutWidget = new QWidget(FunctionalClusteringResults);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 950, 628));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        setSlope = new QSlider(layoutWidget);
        setSlope->setObjectName(QString::fromUtf8("setSlope"));
        setSlope->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(setSlope, 3, 1, 1, 2);

        classify = new QPushButton(layoutWidget);
        classify->setObjectName(QString::fromUtf8("classify"));

        gridLayout->addWidget(classify, 5, 0, 1, 3);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(40, 0));
        label_3->setMaximumSize(QSize(30, 30));

        gridLayout->addWidget(label_3, 0, 4, 1, 1);

        DecisionPlot = new QCustomPlot(layoutWidget);
        DecisionPlot->setObjectName(QString::fromUtf8("DecisionPlot"));
        DecisionPlot->setMinimumSize(QSize(260, 0));
        DecisionPlot->setStyleSheet(QString::fromUtf8("background-color: rgb(167, 167, 167);"));

        gridLayout->addWidget(DecisionPlot, 1, 0, 2, 3);

        listWidget = new QListWidget(layoutWidget);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setMaximumSize(QSize(40, 16777215));

        gridLayout->addWidget(listWidget, 1, 4, 5, 1);

        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 0, 3, 1, 1);

        ClusterCenter = new QCustomPlot(layoutWidget);
        ClusterCenter->setObjectName(QString::fromUtf8("ClusterCenter"));
        ClusterCenter->setMinimumSize(QSize(300, 0));

        gridLayout->addWidget(ClusterCenter, 1, 5, 7, 1);

        setDistance = new QSlider(layoutWidget);
        setDistance->setObjectName(QString::fromUtf8("setDistance"));
        setDistance->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(setDistance, 4, 1, 1, 2);

        ClusterProfiles = new QLabel(layoutWidget);
        ClusterProfiles->setObjectName(QString::fromUtf8("ClusterProfiles"));
        ClusterProfiles->setMinimumSize(QSize(0, 300));

        gridLayout->addWidget(ClusterProfiles, 7, 0, 1, 5);

        SpatialMap = new QCustomPlot(layoutWidget);
        SpatialMap->setObjectName(QString::fromUtf8("SpatialMap"));
        SpatialMap->setMinimumSize(QSize(300, 0));

        gridLayout->addWidget(SpatialMap, 1, 3, 5, 1);

        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 3, 0, 1, 1);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMaximumSize(QSize(16777215, 20));

        gridLayout->addWidget(label_5, 0, 0, 1, 3);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 4, 0, 1, 1);

        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMaximumSize(QSize(16777215, 20));

        gridLayout->addWidget(label_6, 6, 0, 1, 1);


        retranslateUi(FunctionalClusteringResults);

        QMetaObject::connectSlotsByName(FunctionalClusteringResults);
    } // setupUi

    void retranslateUi(QDialog *FunctionalClusteringResults)
    {
        FunctionalClusteringResults->setWindowTitle(QApplication::translate("FunctionalClusteringResults", "Functional clustering results", 0, QApplication::UnicodeUTF8));
        classify->setText(QApplication::translate("FunctionalClusteringResults", "classify", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("FunctionalClusteringResults", "Cluster", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("FunctionalClusteringResults", "Spatial map", 0, QApplication::UnicodeUTF8));
        ClusterProfiles->setText(QString());
        label->setText(QApplication::translate("FunctionalClusteringResults", "Slope", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("FunctionalClusteringResults", "Decision plot", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("FunctionalClusteringResults", "Distance", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("FunctionalClusteringResults", "Cluster profiles", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class FunctionalClusteringResults: public Ui_FunctionalClusteringResults {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FUNCTIONALCLUSTERINGRESULTS_H
