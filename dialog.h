#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include<QListWidgetItem>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    explicit Dialog(std::vector<QString> &list, QWidget *parent=0);
    ~Dialog();
    std::vector<int> EpochListON;

private slots:
    void on_listWidget_clicked(const QModelIndex &index);

    void on_pushButton_clicked();

    void on_clearButton_clicked();

    void on_selectAll_stateChanged(int arg1);

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
