/********************************************************************************
** Form generated from reading UI file 'imageprocessingresults.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMAGEPROCESSINGRESULTS_H
#define UI_IMAGEPROCESSINGRESULTS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QToolButton>
#include <labelmouse.h>

QT_BEGIN_NAMESPACE

class Ui_imageprocessingresults
{
public:
    QGridLayout *gridLayout_4;
    LabelMouse *SegmentedImage;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout_2;
    QLabel *mouse_position;
    QGridLayout *gridLayout;
    QSlider *Size;
    QSlider *CellRadius;
    QSlider *Ratio;
    QLabel *CellRadval;
    QLabel *label_4;
    QLabel *label;
    QLabel *Compactval;
    QSlider *Compactness;
    QLabel *Ratioval;
    QLabel *Sizeval;
    QLabel *label_2;
    QLabel *label_5;
    QLabel *SNRval;
    QLabel *label_3;
    QSlider *SNR;
    QPushButton *SaveButton;
    QPushButton *FilterButton;
    QLabel *cellID;
    QGridLayout *gridLayout_3;
    QPushButton *pushButton;
    QToolButton *toolButton;
    QPushButton *Update;

    void setupUi(QDialog *imageprocessingresults)
    {
        if (imageprocessingresults->objectName().isEmpty())
            imageprocessingresults->setObjectName(QString::fromUtf8("imageprocessingresults"));
        imageprocessingresults->resize(728, 767);
        gridLayout_4 = new QGridLayout(imageprocessingresults);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        SegmentedImage = new LabelMouse(imageprocessingresults);
        SegmentedImage->setObjectName(QString::fromUtf8("SegmentedImage"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SegmentedImage->sizePolicy().hasHeightForWidth());
        SegmentedImage->setSizePolicy(sizePolicy);
        SegmentedImage->setMinimumSize(QSize(0, 512));
        SegmentedImage->setMouseTracking(true);
        SegmentedImage->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        SegmentedImage->setFrameShape(QFrame::Box);
        SegmentedImage->setFrameShadow(QFrame::Raised);
        SegmentedImage->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout_4->addWidget(SegmentedImage, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        mouse_position = new QLabel(imageprocessingresults);
        mouse_position->setObjectName(QString::fromUtf8("mouse_position"));
        mouse_position->setMaximumSize(QSize(16777215, 20));
        mouse_position->setFrameShape(QFrame::Box);
        mouse_position->setFrameShadow(QFrame::Raised);

        gridLayout_2->addWidget(mouse_position, 0, 1, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        Size = new QSlider(imageprocessingresults);
        Size->setObjectName(QString::fromUtf8("Size"));
        Size->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(Size, 2, 1, 1, 1);

        CellRadius = new QSlider(imageprocessingresults);
        CellRadius->setObjectName(QString::fromUtf8("CellRadius"));
        CellRadius->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(CellRadius, 1, 1, 1, 1);

        Ratio = new QSlider(imageprocessingresults);
        Ratio->setObjectName(QString::fromUtf8("Ratio"));
        Ratio->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(Ratio, 3, 1, 1, 1);

        CellRadval = new QLabel(imageprocessingresults);
        CellRadval->setObjectName(QString::fromUtf8("CellRadval"));

        gridLayout->addWidget(CellRadval, 1, 2, 1, 1);

        label_4 = new QLabel(imageprocessingresults);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        label = new QLabel(imageprocessingresults);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        Compactval = new QLabel(imageprocessingresults);
        Compactval->setObjectName(QString::fromUtf8("Compactval"));

        gridLayout->addWidget(Compactval, 4, 2, 1, 1);

        Compactness = new QSlider(imageprocessingresults);
        Compactness->setObjectName(QString::fromUtf8("Compactness"));
        Compactness->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(Compactness, 4, 1, 1, 1);

        Ratioval = new QLabel(imageprocessingresults);
        Ratioval->setObjectName(QString::fromUtf8("Ratioval"));

        gridLayout->addWidget(Ratioval, 3, 2, 1, 1);

        Sizeval = new QLabel(imageprocessingresults);
        Sizeval->setObjectName(QString::fromUtf8("Sizeval"));
        Sizeval->setMinimumSize(QSize(50, 0));

        gridLayout->addWidget(Sizeval, 2, 2, 1, 1);

        label_2 = new QLabel(imageprocessingresults);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_5 = new QLabel(imageprocessingresults);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 4, 0, 1, 1);

        SNRval = new QLabel(imageprocessingresults);
        SNRval->setObjectName(QString::fromUtf8("SNRval"));

        gridLayout->addWidget(SNRval, 0, 2, 1, 1);

        label_3 = new QLabel(imageprocessingresults);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        SNR = new QSlider(imageprocessingresults);
        SNR->setObjectName(QString::fromUtf8("SNR"));
        SNR->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(SNR, 0, 1, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 4, 1);

        SaveButton = new QPushButton(imageprocessingresults);
        SaveButton->setObjectName(QString::fromUtf8("SaveButton"));

        gridLayout_2->addWidget(SaveButton, 3, 1, 1, 1);

        FilterButton = new QPushButton(imageprocessingresults);
        FilterButton->setObjectName(QString::fromUtf8("FilterButton"));

        gridLayout_2->addWidget(FilterButton, 2, 1, 1, 1);

        cellID = new QLabel(imageprocessingresults);
        cellID->setObjectName(QString::fromUtf8("cellID"));
        cellID->setMaximumSize(QSize(16777215, 20));
        cellID->setFrameShape(QFrame::Box);
        cellID->setFrameShadow(QFrame::Raised);

        gridLayout_2->addWidget(cellID, 1, 1, 1, 1);


        horizontalLayout->addLayout(gridLayout_2);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        pushButton = new QPushButton(imageprocessingresults);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        gridLayout_3->addWidget(pushButton, 0, 0, 1, 1);

        toolButton = new QToolButton(imageprocessingresults);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        QIcon icon;
        icon.addFile(QString::fromUtf8("resources/clustering.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton->setIcon(icon);
        toolButton->setIconSize(QSize(50, 50));

        gridLayout_3->addWidget(toolButton, 0, 1, 2, 1);

        Update = new QPushButton(imageprocessingresults);
        Update->setObjectName(QString::fromUtf8("Update"));

        gridLayout_3->addWidget(Update, 1, 0, 1, 1);


        horizontalLayout->addLayout(gridLayout_3);


        gridLayout_4->addLayout(horizontalLayout, 1, 0, 1, 1);


        retranslateUi(imageprocessingresults);

        QMetaObject::connectSlotsByName(imageprocessingresults);
    } // setupUi

    void retranslateUi(QDialog *imageprocessingresults)
    {
        imageprocessingresults->setWindowTitle(QApplication::translate("imageprocessingresults", "Image processing results", 0, QApplication::UnicodeUTF8));
        SegmentedImage->setText(QString());
        mouse_position->setText(QString());
        CellRadval->setText(QString());
        label_4->setText(QApplication::translate("imageprocessingresults", "Ratio", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("imageprocessingresults", "SNR", 0, QApplication::UnicodeUTF8));
        Compactval->setText(QString());
        Ratioval->setText(QString());
        Sizeval->setText(QString());
        label_2->setText(QApplication::translate("imageprocessingresults", "Cell radius", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("imageprocessingresults", "Compactness", 0, QApplication::UnicodeUTF8));
        SNRval->setText(QString());
        label_3->setText(QApplication::translate("imageprocessingresults", "Size", 0, QApplication::UnicodeUTF8));
        SaveButton->setText(QApplication::translate("imageprocessingresults", "Save binary", 0, QApplication::UnicodeUTF8));
        FilterButton->setText(QApplication::translate("imageprocessingresults", "Filter", 0, QApplication::UnicodeUTF8));
        cellID->setText(QString());
        pushButton->setText(QApplication::translate("imageprocessingresults", "Random sample", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QString());
        Update->setText(QApplication::translate("imageprocessingresults", "Update", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class imageprocessingresults: public Ui_imageprocessingresults {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMAGEPROCESSINGRESULTS_H
