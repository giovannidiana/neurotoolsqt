/********************************************************************************
** Form generated from reading UI file 'functionalclusteringsetup.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FUNCTIONALCLUSTERINGSETUP_H
#define UI_FUNCTIONALCLUSTERINGSETUP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FunctionalClusteringSetup
{
public:
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLineEdit *EditPrefix;
    QLabel *InputFileDisplayLabel;
    QPushButton *ChooseResultsFolderButton;
    QSpacerItem *verticalSpacer;
    QLabel *ResultsFolderDisplayLabel;
    QPushButton *ChooseInputButton;
    QLabel *EpochFileDisplayLabel;
    QLabel *label_4;
    QPushButton *ChooseEpochButton;
    QLabel *label;
    QComboBox *comboBox;
    QPushButton *AnalyzeButton;
    QLabel *label_3;
    QCheckBox *checkData;

    void setupUi(QWidget *FunctionalClusteringSetup)
    {
        if (FunctionalClusteringSetup->objectName().isEmpty())
            FunctionalClusteringSetup->setObjectName(QString::fromUtf8("FunctionalClusteringSetup"));
        FunctionalClusteringSetup->resize(388, 383);
        FunctionalClusteringSetup->setAutoFillBackground(false);
        layoutWidget = new QWidget(FunctionalClusteringSetup);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 10, 350, 351));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 11, 0, 1, 2, Qt::AlignRight);

        EditPrefix = new QLineEdit(layoutWidget);
        EditPrefix->setObjectName(QString::fromUtf8("EditPrefix"));

        gridLayout->addWidget(EditPrefix, 11, 2, 1, 1);

        InputFileDisplayLabel = new QLabel(layoutWidget);
        InputFileDisplayLabel->setObjectName(QString::fromUtf8("InputFileDisplayLabel"));
        InputFileDisplayLabel->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));

        gridLayout->addWidget(InputFileDisplayLabel, 6, 0, 1, 3);

        ChooseResultsFolderButton = new QPushButton(layoutWidget);
        ChooseResultsFolderButton->setObjectName(QString::fromUtf8("ChooseResultsFolderButton"));
        ChooseResultsFolderButton->setEnabled(true);

        gridLayout->addWidget(ChooseResultsFolderButton, 8, 2, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout->addItem(verticalSpacer, 12, 2, 1, 1);

        ResultsFolderDisplayLabel = new QLabel(layoutWidget);
        ResultsFolderDisplayLabel->setObjectName(QString::fromUtf8("ResultsFolderDisplayLabel"));
        ResultsFolderDisplayLabel->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));

        gridLayout->addWidget(ResultsFolderDisplayLabel, 9, 0, 1, 3);

        ChooseInputButton = new QPushButton(layoutWidget);
        ChooseInputButton->setObjectName(QString::fromUtf8("ChooseInputButton"));

        gridLayout->addWidget(ChooseInputButton, 5, 2, 1, 1);

        EpochFileDisplayLabel = new QLabel(layoutWidget);
        EpochFileDisplayLabel->setObjectName(QString::fromUtf8("EpochFileDisplayLabel"));
        EpochFileDisplayLabel->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));

        gridLayout->addWidget(EpochFileDisplayLabel, 2, 0, 1, 3);

        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 8, 0, 1, 2);

        ChooseEpochButton = new QPushButton(layoutWidget);
        ChooseEpochButton->setObjectName(QString::fromUtf8("ChooseEpochButton"));
        ChooseEpochButton->setEnabled(true);

        gridLayout->addWidget(ChooseEpochButton, 1, 2, 1, 1);

        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 3);

        comboBox = new QComboBox(layoutWidget);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout->addWidget(comboBox, 5, 0, 1, 2);

        AnalyzeButton = new QPushButton(layoutWidget);
        AnalyzeButton->setObjectName(QString::fromUtf8("AnalyzeButton"));

        gridLayout->addWidget(AnalyzeButton, 13, 2, 1, 1);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 3, 0, 1, 2);

        checkData = new QCheckBox(layoutWidget);
        checkData->setObjectName(QString::fromUtf8("checkData"));

        gridLayout->addWidget(checkData, 3, 2, 1, 1);


        retranslateUi(FunctionalClusteringSetup);

        QMetaObject::connectSlotsByName(FunctionalClusteringSetup);
    } // setupUi

    void retranslateUi(QWidget *FunctionalClusteringSetup)
    {
        FunctionalClusteringSetup->setWindowTitle(QApplication::translate("FunctionalClusteringSetup", "Functional clustering settings", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("FunctionalClusteringSetup", "Prefix", 0, QApplication::UnicodeUTF8));
        InputFileDisplayLabel->setText(QString());
        ChooseResultsFolderButton->setText(QApplication::translate("FunctionalClusteringSetup", "Find", 0, QApplication::UnicodeUTF8));
        ResultsFolderDisplayLabel->setText(QString());
        ChooseInputButton->setText(QApplication::translate("FunctionalClusteringSetup", "Find", 0, QApplication::UnicodeUTF8));
        EpochFileDisplayLabel->setText(QString());
        label_4->setText(QApplication::translate("FunctionalClusteringSetup", "Select results folder", 0, QApplication::UnicodeUTF8));
        ChooseEpochButton->setText(QApplication::translate("FunctionalClusteringSetup", "Find", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("FunctionalClusteringSetup", "Enter epoch file", 0, QApplication::UnicodeUTF8));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("FunctionalClusteringSetup", "Nifti file", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FunctionalClusteringSetup", "Binary file", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FunctionalClusteringSetup", "data file", 0, QApplication::UnicodeUTF8)
        );
        AnalyzeButton->setText(QApplication::translate("FunctionalClusteringSetup", "Analyze", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("FunctionalClusteringSetup", "Select input", 0, QApplication::UnicodeUTF8));
        checkData->setText(QApplication::translate("FunctionalClusteringSetup", "use memory", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class FunctionalClusteringSetup: public Ui_FunctionalClusteringSetup {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FUNCTIONALCLUSTERINGSETUP_H
