#ifndef TIMESEQUENCEDISPLAY_H
#define TIMESEQUENCEDISPLAY_H

#include <QDialog>

namespace Ui {
class TimeSequenceDisplay;
}

class TimeSequenceDisplay : public QDialog
{
    Q_OBJECT

public:
    explicit TimeSequenceDisplay(QWidget *parent = 0,int x=0,int y=0);
    ~TimeSequenceDisplay();
    void setData(std::vector<double>&,std::vector<double>&);
    void setTitle(QString);


private:
    Ui::TimeSequenceDisplay *ui;
};

#endif // TIMESEQUENCEDISPLAY_H
