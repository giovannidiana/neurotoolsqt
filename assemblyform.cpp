#include "assemblyform.h"
#include "ui_assemblyform.h"
#include "bcl.h"
#include <armadillo>
#include <QImage>
#include "vari.h"
#include "entropy.h"
#include "mcolors.h"

AssemblyForm::AssemblyForm(ImageProcessing &IP,MainWindow *parent) :
    QDialog(parent),
    ui(new Ui::AssemblyForm)
{
    ui->setupUi(this);
    ip=&IP;
    main=parent;

    setup(parent);

    plotTotalActivity(0);
    PlotImage(0);

}

AssemblyForm::~AssemblyForm()
{
    delete ui;
}

void AssemblyForm::setup(MainWindow *parent)
{
    parent->showProgressBar(true);
    parent->updateProgressBar(0,"setup neural assembler");
    BCLparams bclp(ip->CX); bclp.print();
    bcl_type bclres(ip->ntimes,ip->CX->DENSCL_FREQ);
    BCLMODE=1;
    BCLEMSTEPS=1;

    unsigned int i,j,k,kp,selection_size;
    unsigned int counter=0;


    // I need to build the binary matrix
    binsize=1;

    // Cells excluded from analysis are labeled with -1
    cellID_orig.resize(ip->all_cell_resp.size(),-1);

    if(ip->ExternalSelection){
        // Find selection size
        selection_size=0;

        for(i=0;i<ip->all_cell_resp.size();++i){
            if(ip->select[i]==1) selection_size++;
        }
        ncells=selection_size;
        binary_from_bcl=arma::Mat<int>(ncells,ip->ntimes);
        for(i=0;i<ip->all_cell_resp.size();++i){
            if(ip->select[i]==1){
                BCLparams bclp2(bclp);
                bclres.setData(&ip->all_cell_resp[i][0]);
                bclp2.varX_from_data(bclres.orig_norm);
                //bclres.bclEM(bclp,BCLMODE,BCLEMSTEPS);
                bclres.bcl(bclp2,BCLMODE);
                for(k=0;k<ip->ntimes;k++) binary_from_bcl(counter,k) = bclres.sks[k];
                cellID_orig[i]=counter;
                counter++;
            }
        }

    } else {

        ncells=ip->all_cell_resp.size();
        binary_from_bcl=arma::Mat<int>(ncells,ip->ntimes);

        for(unsigned int i=0;i<ncells;++i){            
            bclres.setData(&ip->all_cell_resp[i][0]);
            bclres.bclEM(bclp,BCLMODE,BCLEMSTEPS);
            for(k=0;k<ip->ntimes;k++) binary_from_bcl(i,k) = bclres.sks[k];
            cellID_orig[i]=i;
        }
    }


    parent->updateProgressBar(45,"setup interface");
    activity_tot=arma::sum(binary_from_bcl);
    binary_display=arma::Mat<int>(ncells,ip->ntimes);
    binary_display=binary_from_bcl;

    // UI related setups
    ui->Activity->addGraph(); // for the total activity
    ui->Activity->addGraph(); // for the current time frame
    ui->Activity->addGraph(); // for the threshold

    ui->Activity->graph(1)->setPen(QPen(QColor(250,0,0)));
    ui->Activity->graph(2)->setPen(QPen(QColor(0,0,250)));

    ui->Activity->xAxis->setVisible(false);
    ui->Activity->yAxis->setVisible(false);
    ui->Activity->axisRect()->setAutoMargins(QCP::msNone);
    ui->Activity->axisRect()->setMargins(QMargins(0,0,0,0));

    // Setup of raster plot
    pix=NULL;
    // IMPORTANT: it is counter intuitive but when assigning pixel values to QImage(width,height)
    // using setPixel(i,j) j is the row index while i is the column index.
    qim = new QImage(QImage(ip->ntimes,ncells,QImage::Format_RGB32));

    // Setup of neuron image
    pix_neurons=NULL;
    image_neurons = new QImage(QImage(ip->width,ip->height,QImage::Format_RGB32));
    ui->scrollBar->setMaximum(ip->ntimes-1);
    brightness=1;

    // Setup of total activity plot
    threshold=0;

    // SVD variables
    svd_on=false;

    // Set a label for cell that has been selected
    cell_labels.resize(ncells,-1);

    // wst plot
    pix_wst=NULL;
    image_wst = new QImage(QImage(ip->width,ip->height,QImage::Format_RGB32));

    // Setup entropy plot
    ui->entropyPlot->addGraph();
    ui->entropyPlot->addGraph();
    ui->entropyPlot->graph(0)->setPen(QPen(QColor(250,0,0),2));
    ui->entropyPlot->graph(1)->setPen(QPen(QColor(0,0,250),2));

    parent->updateProgressBar(99,"setup segmented data");
    PlotWST();
    ui->Expand->setValue(1);
    parent->showProgressBar(false);

}

void AssemblyForm::MakeBinary(int binsize,double threshold)
{
    main->showProgressBar(true);
    main->showProgressBar(false);
}

void AssemblyForm::plotTotalActivity(double threshold)
{
    QVector<double> x(ip->ntimes);
    QVector<double> y(ip->ntimes);

    for(unsigned int k=0;k<ip->ntimes;++k){
        x[k]=k;
        y[k]=activity_tot(k);
    }

    ui->Activity->xAxis->setRange(0,ip->ntimes);
    ui->Activity->yAxis->setRange(activity_tot.min(),activity_tot.max());
    ui->Activity->graph(0)->setData(x,y);

    QVector<double> x1(20), y1(20);
    for(unsigned int k=0;k<20;++k){
        x1[k]=ui->scrollBar->value();
        y1[k]=ui->Activity->yAxis->range().lower+k/20.*(ui->Activity->yAxis->range().upper-ui->Activity->yAxis->range().lower);
    }


    QVector<double> x2(20), y2(20);
    for(unsigned int k=0;k<20;++k){
        x2[k]=ui->Activity->xAxis->range().lower+k/20.*(ui->Activity->xAxis->range().upper-ui->Activity->xAxis->range().lower);
        y2[k]=threshold;
    }

    ui->Activity->graph(1)->setData(x1,y1);
    ui->Activity->graph(2)->setData(x2,y2);

    ui->Activity->replot();
}

void AssemblyForm::plotMap()
{
    unsigned int i,j;
    int r,g,b;
    int val;

    for(i=0;i<ip->ntimes;++i){
        for(j=0;j<ncells;++j){
            val=250*binary_display(j,i);
            qim->setPixel(i,j,qRgb(val,val,val));
        }
    }

    QSize imageSize=ui->Raster->size();
    QImage scaledImage = qim->scaled(imageSize,Qt::IgnoreAspectRatio);

    if(pix!=NULL){
        delete pix;
        pix=NULL;
    }

    pix = new QPixmap(QPixmap::fromImage(scaledImage));
    ui->Raster->setPixmap(*pix);
    ui->Raster->setScaledContents(true);
    ui->Raster->repaint();
}


// This function plots the movie
void AssemblyForm::PlotImage(int frame){

    int r,g,b;
    int val;
    unsigned int label;
    unsigned short cellid;

    int valr,valg,valb;
    mcolors color;

    if(svd_on){
        for(unsigned int i=0;i<ip->width;++i){
            for(unsigned int j=0;j<ip->height;++j){
                val=(int)(ip->data[ip->width*ip->height*frame+ip->width*j+i]*pow(2.0,8-16)*brightness);
                cellid=ip->wst_nocut.at<unsigned short>(j,i);

                if(cellid > 0){
                    label=(cellID_orig[cellid-1]>=0) ? cell_labels[cellID_orig[cellid-1]] : -1;
                }

                if(cellid==0 || label==-1) {
                    valr=valg=valb=0;
                } else {
                    //cout<<cellid<<' '<<label<<endl;
                    valr=(int)(color.vec[label].red()*val/255.);
                    valg=(int)(color.vec[label].green()*val/255.);
                    valb=(int)(color.vec[label].blue()*val/255.);
                }
                image_neurons->setPixel(i,j,qRgb(valr,valg,valb));
            }
        }
    } else {
        for(unsigned int i=0;i<ip->width;++i){
            for(unsigned int j=0;j<ip->height;++j){
                val=(int)(ip->data[ip->width*ip->height*frame+ip->width*j+i]*pow(2.0,8-16)*brightness);
                image_neurons->setPixel(i,j,qRgb(val,val,val));
            }
        }
    }

      QSize imageSize=ui->neuroimage->size();
      QImage scaledImage = image_neurons->scaled(imageSize,Qt::IgnoreAspectRatio);

      if(pix_neurons!=NULL) delete pix_neurons;
      pix_neurons = new QPixmap(QPixmap::fromImage(scaledImage));
      ui->neuroimage->setPixmap(*pix_neurons);

      ui->neuroimage->setScaledContents(true);


      ui->neuroimage->show();


}

void AssemblyForm::PlotWST()
{

    int val;
    unsigned int label;
    unsigned short cellid;
    mcolors color;

    if(svd_on){
        for(unsigned int i=0;i<ip->width;++i){
            for(unsigned int j=0;j<ip->height;++j){
                cellid=ip->wst_nocut.at<unsigned short>(j,i);
                if(cellid>0){
                    label=(cellID_orig[cellid-1]>=0) ? cell_labels[cellID_orig[cellid-1]] : -1;
                }
                if(cellid==0 || label==-1) {
                    image_wst->setPixel(i,j,qRgb(0,0,0));
                } else {
                    image_wst->setPixel(i,j,qRgb(color.vec[label].red(),
                                                 color.vec[label].green(),
                                                 color.vec[label].blue()));
                }
            }
        }
    } else {
        for(unsigned int i=0;i<ip->width;++i){
            for(unsigned int j=0;j<ip->height;++j){
                cellid=ip->wst_nocut.at<unsigned short>(j,i);
                if(cellid>0){
                    val=(ip->select[cellid-1]>0) ? 255 : 0;
                } else val=0;
                image_wst->setPixel(i,j,qRgb(val,val,val));
            }
        }
    }


    QSize imageSize=ui->wstlabel->size();
    QImage scaledImage = image_wst->scaled(imageSize,Qt::IgnoreAspectRatio);

    if(pix_wst!=NULL) delete pix_wst;
    pix_wst = new QPixmap(QPixmap::fromImage(scaledImage));
    ui->wstlabel->setPixmap(*pix_wst);
    ui->wstlabel->setScaledContents(true);
    ui->wstlabel->repaint();
}

void AssemblyForm::on_maxval_valueChanged(int value)
{
    ui->Activity->yAxis->setRangeUpper((double)value/100*activity_tot.max()+(1-(double)value/100)*activity_tot.min());
    ui->Activity->replot();
}

void AssemblyForm::on_minval_valueChanged(int value)
{
    ui->Activity->yAxis->setRangeLower((double)value/100*activity_tot.max()+(1-(double)value/100)*activity_tot.min());
    ui->Activity->replot();

}

void AssemblyForm::on_scrollBar_valueChanged(int value)
{
    PlotImage(value);
    plotTotalActivity(threshold);

}

void AssemblyForm::on_brightness_valueChanged(int value)
{
    brightness=value/100.*10.;
}

void AssemblyForm::on_Expand_valueChanged(int arg1)
{
    binsize=arg1;
    unsigned int i,j,k,kp,selection_size;

    binary_display.fill(0);

    for(i=0;i<ncells;++i){
        for(k=0;k<ip->ntimes;++k){
            if(binary_from_bcl(i,k)==1){
                for(kp=max(0u,k-binsize);kp<=min((unsigned int)ip->ntimes-1,k+binsize);kp++) binary_display(i,kp)=1;
            }
        }
    }

    activity_tot=arma::sum(binary_display);
    plotTotalActivity(threshold);
    plotMap();
}



void AssemblyForm::on_threshold_valueChanged(int value)
{
    QVector<double> x2(20), y2(20);
    threshold=ui->Activity->yAxis->range().lower+value/100.*(ui->Activity->yAxis->range().upper-ui->Activity->yAxis->range().lower);

    for(unsigned int k=0;k<20;++k){
        x2[k]=ui->Activity->xAxis->range().lower+k/20.*(ui->Activity->xAxis->range().upper-ui->Activity->xAxis->range().lower);
        y2[k]=threshold;
    }
    ui->Activity->graph(2)->setData(x2,y2);
    ui->Activity->replot();
}

void AssemblyForm::on_SVD_clicked()
{
    // Extraction of synchronous frames based on the threshold value
    unsigned int i,j,k;
    unsigned int nguess=ui->spinAssemblies->value();
    arma::mat U,V;
    arma::mat Urot;
    arma::vec s;
    vector<arma::uword> cell_index;

    ofstream testfile("test.dat");
    std::fill(cell_labels.begin(),cell_labels.end(),-1);

    unsigned int counter=0,a_counter;
    for(i=0;i<ip->ntimes;i++){
        if(activity_tot(i)>threshold) counter++;
    }

    arma::mat activity_time_select(ncells,counter);

    counter=0;
    for(i=0;i<ip->ntimes;++i){
        if(activity_tot(i)>threshold){
            for(j=0;j<ncells;++j) activity_time_select(j,counter)=binary_display(j,i);
            counter++;
        }
    }

    arma::vec activity_tot_bycell = arma::sum(activity_time_select,1);

    for(i=0;i<ncells;++i){
        if(activity_tot_bycell(i)>0) cell_index.push_back(i);
    }

    activity=arma::mat(cell_index.size(),counter);
    for(i=0;i<cell_index.size();++i) activity.row(i)=activity_time_select.row(cell_index[i]);

    testfile<<activity;

    // Now the activity matrix is ready to pass to svd
    arma::svd(U,s,V,activity);

    // generate Varimax instance
    arma::mat Ucut=U.cols(0,nguess-1);

    Vari rot(Ucut);
    rot.varimax(1e-5,true);
    Urot = pow(Ucut * rot.rot,2);

    // print the new map
    int r,g,b;
    int val;

    vector<arma::uword> assembly_indexes(cell_index.size());
    // Note: the function .max(uword &idx_max) is not mentioned in the armadillo documentation but
    // people use it.
    for(j=0;j<cell_index.size();++j) {
        Urot.row(j).max(assembly_indexes[j]);
        cell_labels[cell_index[j]]=assembly_indexes[j];
    }

//    for(j=0;j<cell_index.size();++j) cout<<cell_labels[cell_index[j]]<<endl;

    n_assemblies=nguess;
    svd_on=true;

    counter=0; a_counter=0;
    while(counter<cell_index.size() && a_counter<nguess){
        for(j=0;j<cell_index.size();++j){
            if(assembly_indexes[j]==a_counter){
                for(k=0;k<ip->ntimes;++k){
                    val=250*binary_display(cell_index[j],k);
                    qim->setPixel(k,counter,qRgb(val,val,val));
                }

                counter++;
            }
        }
        a_counter++;
    }

    QSize imageSize=ui->Raster->size();
    QImage scaledImage = qim->scaled(imageSize,Qt::IgnoreAspectRatio);

    if(pix!=NULL){
        delete pix;
        pix=NULL;
    }

    pix = new QPixmap(QPixmap::fromImage(scaledImage));
    ui->Raster->setPixmap(*pix);
    ui->Raster->setScaledContents(true);
    ui->Raster->show();
    PlotWST();



}

void AssemblyForm::on_updateEntropy_clicked()
{

    arma::mat U,V;
    arma::mat Urot,Ucut;
    arma::vec s;
    QVector<double> enVec_mean(10);
    QVector<double> enVec_max(10);
    QVector<double> xx(10);
    double enMin=100, enMax=0;

    unsigned int nguess,i;

    for(i=0;i<10;++i){

        nguess=i+2;

        xx[i]=nguess;

        // SVD
        arma::svd(U,s,V,activity);

        // generate Varimax instance
        arma::mat Ucut=U.cols(0,nguess-1);

        Vari rot(Ucut);
        rot.varimax(1e-5,true);
        Urot = pow(Ucut * rot.rot,2);

        getEns(Urot,enVec_mean[i],enVec_max[i]);
        if(enMin>enVec_mean[i]) enMin=enVec_mean[i];
        if(enMax<enVec_max[i]) enMax=enVec_max[i];

    }

    ui->entropyPlot->xAxis->setRange(0,13);
    ui->entropyPlot->yAxis->setRange(enMin,enMax);

    ui->entropyPlot->graph(0)->setData(xx,enVec_mean);
    ui->entropyPlot->graph(1)->setData(xx,enVec_max);

    ui->entropyPlot->replot();

}
