#include "functionalclusteringresults.h"
#include "ui_functionalclusteringresults.h"
#include <fstream>
#include "functionalclustering.h"
#include "qcustomplot.h"
#include <iostream>
#include "labelmouse.h"

FunctionalClusteringResults::FunctionalClusteringResults(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FunctionalClusteringResults)
{
    ui->setupUi(this);

}

void FunctionalClusteringResults::SetupGraphs()
{
    // Setup plot_dp
    ui->DecisionPlot->addGraph();
    ui->DecisionPlot->xAxis->setLabel("distance");
    ui->DecisionPlot->yAxis->setLabel("density");

    QVector<double> x,y;
    for(unsigned int i=0;i<Clustering->dcl.size();++i){
        x.push_back(log(Clustering->dcl[i].get<3>()));
        y.push_back(log(Clustering->dcl[i].get<2>()));
    }

    ui->DecisionPlot->graph(0)->setData(x, y);
    ui->DecisionPlot->axisRect()->setupFullAxesBox();

    minx=*std::min_element(x.begin(),x.end());
    maxx=*std::max_element(x.begin(),x.end());
    miny=*std::min_element(y.begin(),y.end());
    maxy=*std::max_element(y.begin(),y.end());
    Clustering->CX->DENSCL_TH_SLOPE=0;
    Clustering->CX->DENSCL_TH_DIST=minx;
    ui->DecisionPlot->xAxis->setRange(minx,maxx);
    ui->DecisionPlot->yAxis->setRange(miny,maxy);
    ui->DecisionPlot->graph(0)->setLineStyle(QCPGraph::lsNone);
    ui->DecisionPlot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 5));
    ui->DecisionPlot->addGraph(); // threshold line slope
    ui->DecisionPlot->addGraph(); // threshold line dist

    // Setup spatial map
    plot_cell_centers();

    // Setup cluster profiles
    mat = new QImage(Clustering->dcl.size(),Clustering->nEP,QImage::Format_RGB16);
    pixClusters = new QPixmap(QPixmap::fromImage(*mat));

    // Setup ClusterCenter plot
    ui->ClusterCenter->addGraph();
    ui->ClusterCenter->axisRect()->setupFullAxesBox();
    means = new QCPBars(ui->ClusterCenter->yAxis, ui->ClusterCenter->xAxis);
    ui->ClusterCenter->xAxis->setRange(-2,2);
    ui->ClusterCenter->yAxis->setRange(0,Clustering->nEP+1);
    errorBars = new QCPErrorBars(ui->ClusterCenter->yAxis, ui->ClusterCenter->xAxis);

}

FunctionalClusteringResults::~FunctionalClusteringResults()
{
    delete ui;
//    if(pixClusters!=NULL) delete pixClusters;
//    if(means!=NULL) delete means;
//    if(errorBars!=NULL) delete errorBars;
//    if(mat!=NULL) delete mat;
}

void FunctionalClusteringResults::plot_cell_centers()
{
    QVector<double> x,y;
    for(unsigned int i=0;i<Clustering->cellcenters.size();++i){
        x.push_back(Clustering->cellcenters[i].first);
        y.push_back(Clustering->cellcenters[i].second);
    }
    // create graph and assign data to it:
    ui->SpatialMap->addGraph();
    ui->SpatialMap->graph(0)->setPen(QPen(Qt::black));
    ui->SpatialMap->graph(0)->setData(x, y);

    ui->SpatialMap->addGraph();
    ui->SpatialMap->graph(1)->setPen(QPen(Qt::red));
    ui->SpatialMap->graph(1)->setLineStyle(QCPGraph::lsNone);
    ui->SpatialMap->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 5));


    // set axes ranges, so we see all data:
    ui->SpatialMap->xAxis->setRange(*std::min_element(x.begin(),x.end()), *std::max_element(x.begin(),x.end()));
    ui->SpatialMap->yAxis->setRange(*std::min_element(y.begin(),y.end()), *std::max_element(y.begin(),y.end()));
    ui->SpatialMap->graph(0)->setLineStyle(QCPGraph::lsNone);
    ui->SpatialMap->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 3));
    ui->SpatialMap->xAxis->setVisible(false);
    ui->SpatialMap->yAxis->setVisible(false);
    ui->SpatialMap->axisRect()->setAutoMargins(QCP::msNone);
    ui->SpatialMap->axisRect()->setMargins(QMargins(0,0,0,0));
    ui->SpatialMap->show();

}

void FunctionalClusteringResults::plot_profiles(int redcl)
{

    int counter=0;
    int i,j;
    std::vector<double> response(Clustering->nEP);
    int r,g,b,a,value;
    int selec;

    for(int c=0; c<Clustering->nClusters;++c){
        selec = (c==redcl) ? 1 : 0;
        for(unsigned int id=0;id<Clustering->dcl.size();++id){
            if(Clustering->dcl[id].get<5>()==c) {
                response=Clustering->celldata[Clustering->dcl[id].get<0>()];

                for(i=0;i<Clustering->nEP;++i){
                    r=(int)floor((response[i]-Clustering->celldata_min)/(Clustering->celldata_max-Clustering->celldata_min)*255);
                    g=r*(1-.1*selec);
                    b=r*(1-.1*selec);

                    mat->setPixel(counter,i,qRgb(r,g,b));
                }
                counter++;
            }
        }

    }

    QSize imageSize=ui->ClusterProfiles->size();
    QImage scaledImage = mat->scaled(imageSize,Qt::IgnoreAspectRatio);



    *pixClusters = QPixmap::fromImage(scaledImage);
    ui->ClusterProfiles->setPixmap(*pixClusters);
    ui->ClusterProfiles->setScaledContents(true);
    ui->ClusterProfiles->show();

}

void FunctionalClusteringResults::plot_cluster_center(int index)
{
    QVector<double> x(Clustering->nEP,0),y(Clustering->nEP),xsd(Clustering->nEP,0);
    int i=0;
    int epid;
    int count=0;

    for(i=0;i<Clustering->dcl.size();++i){
        if(Clustering->dcl[i].get<5>()==index){
            ++count;
            for(epid=0;epid<Clustering->nEP;++epid){
                x[epid]+=Clustering->celldata[Clustering->dcl[i].get<0>()][epid];
                xsd[epid]+=pow(Clustering->celldata[Clustering->dcl[i].get<0>()][epid],2);
            }
        }
    }

    for(epid=0;epid<Clustering->nEP;++epid){
        y[epid]=epid+1;
        x[epid]/=count;
        xsd[epid]=sqrt(xsd[epid]/count-pow(x[epid],2));
    }

    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);

    textTicker->addTicks(y,Clustering->EpochNames);

    means->setData(y,x);
    ui->ClusterCenter->yAxis->setTicker(textTicker);
    errorBars->setDataPlottable(means);
    errorBars->setPen(QPen(QColor(0,0,180)));
    errorBars->setData(xsd);
    ui->ClusterCenter->replot();

}

void FunctionalClusteringResults::on_setSlope_valueChanged(int value)
{
    Clustering->CX->DENSCL_TH_SLOPE=value*maxy/100.;
    QVector<double> xref,ythslope;
    for(unsigned int i=0;i<100;++i){
        xref.push_back(minx+(maxx-minx)/100.*i);
        ythslope.push_back(Clustering->CX->DENSCL_TH_SLOPE-Clustering->nEP*xref[i]);
    }

    ui->DecisionPlot->graph(1)->setData(xref,ythslope);
    ui->DecisionPlot->replot();

}

void FunctionalClusteringResults::on_setDistance_valueChanged(int value)
{
    Clustering->CX->DENSCL_TH_DIST=minx+value*(maxx-minx)/100.;
    QVector<double> xref,ythslope;
    for(unsigned int i=0;i<100;++i){
        xref.push_back(Clustering->CX->DENSCL_TH_DIST);
        ythslope.push_back(miny+(maxy-miny)*i/100.);
    }

    ui->DecisionPlot->graph(2)->setData(xref,ythslope);
    ui->DecisionPlot->replot();
}

void FunctionalClusteringResults::on_classify_clicked()
{
    Clustering->Classify();
    plot_profiles(-1);
    ui->listWidget->clear();
    for(unsigned int i=0;i<Clustering->nClusters;++i) ui->listWidget->addItem(QString::number(i));
}

void FunctionalClusteringResults::on_listWidget_clicked(const QModelIndex &index)
{

    QVector<double> x,y;
    std::pair<double,double> point;
    for(unsigned int i=0;i<Clustering->dcl.size();++i){
        point=Clustering->cellcenters[Clustering->dcl[i].get<0>()];
        if(Clustering->dcl[i].get<5>()==index.row()){
            x.push_back(point.first);
            y.push_back(point.second);
        }
    }
    ui->SpatialMap->graph(1)->setData(x,y);
    plot_profiles(index.row());
    plot_cluster_center(index.row());
    ui->SpatialMap->replot();

    // update cluster center

}

