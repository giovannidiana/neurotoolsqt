#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/opengl.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include "maxmin.hpp"
#include <QApplication>
#include "handlers.h"
#include "bcl.h"
#include "linint.h"
#include <nifti1_io.h>
#include <QString>
#include <QFileDialog>
#include "settings.h"
#include "traf.hpp"
#include "displayws.h"
#include "functionalclusteringsetup.h"
#include "mainwindow.h"
#include "traf.hpp"
#include "usr_io.hpp"
#include "mlabtools.h"

using namespace std;
using namespace cv;


ImageProcessing::ImageProcessing(settings &set, MainWindow *main){
    CX=&set;
    progress=main;
    data=NULL;

    // Setup external selection flag
    ExternalSelection=false;
}

ImageProcessing::~ImageProcessing()
{
    if(data!=NULL && !ExternalData){

        delete [] data;
    }
}

int ImageProcessing::TCLsimple(){

   int           i,j,k,kp;
   size_t        counter=0;
   size_t        counter_dt=0;

   // DEFINE THE NIFTI IMAGE
   nifti_image *nim;
   if(ExternalData) {
       nim = progress->nim;
   } else {
       if(progress->nim!=NULL) nifti_image_free(progress->nim);
       progress->updateProgressBar(0,"Opening input file");
       QApplication::processEvents(QEventLoop::AllEvents,100);
       /* read input dataset, including data */
       nim = nifti_image_read(InputFile.toStdString().c_str(), 1);
   }

   // DEFINE THE SIZE OF THE IMAGE
   size_t nx=nim->nx;
   size_t ny=nim->ny;
   size_t nz=nim->nz;
   int datatype=nim->datatype;

   // BUFFER
   int buff_cp=CX->TRAF_BUFFSIZE;
   int ws=CX->TRAF_WS;

   boost::circular_buffer<double> buff(buff_cp);
   boost::circular_buffer<double> buff2(ws);

   // DEFINE IN AND OUTPUT DATA POINTERS
   unsigned short   *data_dwt = new unsigned short [nx*ny*nz];

   double   *tmp_trace = new double[nz];
   double   *tmp_trace_proc = new double[nz];
   unsigned short      *trace_dwt = new unsigned short[nz];

   // ASSIGN DATA TO POINTER
   data = static_cast<unsigned short*>(nim->data);

   // DETRENDING MODULE
   for(i=0;i<nx*ny;i++){
       if(i%(int)(nx*ny/20.)==0){
           progress->updateProgressBar((int)(i*100./(nx*ny)),"Detrending");
           QApplication::processEvents(QEventLoop::AllEvents,100);
       }
       for(k=0;k<nz;k++) tmp_trace[k]=(double)data[nx*ny*k+i];
       traf(tmp_trace,nz,tmp_trace_proc,buff,buff2,0);
       for(k=0;k<nz;k++) data_dwt[nx*ny*k+i]=floor(max(0.,tmp_trace_proc[k]));
   }

   // WRITE THE cleaned NIFTI FILE.
   QString cleaned_nii;
   cleaned_nii=ResultsFolder+'/'+Prefix+"_cleaned.nii";
   write_nii(data_dwt,cleaned_nii.toStdString(),nx,ny,nz,datatype);

   nifti_image_free(nim);
   delete [] data_dwt; data_dwt=NULL;
   delete [] tmp_trace; tmp_trace=NULL;
   delete [] tmp_trace_proc; tmp_trace_proc=NULL;
   data = NULL;

   return(0);
}

int ImageProcessing::displayWS()
{

//    AnalysisForm analysis_form;
//    analysis_form.setModal(true);


//    unsigned int i,k,j,counter;
//    size_t nx,ny,nz;
//    Mat image3D;	// image to show.
//    MatIterator_<ushort> it;
//    vector<float> tmp;
//    vector<float> mean_img;
//    int ncells,ncells_eff;
//    double snr_threshold=CX->SNR;
//    float minflu;

//    nifti_image * nim=NULL;
//    nim=nifti_image_read(qPrintable(InputFile),1);
//    data = static_cast<unsigned short*>(nim->data);

//    // Load nii image
//    ofstream imgfile("image.dat");
//    ofstream dimfile("img_dim.dat");

//    nx=nim->nx;
//    ny=nim->ny;
//    nz=nim->nz;

//    dimfile<<nx<<' '<<ny<<' '<<nz;

//    tmp.resize(nx*ny);
//    mean_img.resize(nx*ny);

//    for(i=0;i<nx*ny;i++) {
//        tmp[i]=0;
//        mean_img[i]=0;
//    }
//    for(i=0;i<nx*ny;i++){
//        for(k=0;k<nz;k++) {
//            tmp[i]=max((float)data[nx*ny*k+i],tmp[i]);
//            mean_img[i]+=data[nx*ny*k+i];
//        }
//        mean_img[i]/=nz;
//        if(CX->MIP==0){
//            tmp[i]=mean_img[i];
//        }
//        imgfile<<tmp[i]<<' '<<mean_img[i]<<endl;
//    }

//    dimfile.close();
//    imgfile.close();

//    cout<<nx*ny*nz<<endl;
//    unsigned short* tdata = new unsigned short[nx*ny*nz];

//    for(i=0;i<nx*ny;++i){
//        for(k=0;k<nz;k++){
//            tdata[nz*i + k] = data[nx*ny*k + i];
//        }
//    }
//    cout<<"transpose data..."<<endl;

//    analysis_form.AssociateImage(tmp,nx,ny);
//    analysis_form.show();


//    QApplication::processEvents(QEventLoop::AllEvents,100);

//    image3D = Mat((int)ny,(int)nx,CV_32F,(void*)&tmp[0]);

//    minflu=tmp[0];
//    for(i=1;i<nx*ny;i++)
//        if(minflu>tmp[i]) minflu=tmp[i];

//    vector<Mat> split(3);
//    for(i=0;i<3;i++) split[i]=Mat::zeros((int)ny,(int)nx,CV_32F);
//    Mat mer((int)ny,(int)nx,CV_32FC3);


//    Mat wst=Mat::zeros((int)ny,(int)nx,CV_16U);
//    Mat bw=Mat::zeros((int)ny,(int)nx,CV_32F);
//    Mat wst_toshow=Mat::zeros((int)ny,(int)nx,CV_16U);


//    wsflood(image3D,wst,ncells,ncells_eff,tdata, nz);
//    cout<<ncells<<endl;

//    double *cell_resp = new double[nz];
//    double *cell_resp_dt = new double[nz];
//    double *snr = new double[ncells];
//    vector<double*> all_cell_resp;
//    boost::circular_buffer<double> buff(CX->TRAF_BUFFSIZE);
//    boost::circular_buffer<double> buff2(CX->TRAF_WS);

//    counter=0;
//    for(k=1;k<=ncells;k++){
//        snr[k-1]=0;
//        vector<Point2i> Plist;
//        for(i=0;i<ny;i++){
//            for(j=0;j<nx;j++){
//                if(wst.at<ushort>(i,j)==k){
//                    Plist.push_back(Point2i(i,j));
//                }
//            }
//        }

//        if(Plist.size()>0){
//            for(i=0;i<nz;i++){
//                cell_resp[i]=0;
//                for(j=0;j<Plist.size();j++){
//                    cell_resp[i]+=data[Plist[j].x*nx+Plist[j].y+nx*ny*i];
//                }
//                cell_resp[i]/=Plist.size();
//            }
//            traf(cell_resp,nz,cell_resp_dt, buff, buff2,1);
//            snr[k-1]=maxmin(cell_resp_dt,nz,CX->MAXMIN_BINSIZE); // CODE0002
//            if(snr[k-1]>snr_threshold){
//                all_cell_resp.push_back(NULL);
//                all_cell_resp[counter] = new double[nz];
//                for(i=0;i<nz;i++) all_cell_resp[counter][i] = cell_resp[i];
//                counter++;
//            }

//        } else {
//            snr[k-1]=-1; // CODE0001
//        }

//    }

//    //cout<<endl;
//    counter=0;
//    for(k=1;k<=ncells;k++){
//        //cout<<k<<'\r'<<flush;
//        if(snr[k-1]<snr_threshold){
//            for(it=wst.begin<ushort>();it!=wst.end<ushort>();++it){
//                if(*it==k) *it=0;
//            }
//        } else {
//            counter++;
//            if(k!=counter) {
//                for(it=wst.begin<ushort>();it!=wst.end<ushort>();++it){
//                    if(*it==k) *it=counter;
//                }
//            }
//        }

//    }

//    int level=1;
//    int brightness=5;
//    int varB_int=280,varC_int=5,varX_int=140,lambda_int=14, prob_int=1;
//    int cellID=1;
//    int good;
//    int display_scale=CX->DISPLAY_SCALE;

//    param_t1 mouse_loc;
//    param_t2 pGL(nz), pGL_BCL(nz);
//    bcl_type bclres(nz);
//    //pGL.trace = all_cell_resp[0];
//    //pGL_BCL.trace=NULL;
//    // Create the window to display the 3D image and the WST
//    cvNamedWindow( "Display window", WINDOW_NORMAL | WINDOW_KEEPRATIO ); // Create a window for display.
//    cvSetMouseCallback("Display window",on_mouse_disp,&mouse_loc);

//    cvCreateTrackbar( "Index level", "Display window", &level, counter,  NULL);
//    cvCreateTrackbar( "Brightness", "Display window", &brightness, 10,  NULL);
//    resizeWindow( "Display window", display_scale*300,display_scale*585);
//    moveWindow("Display window",0,0);

//    cvNamedWindow( "Time sequence", WINDOW_OPENGL | WINDOW_KEEPRATIO ); // Create a window for display.
//    setOpenGlContext("Time sequence");
//    resizeWindow( "Time sequence", display_scale*600,display_scale*250);
//    moveWindow("Time sequence",display_scale*(300)+5,0);
//    setOpenGlDrawCallback("Time sequence",on_GL,(void*)&pGL);

//    cvNamedWindow( "BCL", WINDOW_OPENGL | WINDOW_KEEPRATIO ); // Create a window for display.
//    setOpenGlContext("BCL");
//    moveWindow("BCL",display_scale*300+5,display_scale*250+55);
//    resizeWindow( "BCL", display_scale*600,display_scale*320);
//    //setOpenGlDrawCallback("BCL",on_GL,(void*)&pGL_BCL);
//    setOpenGlDrawCallback("BCL",on_GL_BCL,(void*)&bclres);
//    cvCreateTrackbar( "varB", "BCL", &varB_int, 10000,  NULL);
//    cvCreateTrackbar( "varC", "BCL", &varC_int, 10000,  NULL);
//    cvCreateTrackbar( "varX", "BCL", &varX_int, 10000,  NULL);
//    cvCreateTrackbar( "lambda", "BCL", &lambda_int,10000,  NULL);
//    cvCreateTrackbar( "prob", "BCL", &prob_int, 10000,  NULL);

//    Mat image;
//    normalize(image3D,image, 0,1 , NORM_MINMAX, CV_32F);

//    char c=' '; while(c!='q') {
//        c=waitKey(10);
//         for(i=0;i<nx*ny;i++){
//             if(wst.at<ushort>(i)>=1 && wst.at<ushort>(i)<=level){
//                 bw.at<float>(i)=1;
//             } else {
//                 bw.at<float>(i)=0;
//             }
//         }

//         image.copyTo(split[2]);
//         bw.copyTo(split[1]);
//         bw.copyTo(split[0]);
//         merge(split,mer);
//         imshow("Display window",brightness*mer);

//         cellID=wst.at<ushort>(nx*mouse_loc.y+mouse_loc.x);
//         if(mouse_loc.state==1 && cellID>0) {
//             pGL.trace=all_cell_resp[cellID-1];
//         }
//         updateWindow("Time sequence");
//         bclres.orig = pGL.trace;

//         good=bcl(pGL.trace,bclres, nz,
//                 linint(1e-3,.5,prob_int/10000),
//                 linint(log(2.)/100,log(2.),lambda_int/10000.),
//                 linint(1e-6,1e-2,varB_int/10000.),
//                 linint(1e-5,1,varC_int/10000.),
//                 linint(1e-2,1,varX_int/10000.));
//         if(good) updateWindow("BCL");

//    }


//    cvDestroyAllWindows();
//    return 0;

}


// This routine applies wsflood without any further cell selection
// NOTE: this does not generate all_cell_resp.
int ImageProcessing::seg_ws()
{

    unsigned int i,k,j;

    Mat image3D;	// will be the input to wsflood
    MatIterator_<ushort> it;
    vector<float> tmp;
    vector<float> mean_img;
    int ncells,ncells_eff;
    float minflu;

    nifti_image * nim=NULL;

    // Read data from memory or from file
    if(dataType==0){
        if(ExternalData) {
            nim = progress->nim;
        } else {
            if(progress->nim!=NULL) nifti_image_free(progress->nim);
            progress->updateProgressBar(0,"Opening input file");
            QApplication::processEvents(QEventLoop::AllEvents,100);
            /* read input dataset, including data */
            nim = nifti_image_read(InputFile.toStdString().c_str(), 1);
        }

        data = static_cast<unsigned short*>(nim->data);

    } else if (dataType==1){
        LoadDataFromTiff();
    }

    // open processing files
    ofstream imgfile((ResultsFolder+"/"+Prefix+"_image.dat").toStdString().c_str());
    ofstream dimfile((ResultsFolder+"/"+Prefix+"_img_dim.dat").toStdString().c_str());
    ofstream peaksfile((ResultsFolder+"/"+Prefix+"_peaks.dat").toStdString().c_str());

    // Record dimensions
    dimfile<<width<<' '<<height<<' '<<ntimes;

    progress->updateProgressBar(0,"Building data tables");
    tmp.resize(width*height);
    mean_img.resize(width*height);

    // Initialize projection and tmp
    for(i=0;i<width*height;i++) {
        tmp[i]=0;
        mean_img[i]=0;
    }

    // Build 2D projection image
    for(i=0;i<width*height;i++){
        if(i % (int)(width*height/20.)==0) progress->updateProgressBar(100*i/(width*height),"Building data tables");
        for(k=0;k<ntimes;k++) {
            tmp[i]=max((float)data[width*height*k+i],tmp[i]);
            mean_img[i]+=data[width*height*k+i];
        }
        mean_img[i]/=ntimes;
        if(CX->MIP==0){
            tmp[i]=mean_img[i];
        }
        // save 2D projection to imgfile
        imgfile<<tmp[i]<<' '<<mean_img[i]<<endl;
    }

    // Close processing files
    dimfile.close();
    imgfile.close();

    // transpose data to speed up the analysis of time traces
    unsigned short* tdata = new unsigned short[width*height*ntimes];
    for(i=0;i<width*height;++i){
        for(k=0;k<ntimes;k++){
            tdata[ntimes*i + k] = data[width*height*k + i];
        }
    }

    // Build the wsflood input
    image3D = Mat((int)height,(int)width,CV_32F,(void*)&tmp[0]);

    // Calculate minimum fluorescence of the 2D projection
    minflu=tmp[0];
    for(i=1;i<width*height;i++)
        if(minflu>tmp[i]) minflu=tmp[i];

    // Initialize wsflood output
    Mat wst=Mat::zeros((int)height,(int)width,CV_16U);

    // Run wsflood
    wsflood(image3D,wst,ncells,ncells_eff,tdata,ntimes);

    // Store peaks found
    for(i=0;i<ncells;++i) peaksfile<<peaks[i][0]<<' '<<peaks[i][1]<<endl;
    peaksfile.close();

    delete [] tdata; tdata=NULL;

}

int ImageProcessing::cosi()
{

    size_t i,j,k;


    // Load nii image
    nifti_image * nim;

    if(dataType==0){
        if(ExternalData){
            nim = progress->nim;
        } else {
            LoadData();
//            if(progress->nim!=NULL) nifti_image_free(progress->nim);
//            progress->updateProgressBar(0,"Opening input file");
//            QApplication::processEvents(QEventLoop::AllEvents,100);
//            /* read input dataset, including data */
//            nim = nifti_image_read(InputFile.toStdString().c_str(), 1);
        }
    } else if(dataType==1){
        LoadDataFromTiff();
    }


    progress->updateProgressBar(0,"Building");
    QApplication::processEvents(QEventLoop::AllEvents,100);

    Mat image;	// image to show.
    float* tmp = new float[width*height];
    vector<double> cell_resp(ntimes);
    vector<double> cell_resp_dt(ntimes);
    vector<short> cell_resp_mask(ntimes);
    int ncells,ncells_eff;

    unsigned short* tdata = new unsigned short[width*height*ntimes];
    for(i=0;i<width*height;++i){
        for(k=0;k<ntimes;k++){
            tdata[ntimes*i + k] = data[width*height*k + i];
        }
    }

    double snr_threshold=CX->SNR;
    double snr;

    stringstream binfilename,
                  mipfilename,
                  ccfilename,
                  maskfilename;

    binfilename<<ResultsFolder.toStdString()<<"/"<<Prefix.toStdString()<<".bin";
    maskfilename<<ResultsFolder.toStdString()<<"/"<<Prefix.toStdString()<<"_mask.bin";
    mipfilename<<ResultsFolder.toStdString()<<"/"<<Prefix.toStdString()<<"_mip.dat";
    ccfilename<<ResultsFolder.toStdString()<<"/"<<Prefix.toStdString()<<"_cell_centers.dat";

    ofstream mip(mipfilename.str().c_str());

    progress->updateProgressBar(10,"Generating 2D projection");
    QApplication::processEvents(QEventLoop::AllEvents,100);

    for(i=0;i<width*height;i++) tmp[i]=0;
    for(i=0;i<width*height;i++){
        if((i % (int)(width*height/20.))==0) {
            progress->updateProgressBar((int)(i*100./(width*height)),"Generating 2D projection");
            QApplication::processEvents(QEventLoop::AllEvents,200);
        }
        if(CX->MIP==1){
            for(k=0;k<ntimes;k++) tmp[i]=max((float)data[width*height*k+i],tmp[i]);
        } else {
            for(k=0;k<ntimes;k++) tmp[i]+=data[width*height*k+i];
            tmp[i]/=ntimes;
        }

        mip<<i<<' '<<tmp[i]<<endl;
    }

    image = Mat((int)height,(int)width,CV_32F,(void*)tmp);

    Mat wst=Mat::zeros((int)height,(int)width,CV_16U);
    wsflood(image,wst,ncells,ncells_eff,tdata,ntimes);

    ofstream cenfile(ccfilename.str().c_str());
    vector< vector<short> > all_cell_mask;
    boost::circular_buffer<double> buff(CX->TRAF_BUFFSIZE);
    boost::circular_buffer<double> buff2(CX->TRAF_WS);

    ofstream binfile(binfilename.str().c_str(),ios::out | ios::binary);
    ofstream maskfile(maskfilename.str().c_str(),ios::out | ios::binary);

    for(k=1;k<=ncells;k++){
        vector<Point2i> Plist;
        centers.push_back(Point2i(0,0));
        if((k % (int)(ncells/20.))==0) {
            progress->updateProgressBar((int)(k*100./ncells),"Detrending");
            QApplication::processEvents(QEventLoop::AllEvents,100);
        }
        for(i=0;i<height;i++){
            for(j=0;j<width;j++){
                if(wst.at<ushort>(i,j)==k){
                    centers.back().x+=i;
                    centers.back().y+=j;
                    Plist.push_back(Point2i(i,j));
                }
            }
        }

        if(Plist.size()>0){
            for(i=0;i<ntimes;i++){
                cell_resp[i]=0;
                for(j=0;j<Plist.size();j++){
                    cell_resp[i]+=data[Plist[j].x*width+Plist[j].y+width*height*i];
                }
                cell_resp[i]/=Plist.size();
            }
            traf(&cell_resp[0],ntimes,&cell_resp_dt[0],buff,buff2,1,false,&cell_resp_mask[0]);
            centers.back()=(centers.back()/(int)Plist.size());
            snr=maxmin(&cell_resp_dt[0],ntimes,CX->MAXMIN_BINSIZE);
            if(snr>snr_threshold){
                // when using the "cosi" method, all_cell_resp IS thresholded, unlike
                // in GetCellProperties.
                all_cell_resp.push_back(cell_resp);
                all_cell_mask.push_back(cell_resp_mask);
                cenfile<<centers.back().x<<' '<<centers.back().y<<endl;

                // reassign wst_selec values
                for(unsigned int l=0;l<Plist.size();++l) wst_selec.at<ushort>(Plist[l].x,Plist[l].y)=all_cell_resp.size();

            } else {
                for(unsigned int l=0;l<Plist.size();++l) wst_selec.at<ushort>(Plist[l].x,Plist[l].y)=0;
            }
        }
    }

    ncells_eff=all_cell_resp.size();

    binfile.write((char*)&ncells_eff,sizeof(int));
    binfile.write((char*)&ntimes,sizeof(size_t));
    maskfile.write((char*)&ncells_eff,sizeof(int));
    maskfile.write((char*)&ntimes,sizeof(size_t));

    for(k=0;k<all_cell_resp.size();k++){
        binfile.write((char*)&all_cell_resp[k][0],ntimes*sizeof(double));
        maskfile.write((char*)&all_cell_mask[k][0],ntimes*sizeof(short));
    }

    delete [] tmp; tmp=NULL;
    delete [] tdata; tdata=NULL;

    return 0;

}

// This routine compute the cell properties of segmented cells
void ImageProcessing::GetCellProperties(){

    // open output file to store cell properties
    ofstream CellPropertiesFile(qPrintable(ResultsFolder+"/"+Prefix+"_cell_properties.dat"));

    int i,j,k,counter;
    int pattern_size=(CX->WSFLOOD_CELLRAD+2)*2+1;
    Mat tmp_image=Mat::zeros(pattern_size,pattern_size,CV_32F);
    Mat tmp_wst=Mat::zeros(pattern_size,pattern_size,CV_16U);
    Mat test=Mat::zeros(pattern_size,pattern_size,CV_32F);

    double cellprop[3];
    vector<double> cell_resp(ntimes);
    vector<double> cell_resp_dt(ntimes);
    vector<short> cell_resp_mask(ntimes);

    size_match.resize(nsegments);
    eccentricity.resize(nsegments);
    radius.resize(nsegments);
    compactness.resize(nsegments);
    snr_vec.resize(nsegments);
    centers.resize(nsegments);

    boost::circular_buffer<double> buff(CX->TRAF_BUFFSIZE);
    boost::circular_buffer<double> buff2(CX->TRAF_WS);

    for(i=1;i<=nsegments;i++){
        if((i-1) % (int)floor(nsegments/20.)==0){
            progress->updateProgressBar((int)(i*100./nsegments),"processing cell "+QString::number(i)+"/"+QString::number(nsegments));
            QApplication::processEvents(QEventLoop::AllEvents,100);
        }

        counter=0;
        copyPattern(wst_nocut,tmp_wst,peaks[i-1],CX->WSFLOOD_CELLRAD+2);
        copyPattern(image_float_b,tmp_image,peaks[i-1],CX->WSFLOOD_CELLRAD+2);

        for(j=0;j<tmp_wst.rows*tmp_wst.cols;++j){
            if(tmp_wst.at<ushort>(j)==i){
                test.at<float>(j)=tmp_image.at<float>(j);
                counter++;
            } else test.at<float>(j)=0;
        }

        GetShape(test,cellprop);

        size_match[i-1]=counter;
        radius[i-1]=cellprop[0];
        eccentricity[i-1]=cellprop[1];
        compactness[i-1]=cellprop[2];

        vector<Point2i> Plist;
        centers[i-1]=Point2i(0,0);

        for(k=0;k<height;k++){
            for(j=0;j<width;j++){
                if(wst_nocut.at<ushort>(k,j)==i){
                    centers[i-1].x+=k;
                    centers[i-1].y+=j;
                    Plist.push_back(Point2i(k,j));
                }
            }
        }

        for(k=0;k<ntimes;k++){
            cell_resp[k]=0;

            for(j=0;j<Plist.size();j++){
                cell_resp[k]+=data[Plist[j].x*width+Plist[j].y+width*height*k];
            }
            cell_resp[k]/=Plist.size();
        }

        traf(&cell_resp[0],ntimes,&cell_resp_dt[0],buff,buff2,1,false,&cell_resp_mask[0]);
        centers[i-1]=(centers[i-1]/(int)Plist.size());
        snr_vec[i-1]=maxmin(&cell_resp_dt[0],ntimes,CX->MAXMIN_BINSIZE);

        // NOTE: when all_cell_resp is assigned from here, ALL segments are included, regardless of thresholds on
        // shape parameters or SNR.
        all_cell_resp.push_back(cell_resp);

        CellPropertiesFile<<snr_vec[i-1]<<' '
                          <<radius[i-1]<<' '
                          <<size_match[i-1]<<' '
                          <<eccentricity[i-1]<<' '
                          <<compactness[i-1]<<std::endl;

    }

    CellPropertiesFile.close();

}

void ImageProcessing::WriteCellCenters()
{

    unsigned int i;

    // Save cell centers
    QString ccfilename;
    ccfilename=ResultsFolder+"/"+Prefix+"_cell_centers.dat";
    ofstream ccfile( qPrintable(ccfilename) );

    for(i=0;i<all_cell_resp.size();++i){
        if(select[i]==1){
            ccfile<<centers[i].x<<' '<<centers[i].y<<std::endl;
        }
    }
}

void ImageProcessing::LoadPreviousResults(){

    double maxVal;
    double minVal;
    std::ifstream dimfile( qPrintable(ResultsFolder+'/'+Prefix+"_img_dim.dat"));
    std::ifstream peaksfile( qPrintable(ResultsFolder+'/'+Prefix+"_peaks.dat"));
    std::ifstream cellprop( qPrintable(ResultsFolder+'/'+Prefix+"_cell_properties.dat"));

    nifti_image * nim=NULL;
    nim=nifti_image_read(qPrintable(InputFile),1);

    if(data!=NULL){
        delete [] data; data=NULL;
    }

    data = static_cast<unsigned short*>(nim->data);

    dimfile>>width>>height>>ntimes;
    wst_nocut=cv::imread( (ResultsFolder+'/'+Prefix+"_wst.tiff").toStdString(),IMREAD_ANYDEPTH);
    minMaxLoc(wst_nocut,&minVal,&maxVal);
    nsegments=(int)maxVal;

    dimfile.close();

    peaks.resize(nsegments);
    size_match.resize(nsegments);
    radius.resize(nsegments);
    eccentricity.resize(nsegments);
    compactness.resize(nsegments);
    snr_vec.resize(nsegments);

    for(unsigned int i=0;i<nsegments;++i){
        peaksfile>>peaks[i][0]>>peaks[i][1];
        cellprop>>snr_vec[i]>>radius[i]>>size_match[i]>>eccentricity[i]>>compactness[i];
    }

    cellprop.close();
    peaksfile.close();
}

void ImageProcessing::LoadData(){
    nifti_image * nim=NULL;
    nim=nifti_image_read(qPrintable(InputFile),1);
    width=nim->nx;
    height=nim->ny;
    ntimes=nim->nz;
    if(data!=NULL){
        delete [] data; data=NULL;
    }
    data = static_cast<unsigned short*>(nim->data);
}

void ImageProcessing::LoadDataFromTiff()
{


    //updateProgressBar(0,"loading data");


    int index=1;
    QApplication::processEvents(QEventLoop::AllEvents,100);

    string filename=InputFile.toStdString()+"/"+Prefix.toStdString()+"_Cycle00001_Ch2_"+MLabTools::fixedLengthString(index,6)+".ome.tif";
    cout<<filename<<endl;
    Mat src=imread(filename);
    MatIterator_<ushort> it;
    while(!src.empty()) {
        srcRec.push_back(src.clone());
        filename=InputFile.toStdString()+"/"+Prefix.toStdString()+"_Cycle00001_Ch2_"+MLabTools::fixedLengthString(index,6)+".ome.tif";
        src=imread(filename,IMREAD_UNCHANGED);
        cout<<filename<<endl;
        index++;
    }


    width = srcRec[0].cols;
    height = srcRec[0].rows;
    ntimes = srcRec.size();

    data = new unsigned short[width*height*ntimes];

    size_t i=0,k;

    for(k=0;k<ntimes;++k){
        for(it=srcRec[k].begin<ushort>(),i=0;
            it!=srcRec[k].end<ushort>();
            ++it,++i) data[width*height*k+i]=*it;
    }
}

void ImageProcessing::set_wst_selec()
{
    int i,j,k,l;
    unsigned int counter=1;
    wst_selec=cv::Scalar::all(0);

    for(i=1; i<=nsegments; ++i){

        vector<Point2i> Plist;

        for(k=0;k<height;k++){
            for(j=0;j<width;j++){
                if(wst_nocut.at<ushort>(k,j)==i){
                    Plist.push_back(Point2i(k,j));
                }
            }
        }

        if(select[i-1]==1){
            for(l=0;l<Plist.size();++l) wst_selec.at<ushort>(Plist[l].x,Plist[l].y)=counter;
            counter++;
        }
    }
}
