#include <opencv2/highgui.hpp>
#include <opencv2/core/opengl.hpp>
#include <iostream>
#include "handlers.h"
#include "bcl.h"
#include <GL/freeglut.h>
#ifdef __APPLE__
    #include<OpenGL/gl.h>
#else
    #include <GL/gl.h>
#endif


using namespace std;
using namespace cv;

void param_t2::zero(){
    if(nz>0){
        for(int i=0;i<nz;++i){
            trace[i]=0;
        }
    }
}

void on_mouse_disp(int event, int x, int y, int flags, void* param){
    param_t1* p = (param_t1*)param;
    p->x=x;
    p->y=y;
    if(event == EVENT_LBUTTONDOWN) {
        p->state=1;
    } else {
        p->state=0;
    }

}

void on_GL(void* param){
    param_t2* p = (param_t2*)param;
    if(p->trace!=NULL){
        p->min=p->trace[0];
        p->max=p->trace[0];
        for(int i=0;i<p->nz;++i){
            if(p->trace[i] < p->min) p->min = p->trace[i];
            if(p->trace[i] > p->max) p->max = p->trace[i];
        }

        glLoadIdentity();
        glMatrixMode(GL_PROJECTION);
        glOrtho(0,p->nz,p->min,p->max,-1,1);
        glColor4ub(255,153,0,100);
        glBegin(GL_LINES);
        for(int i=0;i<p->nz-1;i++){
            glVertex3f(i,p->trace[i],0);
            glVertex3f(i+1,p->trace[i+1],0);
        }
        glEnd();
    }
}

void on_GL_BCL(void* param){
    bcl_type* p = (bcl_type*)param;
    double pos;
    int nz=p->cal.size();
    double mi,ma;
    if(!p->IS_EMPTY){
        mi=p->min;
        ma=p->max;
        pos=(mi)+(ma - mi)/100.;

        glLoadIdentity();
        glMatrixMode(GL_PROJECTION);
        glOrtho(0,nz,mi,ma,-1,1);
        glColor4ub(255,153,0,100);
        glBegin(GL_LINES);
        for(int i=0;i<nz-1;i++){
            glVertex3f(i,p->B[i]+p->cal[i],0);
            glVertex3f(i+1,p->B[i]+p->cal[i+1],0);
        }
        glEnd();

        glColor4ub(102, 255, 102,100);
        glBegin(GL_LINES);
        for(int i=0;i<nz-1;i++){
            glVertex3f(i,p->B[i],0);
            glVertex3f(i+1,p->B[i+1],0);
        }
        glEnd();

        glColor4ub(255,0,0,100);
        glBegin(GL_POINTS);
        for(int i=0;i<nz-1;i++){
            if(p->sks[i]==1){
                glVertex3f(i,pos,0);
            }
        }
        glEnd();
    }
}



