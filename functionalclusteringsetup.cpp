#include "functionalclusteringsetup.h"
#include "ui_functionalclusteringsetup.h"
#include <QFileDialog>
#include "functionalclustering.h"
#include "dialog.h"
#include <fstream>
#include <iostream>
#include "displayws.h"
#include "progressdisplay.h"
#include "mainwindow.h"
#include "functionalclusteringresults.h"

FunctionalClusteringSetup::FunctionalClusteringSetup(QWidget *parent) :
    QDialog (parent),
    ui(new Ui::FunctionalClusteringSetup)
{
    ui->setupUi(this);
    ui->EditPrefix->setText("test");

}

FunctionalClusteringSetup::FunctionalClusteringSetup(MainWindow *main, QWidget *parent) :
    QDialog (parent),
    ui(new Ui::FunctionalClusteringSetup)
{
    ui->setupUi(this);
    ui->EditPrefix->setText("test");
    mw=main;
//    ResultsFolder="/home/diana/workspace/ToolBox_ML/ToolBox_QT/test";
//    InputFileName=ResultsFolder+"/"+"test.bin";
    Mode=0;
    SelectEpochDialog=NULL;

}

FunctionalClusteringSetup::~FunctionalClusteringSetup()
{
    delete ui;
    if(SelectEpochDialog!=NULL) delete SelectEpochDialog;
}

void FunctionalClusteringSetup::on_ChooseEpochButton_clicked()
{
    EpochFileName=QFileDialog::getOpenFileName(
                this,
                tr("Select Epoch file"),
                "/home/giovanni/workspace/data/TomSh/",
                "Log File (*.log)"
                );
    ui->EpochFileDisplayLabel->setText(EpochFileName);

    std::vector<QString> EpochList;
    RetrieveEpochList(EpochList);
    SelectEpochDialog = new Dialog(EpochList);

    SelectEpochDialog->setModal("true");
    SelectEpochDialog->exec();

}

void FunctionalClusteringSetup::on_ChooseInputButton_clicked()
{
    switch (ui->comboBox->currentIndex()) {
    case 0:
        Mode=0;
        InputFileName=QFileDialog::getOpenFileName(
                    this,
                    tr("Select Nifti file"),
                    "/home/diana/workspace/data/TomSh/",
                    "Nifti file (*.nii)"
                    );
        break;
    case 1:
        Mode=1;
        InputFileName=QFileDialog::getOpenFileName(
                    this,
                    tr("Select binary file"),
                    "/home/diana/",
                    "Binary File (*.bin)"
                    );
        break;

    case 2:
        Mode=2;
        InputFileName=QFileDialog::getOpenFileName(
                    this,
                    tr("Select data file"),
                    ":/",
                    "Data File (*.dat)"
                    );
        break;
    }

    ui->InputFileDisplayLabel->setText(InputFileName);
}

void FunctionalClusteringSetup::on_ChooseResultsFolderButton_clicked()
{
    ResultsFolder=QFileDialog::getExistingDirectory(
                0,
                ("Select Folder"),
                (".")
                );

    ui->ResultsFolderDisplayLabel->setText(ResultsFolder);
}

void FunctionalClusteringSetup::on_AnalyzeButton_clicked()
{
    ImageProcessing IP(*CX,mw);
    IP.InputFile=InputFileName;
    IP.ResultsFolder=ResultsFolder;
    IP.Prefix=ui->EditPrefix->text();
    IP.ExternalData=ui->checkData->checkState();
    FunctionalClustering CL;
    FunctionalClusteringResults resDisplay;
    Prefix=ui->EditPrefix->text();
    CL.AssignParameters(ResultsFolder,Prefix);
    CL.CX=CX;
    CX->print();
    CL.nEP=GetNEP();
    //ProgressDisplay progress;
    this->hide();

    switch(Mode){
    case 0:
        //progress.show();
        mw->showProgressBar(true);
        IP.cosi();
        CL.MakeOutputFile(ResultsFolder+"/"+Prefix+".bin",EpochFileName,SelectEpochDialog->EpochListON);
        CL.DensCL();
        CL.GetCenters();
        resDisplay.Clustering = &CL;
        resDisplay.SetupGraphs();
        resDisplay.exec();

        mw->showProgressBar(false);
        break;
    case 1:
        CL.MakeOutputFile(InputFileName,EpochFileName,SelectEpochDialog->EpochListON);
        CL.DensCL();
        CL.GetCenters();
        resDisplay.Clustering = &CL;
        resDisplay.SetupGraphs();

        resDisplay.exec();
        break;
    case 2:
        break;
    }
    this->close();
}

void FunctionalClusteringSetup::RetrieveEpochList(std::vector<QString> &EpochList)
{
    std::ifstream EP_file(EpochFileName.toStdString().c_str());

    std::string dum;
    std::string torecast;
    int nepoches=0;

    while(EP_file>>torecast){
        getline(EP_file,dum);
        EP_file>>torecast;
        getline(EP_file,dum);
        nepoches++;
        EpochList.push_back(QString::fromStdString(dum));
    }
}

int FunctionalClusteringSetup::GetNEP()
{
    int count=0;
    for(unsigned int i=0;i<SelectEpochDialog->EpochListON.size();++i)
        if(SelectEpochDialog->EpochListON[i]==1) count++;
    return(count);
}

void FunctionalClusteringSetup::on_checkData_clicked(bool checked)
{
    ui->comboBox->setEnabled(!checked);
    ui->ChooseInputButton->setEnabled(!checked);
}
