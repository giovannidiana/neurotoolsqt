#ifndef BCLDISPLAY_H
#define BCLDISPLAY_H

#include <QDialog>
#include "bcl.h"

namespace Ui {
class BCLDisplay;
}

class BCLDisplay : public QDialog
{
    Q_OBJECT

public:
    explicit BCLDisplay(std::vector<double>&,double,BCLparams &, QWidget *parent = 0,int x0=0,int y0=0);
    ~BCLDisplay();
    void plot();
    void setTitle(QString);
    double *y;
    int n;
    bcl_type* bclres;

private slots:

    void on_variable_currentIndexChanged(int index);

    void on_slider_sliderMoved(int position);

private:
    Ui::BCLDisplay *ui;
    BCLparams *bclpar;
};

#endif // BCLDISPLAY_H
