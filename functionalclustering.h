#ifndef FUNCTIONALCLUSTERING_H
#define FUNCTIONALCLUSTERING_H

#include<QString>
#include "settings.h"
#include<vector>
#include <boost/tuple/tuple.hpp>
#include <QVector>

typedef boost::tuple<int,int,double,double,int,int> denclres;

class FunctionalClustering
{
public:
    FunctionalClustering();
    void AssignParameters(QString resfol,
                          QString pref
                          );
    settings *CX;
    int nEP;
    int nClusters;
    std::vector<denclres> dcl;
    std::vector<std::vector<double> > celldata;
    double celldata_min;
    double celldata_max;
    std::vector< std::pair<double,double> > cellcenters;
    QVector<QString> EpochNames;

    void Get_kNN(std::vector<double>& dist,
                 std::vector<double> &dist_array,
                 std::vector<int> &knn_array,
                 double eps);

    void MakeOutputFile(QString binaryfile,QString epochfile, const std::vector<int> &);
    void DensCL();
    void Classify();
    void GetCenters();


private:
    QString ResultsFolder;
    QString prefix;

};

#endif // FUNCTIONALCLUSTERING_H
