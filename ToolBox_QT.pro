#-------------------------------------------------
#
# Project created by QtCreator 2018-02-16T11:33:11
#
#-------------------------------------------------

QT       += core gui
QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ToolBox_QT
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    displayws.cpp \
    settings.cpp \
    bcl.cpp \
    linint.cpp \
    wsflood.cpp \
    handlers.cpp \
    display.cpp \
    functionalclusteringsetup.cpp \
    functionalclustering.cpp \
    dialog.cpp \
    progressdisplay.cpp \
    functionalclusteringresults.cpp \
    settingsform.cpp \
    qcustomplot.cpp \
    imageprocessingsetup.cpp \
    imageprocessingresults.cpp \
    labelmouse.cpp \
    timesequencedisplay.cpp \
    bcldisplay.cpp \
    assemblyform.cpp \
    vari.cpp \
    mcolors.cpp \
    mlabtools.cpp \
    bclgroupdisplay.cpp \
    entropy.cpp

HEADERS  += mainwindow.h \
    displayws.h \
    settings.h \
    traf.hpp \
    maxmin.hpp \
    handlers.h \
    bcl.h \
    linint.h \
    usr_io.hpp \
    display.h \
    functionalclusteringsetup.h \
    functionalclustering.h \
    dialog.h \
    progressdisplay.h \
    functionalclusteringresults.h \
    settingsform.h \
    qcustomplot.h \
    imageprocessingsetup.h \
    imageprocessingresults.h \
    labelmouse.h \
    timesequencedisplay.h \
    bcldisplay.h \
    assemblyform.h \
    vari.h \
    mcolors.h \
    mlabtools.h \
    bclgroupdisplay.h \
    entropy.h

FORMS    += mainwindow.ui \
    functionalclusteringsetup.ui \
    dialog.ui \
    progressdisplay.ui \
    functionalclusteringresults.ui \
    settingsform.ui \
    imageprocessingsetup.ui \
    imageprocessingresults.ui \
    timesequencedisplay.ui \
    bcldisplay.ui \
    assemblyform.ui \
    bclgroupdisplay.ui

LIBS += -lniftiio `pkg-config --libs opencv` -lGL -lglut -lznz -lm -lz -larmadillo

INCLUDEPATH += /home/giovanni/software/build/nifticlib-2.0.0/include
DEPENDPATH += /home/giovanni/software/build/nifticlib-2.0.0/include

PRE_TARGETDEPS += /home/giovanni/software/build/nifticlib-2.0.0/lib/libniftiio.a

RESOURCES += \
    icons.qrc
