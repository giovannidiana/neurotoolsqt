#include "progressdisplay.h"
#include "ui_progressdisplay.h"

ProgressDisplay::ProgressDisplay(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProgressDisplay)
{
    ui->setupUi(this);
}

ProgressDisplay::~ProgressDisplay()
{
    delete ui;
}


void ProgressDisplay::updateProgressBar(int p, QString s)
{
    ui->AnalysisStatus->setText(s);
    ui->progressBar->setValue(p);
//    this->update();

}
