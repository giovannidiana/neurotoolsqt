#ifndef FUNCTIONALCLUSTERINGRESULTS_H
#define FUNCTIONALCLUSTERINGRESULTS_H

#include <QDialog>
#ifndef Q_MOC_RUN
#include "qcustomplot.h"
#endif
#include "functionalclustering.h"
#include <QModelIndex>
#include <QPixmap>

namespace Ui {
class FunctionalClusteringResults;
}

class FunctionalClusteringResults : public QDialog
{
    Q_OBJECT

public:
    explicit FunctionalClusteringResults(QWidget *parent = 0);
    FunctionalClustering *Clustering;
    ~FunctionalClusteringResults();

    void SetupGraphs();
    void plot_cell_centers();
    void plot_profiles(int);
    void plot_cluster_center(int);

private slots:
    void on_setSlope_valueChanged(int value);

    void on_setDistance_valueChanged(int value);

    void on_classify_clicked();

    void on_listWidget_clicked(const QModelIndex &index);

private:
    Ui::FunctionalClusteringResults *ui;
    double maxx,minx,maxy,miny;
    QPixmap* pixClusters;
    QCPBars *means;
    QCPErrorBars* errorBars;
    QImage *mat;

};

#endif // FUNCTIONALCLUSTERINGRESULTS_H
