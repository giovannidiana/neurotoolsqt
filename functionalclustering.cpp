#include "functionalclustering.h"
#include <iostream>
#include <opencv2/opencv.hpp>
#include <fstream>
#include <sstream>
#include<boost/circular_buffer.hpp>
#include<boost/tuple/tuple.hpp>
#include<boost/array.hpp>
#include<vector>
#include "settings.h"
#include <vector>
#include <armadillo>

using namespace std;

FunctionalClustering::FunctionalClustering()
{

}

void FunctionalClustering::AssignParameters(QString rf,QString pfx)
{
    ResultsFolder=rf;
    prefix=pfx;
}

void FunctionalClustering::MakeOutputFile(QString binaryfile, QString epochfile, const vector<int> &EpochList)
{

    int ncells;
    size_t nz;

    ifstream binfile(binaryfile.toStdString().c_str(),ios::in | ios::binary);
    ifstream EP_file(epochfile.toStdString().c_str());

    binfile.read((char*)&ncells,sizeof(int));
    binfile.read((char*)&nz,sizeof(size_t));

    double* resp = new double[nz];
    boost::circular_buffer<double> buff(CX->TRAF_BUFFSIZE);
    boost::circular_buffer<double> buff2(CX->TRAF_WS);
    double mean=0;
    double mean2,sd2;

    int i=0,j,k;
    pair<int,int> rec;
    double torecast;
    string dum;
    int nepoches=0;
    vector<pair<int,int> > EPmatr;

    ofstream outfile((ResultsFolder+"/"+prefix+"_output.dat").toStdString().c_str());

    unsigned int count=0;
    while(EP_file>>torecast){
        getline(EP_file,dum);
        rec.first=(int)(CX->DENSCL_FREQ*(torecast+CX->DENSCL_TSHIFT));

        EP_file>>torecast;
        getline(EP_file,dum);
        rec.second=(int)(CX->DENSCL_FREQ*(torecast+CX->DENSCL_TSHIFT));
        dum.erase(remove(dum.begin(),dum.end(),' '));

        if(EpochList[count]==1){
            EpochNames.push_back(QString::fromStdString(dum));
            nepoches++;
            EPmatr.push_back(rec);
            //cout<<EPmatr[nepoches-1].first<<' '<<EPmatr[nepoches-1].second<<' '<<dum<<endl;
        }
        count++;
    }

    double all_cell_resp[ncells][nepoches];
    double meanXepoch[nepoches];

    while(i<ncells){
        binfile.read((char*)resp,nz*sizeof(double));

        vector<double> meanvec(nepoches);
        for(j=0;j<nepoches;j++){
            mean=0;
            for(k=EPmatr[j].first;k<=EPmatr[j].second;k++) mean+=resp[k];
            mean/=EPmatr[j].second-EPmatr[j].first;
            meanvec[j]=mean;
        }
        for(k=0;k<nepoches;k++) all_cell_resp[i][k]=meanvec[k];
        i++;
    }

    for(k=0;k<nepoches;k++){
        meanXepoch[k]=0;
        for(i=0;i<ncells;i++){
            meanXepoch[k]+=all_cell_resp[i][k]/ncells;
        }
    }

    for(i=0;i<ncells;i++){
        for(k=0;k<nepoches;k++) all_cell_resp[i][k]/=meanXepoch[k];
    }

    for(i=0;i<ncells;i++){
        mean2=0;
        sd2=0;
        for(k=0;k<nepoches;k++){
            mean2+=all_cell_resp[i][k]/nepoches;
        }
        for(k=0;k<nepoches;k++){
            sd2+=pow(all_cell_resp[i][k]-mean2,2)/nepoches;
        }
        for(k=0;k<nepoches;k++) all_cell_resp[i][k]=(all_cell_resp[i][k]-mean2)/sqrt(sd2);
    }

    vector<double> tmp(nepoches);
    celldata_min=0;
    celldata_max=0;
    for(i=0;i<ncells;i++){
        outfile<<i<<' ';

        for(k=0;k<nepoches;k++){
            outfile<<all_cell_resp[i][k]<<' ';
            tmp[k]=all_cell_resp[i][k];
            if(celldata_min>tmp[k]) celldata_min=tmp[k];
            if(celldata_max<tmp[k]) celldata_max=tmp[k];
        }

        celldata.push_back(tmp);

        outfile<<endl;
    }

    delete [] resp;

}

typedef boost::tuple<int,double,double,int,int> out_tuple;
typedef boost::tuple<int,double> idouble;

struct SortByProb{
        inline bool operator() (const out_tuple& t1, const out_tuple& t2){
            return (t1.get<1>() > t2.get<1>());
        }
};

struct SortByDist{
        inline bool operator() (const idouble& t1, const idouble& t2){
            return (t1.get<1>() < t2.get<1>());
        }
};

void FunctionalClustering::Get_kNN(vector<double>& dist,
             vector<double> &dist_array,
             vector<int> &knn_array,
             double eps=1e-7){

    int i,j,k;
    int curr=0;
    vector<double>::iterator it;
    double comp;
    for(k=0;k<CX->KNNVAR;k++){

        if(k==0) {
            comp=eps;
        } else {
            comp=dist_array[k-1];
        }

        // Give initial values to dist_array and knn_array.
        j=0;while(dist[j]<=comp) j++;
        dist_array[k]=dist[j];
        knn_array[k]=j;

        // Scan through dist to find kNN
        curr=0;for(it=dist.begin();it!=dist.end();++it){
            if(*it<dist_array[k] && *it>comp) {
                dist_array[k]=*it;
                knn_array[k]=curr;
            }
            curr++;
        }

    }

}


void FunctionalClustering::DensCL(){

    // MAIN OUTPUT

    vector< out_tuple > dencl;
    vector< vector<int> > kNN_vec;
    vector< vector<double> > dist_vec;
    vector<int> IDs;

    // run variables
    int i,j,k;

    // COUNT NON ZERO LINES
    const int NSTY=nEP;
    size_t nvox=0;
    size_t zerores=0;
    vector<double> mem;    // flatten vector of voxel responses
    ofstream ThSum((ResultsFolder+"/"+prefix+"_thresholds.dat").toStdString().c_str());
    double tmp_mem[NSTY];
    double mean_resp;
    int ID_orig;
    double read_ID_orig;
    // Read cleaned responses
    ifstream infile((ResultsFolder+"/"+prefix+"_output.dat").toStdString().c_str());
    ofstream oufile((ResultsFolder+"/"+prefix+"_summary.dat").toStdString().c_str());
    //ofstream KNNrecord("KNNrecord.dat");
    ofstream KNNrecord((ResultsFolder+"/"+prefix+"_KNNrecord.dat").toStdString().c_str());

    while(infile>>read_ID_orig){ // read voxel ID first
        ID_orig=(int)read_ID_orig;
        mean_resp=0; // initialize mean voxel response
        for(i=0;i<NSTY;i++) { // read the NSTY responses
            infile>>tmp_mem[i];
            mean_resp+=(double)tmp_mem[i]/NSTY;
        }

        // If the mean response is non zero add the voxel response to
        // the vector "mem" and the voxel ID to IDs.
        if(mean_resp!=0 /*&& (rand() % 100) < 50*/ ){
            nvox++;
            //############################################
            // CAREFUL!! HERE THE RESPONSE IS NORMALIZED!!!
            for(i=0;i<NSTY;i++)	mem.push_back(tmp_mem[i]);
            IDs.push_back(ID_orig);
        } else {
            zerores++;
        }
    }

    //cout<<"Number of zero lines = "<<zerores<<endl;
    //cout<<"Number of voxels = "<<nvox<<endl;

    // LOAD THE DATASET
    int ncol=nvox,nrow=NSTY;
    // build a matrix of responses from the vector "mem"
    arma::mat X(&mem[0],nrow,ncol);
    // allocate memory for all voxels
    dencl.resize(nvox);
    kNN_vec.resize(nvox);
    dist_vec.resize(nvox);

    // CALCULATE PROBABILITY
    arma::vec tmp_vec(NSTY);
    double dist;
    vector<double> d_vec(nvox);

    for(i=0;i< ncol ;i++){ // scan throughout voxel responses
        tmp_vec = X.col(i); // pick up voxel response
        for(j=0;j<ncol;j++){
            // NOTE: here std::norm is the same as arma::norm.
            if(CX->DIST_FUN==settings::COSINE_DIST){
                d_vec[j]=1-arma::norm_dot(tmp_vec,X.col(j));
            } else {
                d_vec[j]=norm(tmp_vec-X.col(j));
            }
        }
        //cout<<"Get probability "<<i<<"\r"<<flush;
        // Feed Get_kNN with the vector of distances relative to i-th voxel,
        // returning dist_vec and kNN_vec for that voxel.
        kNN_vec[i].resize(CX->KNNVAR);
        dist_vec[i].resize(CX->KNNVAR);
        Get_kNN(d_vec,dist_vec[i],kNN_vec[i],1e-7);
        KNNrecord<<IDs[i]<<' ';
        for(k=0;k<CX->KNNVAR;k++) KNNrecord<<kNN_vec[i][k]<<' ';
        KNNrecord<<endl;

        // Assign the first two entries of dencl: the column index (which is not the voxel ID and the density estimation
        dencl[i]=boost::make_tuple(i,1/(pow(dist_vec[i][CX->NNUSED-1],NSTY)),0,-1,-1);
    }
    //cout<<endl;

    // sort dencl with respect to the probability (second entry)
    sort(dencl.begin(),dencl.end(),SortByProb());

    // Once sorted, assign to the voxel with larger density the maximum distance
    // obtained from d_vec.
    dencl[0].get<2>()=*max_element(d_vec.begin(),d_vec.end());

    idouble minel;

    // Scan through the rest of the voxels
    for(i=1;i<nvox;i++){
        vector< boost::tuple<int,double> > tmp_dist(i);
        // Get voxel response
        tmp_vec=X.col(dencl[i].get<0>());
        for(j=0;j<i;j++) {
            tmp_dist[j].get<0>()=j;
            if(CX->DIST_FUN==settings::COSINE_DIST){
                tmp_dist[j].get<1>()=1-arma::norm_dot(tmp_vec,X.col(dencl[j].get<0>()));
            } else {
                tmp_dist[j].get<1>()=norm(tmp_vec-X.col(dencl[j].get<0>()));
            }
        }
        minel=*min_element(tmp_dist.begin(),tmp_dist.end(),SortByDist());
        // associate the third and forth entry in dencl:
        //      - the distance from the neirest neighbour with higher density
        //      - the column label of that voxel (again, not ID)
        dencl[i].get<2>()=minel.get<1>();
        dencl[i].get<3>()=minel.get<0>();
        //cout<<"Get deltas "<<i<<'\r'<<flush;
    }
    //cout<<endl;


    // Assign labels to maxima
    int a_counter=0;
    for(i=0;i<nvox;i++){
        if(log(dencl[i].get<1>())>-NSTY*log(dencl[i].get<2>())+CX->DENSCL_TH_SLOPE
            && log(dencl[i].get<2>())>CX->DENSCL_TH_DIST){

            dencl[i].get<4>()=a_counter;
            a_counter++;
        }
    }
    //cout<<"Selected "<<a_counter<<" seeds"<<endl;
    ThSum<<CX->DENSCL_TH_DIST<<' '<<CX->DENSCL_TH_SLOPE<<' '<<a_counter<<endl;
    nClusters=a_counter;

    // Assign the rest of the voxels
    for(i=1;i<nvox;i++){
        j=dencl[i].get<3>();
        if(dencl[i].get<4>()<0) dencl[i].get<4>()=dencl[j].get<4>();
    }


    for(i=0;i< dencl.size() ;i++){
        oufile<<IDs[dencl[i].get<0>()]<<' '
              <<dencl[i].get<0>()<<' '
              <<dencl[i].get<1>()<<' '
              <<dencl[i].get<2>()<<' '
              <<dencl[i].get<3>()<<' '
              <<dencl[i].get<4>()<<endl;

        dcl.push_back(boost::make_tuple(IDs[dencl[i].get<0>()],
                                        dencl[i].get<0>(),
                                        dencl[i].get<1>(),
                                        dencl[i].get<2>(),
                                        dencl[i].get<3>(),
                                        dencl[i].get<4>()));

    }
}

void FunctionalClustering::Classify()
{
    int i,j;
    int nvox=dcl.size();
    ofstream ouSum((ResultsFolder+"/"+prefix+"_summary2.dat").toStdString().c_str());
    ofstream ThSum((ResultsFolder+"/"+prefix+"_thresholds.dat").toStdString().c_str());
    double th_dist=CX->DENSCL_TH_DIST;
    double th_slope=CX->DENSCL_TH_SLOPE;
    const int NSTY=nEP;

    for(i=0;i<nvox;i++) dcl[i].get<5>()=-1;

    int a_counter=0;
    for(i=0;i<nvox;i++){
        if(log(dcl[i].get<2>())>-NSTY*log(dcl[i].get<3>())+th_slope &&
           log(dcl[i].get<3>())>th_dist){
            dcl[i].get<5>()=a_counter;
            a_counter++;
        }
    }
    ThSum<<th_dist<<' '<<th_slope<<' '<<a_counter<<endl;
    nClusters=a_counter;

    for(i=1;i<nvox;i++){
        j=dcl[i].get<4>();
        if(dcl[i].get<5>()<0) dcl[i].get<5>()=dcl[j].get<5>();
    }

    for(i=0;i< dcl.size() ;i++){
        ouSum <<dcl[i].get<0>()<<' '
              <<dcl[i].get<1>()<<' '
              <<dcl[i].get<2>()<<' '
              <<dcl[i].get<3>()<<' '
              <<dcl[i].get<4>()<<' '
              <<dcl[i].get<5>()<<endl;
    }

}

void FunctionalClustering::GetCenters()
{
    std::ifstream ccen((ResultsFolder+"/"+prefix+"_cell_centers.dat").toStdString().c_str());
    double a,b;
    while(ccen>>a>>b){
        cellcenters.push_back(std::make_pair(a,b));
    }

    ccen.close();

}

