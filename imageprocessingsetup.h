#ifndef IMAGEPROCESSINGSETUP_H
#define IMAGEPROCESSINGSETUP_H

#include <QDialog>
#include "settings.h"
#include "mainwindow.h"
#include "displayws.h"

namespace Ui {
class imageprocessingsetup;
}

class imageprocessingsetup : public QDialog
{
    Q_OBJECT

public:
    explicit imageprocessingsetup(settings &set,MainWindow *parent = 0);
    ~imageprocessingsetup();


private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_Clean_clicked();

    void on_Segment_clicked();

    void on_Assembler_clicked();

    void on_checkData_clicked(bool checked);

private:
    Ui::imageprocessingsetup *ui;
    QString InputFileName;
    QString ResultsFolder;
    QString Prefix;
    MainWindow *main;
    settings *CX;

};

#endif // IMAGEPROCESSINGSETUP_H
