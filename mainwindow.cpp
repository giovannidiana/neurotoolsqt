#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include "displayws.h"
#include <QApplication>
#include "display.h"
#include "displayws.h"
#include "functionalclustering.h"
#include "functionalclusteringsetup.h"
#include "settingsform.h"
#include <QString>
#include "imageprocessingsetup.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    nim(NULL)
{
    ui->setupUi(this);
    ui->progressBar->hide();
    ui->statusBar->showMessage(tr("Welcome to the MLab toolbox"));
    ui->menuBar->setNativeMenuBar(false);
}

MainWindow::~MainWindow()
{
    delete ui;
    if(nim!=NULL) nifti_image_free(nim);
}

void MainWindow::showProgressBar(bool b){
    if(b){
        ui->progressBar->show();
    } else {
        ui->progressBar->hide();
    }
}

void MainWindow::updateProgressBar(int p, QString s)
{
    ui->statusBar->showMessage(s,1000);
    ui->progressBar->setValue(p);
}

void MainWindow::on_actionClose_triggered()
{
    QApplication::exit();
}

void MainWindow::on_actionAbout_this_software_triggered()
{
    QMessageBox about;
    about.setText("This is our toolbox!");
    about.exec();
}

void MainWindow::on_actionSelect_config_file_triggered()
{
    QString SettingFileName=QFileDialog::getOpenFileName(
                this,
                tr("Select config file"),
                ":",
                "Config File (*)"
                );
    CX.load(SettingFileName);
}

void MainWindow::on_actionOpen_Nifti_image_triggered()
{
    showProgressBar(true);
    nim=loadData();
    if(nim!=NULL){
        DisplayNifti(nim);
        updateProgressBar(100,"");
        showProgressBar(false);
    } else {
        QMessageBox msgBox;
        msgBox.setText("Wrong filename!");
        msgBox.exec();
    }
    showProgressBar(false);
}

void MainWindow::on_actionOpen_Nifti_in_memory_triggered()
{

    DisplayNifti(nim);
}

nifti_image* MainWindow::loadData()
{
    nifti_image* ni=NULL;

    QString NiftiFileName=QFileDialog::getOpenFileName(
                this,
                tr("Select Nifti file"),
                "/home/diana/workspace/data/TomSh/",
                "Nifti files (*.nii)"
                );

    updateProgressBar(0,"loading data");

    QApplication::processEvents(QEventLoop::AllEvents,100);

    if(!NiftiFileName.isEmpty()){

        if(nim!=NULL){
            updateProgressBar(100,"release memory");
            std::cout<<"free nifti"<<std::endl;
            nifti_image_free(nim);
            nim=NULL;
        }

        ni=nifti_image_read(NiftiFileName.toStdString().c_str(),1);
    }

    return ni;
}


void MainWindow::on_actionFunctional_clustering_triggered()
{
    FunctionalClusteringSetup setup(this);
    setup.setModal(true);
    setup.CX = &CX;
    setup.exec();
}

void MainWindow::on_actionSet_parameters_triggered()
{
    SettingsForm SF(&CX);
    SF.setModal(true);
    SF.exec();
}

void MainWindow::on_actionNeural_assembler_triggered()
{
    QString NiftiFileName=QFileDialog::getOpenFileName(
                this,
                tr("Select Nifti file"),
                "/home/diana/workspace/data/TomSh/",
                "Nifti files (*.nii)"
                );

    QApplication::processEvents(QEventLoop::AllEvents,100);
    ImageProcessing IP(CX,this);
    IP.displayWS();
}

void MainWindow::on_actionImage_processing_triggered()
{
    imageprocessingsetup setup(CX,this);
    setup.setModal(true);
    setup.exec();
}


void MainWindow::on_actionOpen_Tif_list_triggered()
{
    QString InputFileName=QFileDialog::getExistingDirectory(
            0,
            ("Select Folder"),
            (".")
            );

    bool ok;
    QString Prefix=QInputDialog::getText(this,tr("QInputDialog::getText()"),
                                         tr("Prefix:"));

    DisplayTiff(InputFileName,Prefix);
}
