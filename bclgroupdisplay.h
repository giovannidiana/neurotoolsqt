#ifndef BCLGROUPDISPLAY_H
#define BCLGROUPDISPLAY_H

#include <QDialog>
#include "bcl.h"
#include "qcustomplot.h"

namespace Ui {
class BCLGroupDisplay;
}

class BCLGroupDisplay : public QDialog
{
    Q_OBJECT

public:
    explicit BCLGroupDisplay(std::vector<std::vector<double> >&,std::vector<int> selection,double,BCLparams &, QWidget *parent = 0,int x0=0,int y0=0);
    ~BCLGroupDisplay();
    void plot();
    void setTitle(QString);

private slots:

    void on_variable_currentIndexChanged(int index);

    void on_slider_sliderMoved(int position);

    void on_Resample_clicked();

private:
    Ui::BCLGroupDisplay *ui;
    BCLparams *bclpar;
    QVector<QCustomPlot*> cplist;
    std::vector<double*> y;
    std::vector<std::vector<double> > *responsematrix;
    std::vector<int> *selectvec;
    int n;
    int ncells;
    std::vector<bcl_type> bclres;
};

#endif // BCLGROUPDISPLAY_H
