#include "labelmouse.h"
#include <iostream>

LabelMouse::LabelMouse(QWidget *parent) : QLabel(parent)
{
    this->setMouseTracking(true);
}

LabelMouse::~LabelMouse(){

}

void LabelMouse::mouseMoveEvent(QMouseEvent *ev)
{
    QPoint mouse_pos=ev->pos();
    if(mouse_pos.x() <=this->size().width() && mouse_pos.y()<=this->size().height()){
        if(mouse_pos.x()>0 && mouse_pos.y()>0)
            emit mouse_moved(mouse_pos);
    }

}

void LabelMouse::mousePressEvent(QMouseEvent *ev)
{
    QPoint mouse_pos=ev->pos();
    if(mouse_pos.x() <=this->size().width() && mouse_pos.y()<=this->size().height()){
        if(mouse_pos.x()>0 && mouse_pos.y()>0)
            if(ev->button()==Qt::LeftButton){
                emit mouse_click_on_cell(mouse_pos);
            } else if(ev->button()==Qt::RightButton) {
                emit mouse_double_click_on_cell(mouse_pos);
            }

    }

}
