#include "dialog.h"
#include "ui_dialog.h"
#include <vector>
#include <iostream>
#include <QCheckBox>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::Dialog(std::vector<QString> &list, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    EpochListON.resize(list.size());

    for(unsigned int i=0;i<list.size();++i){
        ui->listWidget->addItem(list[i]);
        EpochListON[i]=0;
    }

}

Dialog::~Dialog()
{
    delete ui;
}


void Dialog::on_listWidget_clicked(const QModelIndex &index)
{
    if(EpochListON[index.row()]==0){
        ui->listWidget->item(index.row())->setBackgroundColor(Qt::green);
        EpochListON[index.row()]=1;
    } else {
        ui->listWidget->item(index.row())->setBackgroundColor(Qt::white);
        EpochListON[index.row()]=0;
    }

}

void Dialog::on_pushButton_clicked()
{
    close();
}

void Dialog::on_clearButton_clicked()
{
    for(unsigned int i=0;i<EpochListON.size();++i) {
        ui->listWidget->item(i)->setBackgroundColor(Qt::white);
        ui->selectAll->setChecked(false);
        EpochListON[i]=0;
    }
}

void Dialog::on_selectAll_stateChanged(int arg1)
{
    if(arg1==0){
        for(unsigned int i=0;i<EpochListON.size();++i) {
            ui->listWidget->item(i)->setBackgroundColor(Qt::white);
            EpochListON[i]=0;
        }
    } else {
        for(unsigned int i=0;i<EpochListON.size();++i) {
            ui->listWidget->item(i)->setBackgroundColor(Qt::green);
            EpochListON[i]=1;
        }
    }
}

