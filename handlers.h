#ifndef HANDLERS_H
#define HANDLERS_H

#include<iostream>

class param_t1{
    public:
     param_t1() : x(0),y(0),state(0) {};
     int x,y;
     int state;
};

class param_t2{
    public:
    param_t2() : trace(NULL), nz(0) {};
    param_t2(size_t n) : trace(NULL), nz(n) {} ;
    double* trace;
    size_t nz;
    double max;
    double min;
    void zero();
};

void on_GL(void*);
void on_GL_BCL(void*);
void on_mouse_disp(int,int,int,int,void*);

#endif // HANDLERS_H

