#ifndef TRAF_HPP
#define TRAF_HPP

#include<iostream>
#include<cmath>
#include <boost/circular_buffer.hpp>

using namespace std;

template<typename T >
void traf(T* x, size_t nz,T* x_out,
          boost::circular_buffer<T>& buffer,
          boost::circular_buffer<T>& buffer_mv,
          int normalize=0,
          bool verbose=0,
          short *mask=NULL){
    const int r=buffer.capacity();
    const int ws=buffer_mv.capacity();
    int i,j,k;
    int ind;
    int IndOnX=0;
    T sum=0,var=0,mean,sd,slope;
    T mean_comp,mean_comp_first;
    T bsum=0,bmean=0;
    T global_average=0;
    bool reached_end=0;
    int counter=0;

    for(k=0;k<nz;k++) global_average+=max((T)0,x[k]);
    global_average/=nz;

    T buff_init[ws];

    for(k=0;k<r;k++) {
        buffer.push_back(x[k]);
        sum+=x[k];
        var+=pow(x[k],2);
    }

    mean=sum/r;
    for(k=0;k<r;k++) x_out[k]=(x[k]-mean);
    if(mask) for(k=0;k<r;k++) mask[k]=0;

    if(verbose) cout<<mean<<endl;
    sd=sqrt(var/r-pow(mean,2));

    IndOnX=r;
    while(IndOnX<nz){

        // FILL MOVING WINDOW BUFFER
        bsum=0;
        k=0;while(k<ws && IndOnX+k<nz) {
            buffer_mv.push_back(x[IndOnX+k]);
            buff_init[k]=x[IndOnX+k];
            bsum+=x[IndOnX+k];
            k++;
        }
        counter=k;
        if(k<ws){
            reached_end=1;
            if(verbose) cout<<reached_end<<' '<<IndOnX<<endl;
        }

        mean_comp=bsum/counter;
        mean_comp_first=mean_comp;
        ind=0;

        if((((mean_comp-mean)/(sd/sqrt(ws))>ws || (mean_comp-mean)/(ws/2.)>3)) && !reached_end){
            if(verbose==1) {
                cout<<"Resp. at "<<IndOnX<<endl;
                cout<<"Slope = "<<(mean_comp-mean)/(sd/sqrt(ws))<<endl;
                cout<<"Deviation = "<<(mean_comp-mean)/(ws/2.)<<endl;
                cout<<"mean = "<<mean<<endl;
                cout<<"mean_comp = "<<mean_comp<<endl;
                cout<<"sd = "<<sd<<endl;
            }

            while(((mean_comp-mean)/(sd/sqrt(ws))>(ws) || fabs(mean_comp-mean)/(ws/2.)>3)){
                if(IndOnX+ind+ws<nz){
                    buffer_mv.push_back(x[IndOnX+ind+ws]);
                    bsum+=x[IndOnX+ind+ws]-x[IndOnX+ind];
                    mean_comp=bsum/ws;
                    ind++;
                    if(verbose){
                        cout<<" mc = "<<mean_comp<<endl;
                    }
                }
                if((IndOnX+ind+ws)==nz || (ind > 500)) {
                    ind=0;
                    for(k=0;k<ws;k++) buffer_mv[k]=buff_init[k];
                    mean_comp=mean_comp_first;
                    break;
                }
            }
            if(verbose==1) {
                cout<<"Resp. stops at "<<IndOnX+ind<<endl;
                cout<<"-----------------"<<endl;
            }

        }

        for(k=IndOnX;k<ind+IndOnX+counter;k++){
            x_out[k]=x[k]-(mean+(mean_comp-mean)/(ind+ws)*(k-IndOnX));
            if(mask) mask[k]=(k<ind+IndOnX) ? 1 : 0;
        }

        if(!reached_end){
            // UPDATE SUM AND VARIANCE
            for(k=0;k<ws;k++) {
                buffer.push_back(buffer_mv[k]);
                sum+=buffer_mv[k]-buffer[0];
                var+=pow(buffer_mv[k],2)-pow(buffer[0],2);
            }

            // RECALCULATE MEAN AND SD
            mean=sum/r;
            sd=sqrt(var/r-pow(mean,2));
        } else {
            break;
        }

        // REPOSITION ON X
        IndOnX+=ind+counter;

    }

    if(normalize)
        for(k=0;k<nz;k++) x_out[k]/=global_average;


}


#endif // TRAF_HPP

